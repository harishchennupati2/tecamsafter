const jsonServer = require('json-server')
const config = require('config')
const bodyParser = require('body-parser')
const auth = require('./src/common/auth')
const loginService = require('./src/services/loginService')
const newAppointmentService = require('./src/services/newAppointmentService')
const userService = require('./src/services/userService')
const errorResponse = require('./src/common/errorResponse')
const _ = require('lodash')
const app = jsonServer.create()
const router = jsonServer.router('data/db.json')
const middlewares = jsonServer.defaults()
const requestIP = require('request-ip')
const logger = require('./src/common/logger')

app.set('port', config.PORT)
app.use(middlewares)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
const apiBase = `/presentation/${config.API_VERSION}`
app.post(`${apiBase}/auth`, loginService.login(router.db))
app.use(auth)

app.get(`${apiBase}/appointplusportal/tecams/required-extension-types`, newAppointmentService.retrieveRequiredExtensionTypes(router.db))
app.get(`${apiBase}/appointplusportal/tecams/user-summary`, userService.searchUsers(router.db))
app.post(`${apiBase}/appointplusportal/tecams/slot-enquiry`, newAppointmentService.slotEnquiry(router.db))
app.post(`${apiBase}/appointplusportal/tecams/reservation/:slotID`, newAppointmentService.postAppointment(router.db))
let remapRouter = {}

remapRouter[`${apiBase}/appointplusportal/tecams/user-work-schedules?*`] =
  '/user-work-schedules'
remapRouter[
  `${apiBase}/appointments/appointplusportal/tecams/existing-appointments?*`
  ] = `/appointments`
remapRouter[
  `${apiBase}/appointplusportal/tecams/work-types`
  ] = `/workTypes`

app.use(jsonServer.rewriter(remapRouter))

remapRouter = {}
remapRouter[apiBase + '/*'] = '/$1'

app.use(jsonServer.rewriter(remapRouter))
app.use(function (req, res, next) {
  logger.log({
    level: 'info',
    message: `
     URL:${JSON.stringify(req.url)}
     QUERY_PARAMS:${JSON.stringify(req.query, null, 4)},
     HEADERS:${JSON.stringify(req.headers, null, 4)},
     `,
    METHOD: `${req.method}`,
    ip: `${requestIP.getClientIp(req)}`,
  })
  if (req.method === 'GET') {
    if (_.has(req.query, 'perPage')) {
      req.query._limit = req.query.perPage
    }
    if (_.has(req.query, 'page')) {
      req.query._page = req.query.page
    }
    next()
  } else {
    next()
  }
})

app.use(router)
app.use(errorResponse)
router.render = (req, res) => {
  console.log(res.locals.data)
  const data = _.cloneDeep(res.locals.data)
  res.set('correlationId', 'db1234')
  res.set('requestId', '1')
  res.set('time', new Date())
  logger.log({
    level: 'info',
    message: `
      RESPONSE:${JSON.stringify(res.locals.data, null, 4).substring(0, 1000) +
    '...'}
      `,
    METHOD: `${req.method}`,
    status_code: `${res.statusCode}`,
    ip: `${requestIP.getClientIp(req)}`,
  })

  return res.jsonp(data)
}

app.listen(app.get('port'), () => {
  console.log(`Express server listening on port ${app.get('port')}`)
})

module.exports = app
