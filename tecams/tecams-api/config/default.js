/**
 * The configuration file.
 */

module.exports = {
  API_VERSION: process.env.API_VERSION || 'v1',
  PORT: process.env.PORT || 3002 ,
  DEFAULT_PAGE_SIZE: process.env.DEFAULT_PAGE_SIZE || 20,
  DEFAULT_ORDER: process.env.DEFAULT_ORDER || 'asc',
  DEFAULT_SORT: process.env.DEFAULT_SORT || 'id',

  JWT: {
    SECRET: process.env.JWT_SECRET || 'mysecret',
  }
};
