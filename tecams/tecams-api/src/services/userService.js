/**
 * This module provide user related services
 */

const config = require('config')
const jwt = require('jsonwebtoken')
const errors = require('../common/errors')

/**
 * @param {Object} db  the lowdb instance
 * @returns {Function} the wrapped function
 */
function searchUsers (db) {
  return function (req, res, next) {
    const { userID, partialName, skillsetID } = req.query
    let users = []
    if (userID) {
      users.push(...db.get('userSummary.userProfiles')
      .filter(item => item.userID.toLowerCase().includes(userID.toLowerCase())).value())
    }
    if (partialName) {
      users.push(...db.get('userSummary.userProfiles')
      .filter(item => `${item.firstName} ${item.lastName}`.toLowerCase().includes(partialName.toLowerCase())).value())
    }
    if (skillsetID) {
      users.push(...db.get('userSummary.userProfiles')
        .filter(item => _.includes(item.skillsets,
          skill => skill.skillsetID.toLowerCase() === skillsetID.toLowerCase())).value()
      )
    }
    res.json({
      userProfiles: users
    })
  }
}

module.exports = {
  searchUsers,
}
