/**
 * This module provide new appointment related services
 */
const moment = require('moment')
const errors = require('../common/errors');

/**
 * @param {Object} db  the lowdb instance
 * @returns {Function} the wrapped function
 */
function retrieveRequiredExtensionTypes (db) {
  return function (req, res, next) {
    const { level, appointmentType, appointmentSubType, productFamily, workTypeID } = req.query
    if (level === 'APPT_LEVEL') {
      if (appointmentType === 'BUSINESS_HOURS') {
        if (appointmentSubType === 'CUTOVER') {
          return res.json(db.get('businessHoursCutOverRequiredExtensionTypes').value())
        } else if (appointmentSubType === 'NONCUTOVER') {
          return res.json(db.get('businessHoursNonCutOverRequiredExtensionTypes').value())
        }
      }
    } else if (level === 'TASK_LEVEL') {
      return res.json({
        extensions: db.get('workTypeRequiredExtensionTypes.extensions')
        .filter(item => workTypeID === item.workTypeID).value()
      })
    }
    return res.json({})
  }
}

/**
 * @param {Object} db  the lowdb instance
 * @returns {Function} the wrapped function
 */
function slotEnquiry (db) {
  return function (req, res, next) {
    const leadTime = 3;
    const format = 'YYYY-MM-DDTHH:mm:ss'
    let date = moment(req.body.data.slotCriteria.startDate, format).startOf('day')
    const totalSlots = 30
    const slots = []
    // Ensure 3 business days lead time
    const minDate = moment().startOf('day').add(3, 'days')
    if(date.isBefore(minDate)) {
      date = minDate
    }

    const day = date.dayOfYear()
    for(let currentDay = day; currentDay <= day + totalSlots; currentDay++) {
      // Create slot once for 3 days
      if(currentDay % 3 === 0) {
        const currentDate = moment(date).dayOfYear(currentDay)
        slots.push({
          slotID: `S123${currentDay}0`,
          startDate: currentDate.add(8, 'hours').format(format),
          endDate: currentDate.add(2, 'hours').format(format),
        })

        slots.push({
          slotID: `S123${currentDay}1`,
          startDate: currentDate.format(format),
          endDate: currentDate.add(2, 'hours').format(format),
        })

        slots.push({
          slotID: `S123${currentDay}2`,
          startDate: currentDate.add(2, 'hours').format(format),
          endDate: currentDate.add(2, 'hours').format(format),
        })

        slots.push({
          slotID: `S123${currentDay}3`,
          startDate: currentDate.add(30,'minutes').format(format),
          endDate: currentDate.add(2, 'hours').format(format),
        })
      }
    }

    const response = {
      slots,
      estimatedDuration: 120
    }
    return res.json(response)
  }
}

/**
 * @param {Object} db  the lowdb instance
 * @returns {Function} the wrapped function
 */
function postAppointment (db) {
  return function (req, res, next) {
    // Throw error if user selects 16:30 - 18:30 slot
    if(req.params.slotID.endsWith('3')){
      throw new errors.BadRequestError('The slot is not available')
    }
    const appointmentID = `TECAMS${(new Array(10)).fill().map(()=> Math.floor(Math.random()*10)).join('')}TECAM`
    return res.json({appointmentID})
  }
}


module.exports = {
  retrieveRequiredExtensionTypes,
  slotEnquiry,
  postAppointment
}
