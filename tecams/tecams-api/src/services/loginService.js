/**
 * This module provide authentication service.
 */

const config = require('config');
const jwt = require('jsonwebtoken');
const errors = require('../common/errors');

/**
 * @param {Object} db  the lowdb instance
 * @returns {Function} the wrapped function
 */
function login(db) {
  return function(req, res, next) {
    const username = req.body.username;

    const user = db
      .get('users')
      .find({username})
      .value();

    if (!user) {
      throw new errors.UnauthorizedError('No user found');
    }
    if (req.body.password !== user.password) {
      throw new errors.UnauthorizedError('Wrong username or password');
    }

    const jwtBody = {
      id: user.id,
      username,
    };
    delete user.password;
    res.json({
      ...user,
      accessToken: jwt.sign(jwtBody, Buffer.from(config.JWT.SECRET, 'base64')),
    });
  };
}

module.exports = {
  login,
};
