/**
 * This module catch error and return error response.
 */

const _ = require('lodash');
const HttpStatus = require('http-status-codes');
const winston = require('winston');
const requestIP = require('request-ip');
const {format} = winston;

const logger = winston.createLogger({
  level: 'error',
  meta: 'true',
  transports: [
    new winston.transports.Console({
      format: format.combine(
        format.colorize({all: true}),
        format.simple(),
        format.timestamp({
          format: 'YY-MM-DD HH:mm:ss',
        }),
        format.align(),
        format.printf(info => {
          const {timestamp, level, message, ...args} = info;
          const ts = timestamp.slice(0, 19).replace('T', ' ');
          let code = args.status_code ? args.status_code : '';
          return `${ts} ${args.METHOD} ${code} [${level}]:${args.ip} ${message}`;
        })
      ),
    }),
  ],
});
/**
 * Catch error and return error response.
 * @param {Object} err     the error
 * @param {Object} req     the request
 * @param {Object} res     the response
 * @param {Function} next  the callback function
 * @returns {Function} the wrapped function
 */
module.exports = function(err, req, res, next) {
  const errorResponse = {};
  const status = err.isJoi
    ? HttpStatus.BAD_REQUEST
    : err.httpStatus || HttpStatus.INTERNAL_SERVER_ERROR;
  errorResponse.status = status;

  if (_.isArray(err.details)) {
    if (err.isJoi) {
      _.map(err.details, e => {
        if (e.message) {
          if (_.isUndefined(errorResponse.message)) {
            errorResponse.message = e.message;
          } else {
            errorResponse.message += `, ${e.message}`;
          }
        }
      });
    }
  }
  if (_.isUndefined(errorResponse.message)) {
    if (err.message && status !== HttpStatus.INTERNAL_SERVER_ERROR) {
      errorResponse.message = err.message;
    } else {
      errorResponse.message = 'Internal server error';
    }
  }
  logger.log({
    level: 'error',
    message:
      `Error happened in ${req.method} ${req.url} ` +
      JSON.stringify(errorResponse.message, null, 4).substring(0, 1000) +
      '...',
    METHOD: `${req.method}`,
    status_code: `${errorResponse.status}`,
    ip: `${requestIP.getClientIp(req)}`,
  });
  res.status(status).json(errorResponse);
};
