/**
 * Authentication middleware
 */
const config = require('config');
const jwt = require('jsonwebtoken');
const errors = require('./errors');

const loginUrl = `/api/${config.API_VERSION}/login`;

/**
 * Check if the request is authenticated.
 * @param {Object} req     the request
 * @param {Object} res     the response
 * @param {Function} next  the callback function
 * @returns {Function} the wrapped function
 */
function authMiddleware(req, res, next) {
  // Ignore login path, OAuth paths
  if (
    req.path === loginUrl
  ) {
    next();
    return;
  }

  // Parse the token from request header
  let token;
  if (req.headers.authorization) {
    const authHeaderParts = req.headers.authorization.split(' ');
    if (authHeaderParts.length === 2 && authHeaderParts[0] === 'Bearer') {
      token = authHeaderParts[1];
    }
  }

  if (!token) {
    throw new errors.UnauthorizedError(
      'Action is not allowed for anonymous or invalid token'
    );
  }

  jwt.verify(
    token,
    Buffer.from(config.JWT.SECRET, 'base64'),
    (err, decoded) => {
      if (err) {
        next(new errors.UnauthorizedError('Invalid token'));
        return;
      }

      req.authUser = {
        id: decoded.id,
        username: decoded.username,
      };

      next();
    }
  );
}

module.exports = authMiddleware;
