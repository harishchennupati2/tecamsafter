const winston = require('winston');
const {format} = winston;

const logger = winston.createLogger({
  level: 'info',
  meta: 'true',
  transports: [
    new winston.transports.Console({
      format: format.combine(
        format.colorize({all: true}),
        format.simple(),
        format.timestamp({
          format: 'YY-MM-DD HH:mm:ss',
        }),
        format.align(),
        format.printf(info => {
          const {timestamp, level, message, ...args} = info;
          const ts = timestamp.slice(0, 19).replace('T', ' ');
          let code = args.status_code ? args.status_code : '';
          return `${ts} ${args.METHOD} ${code} [${level}]:${args.ip} ${message}`;
        })
      ),
    }),
  ],
});
module.exports = logger;
