# tecams-api (mock server

## install npm modules

    - npm install

## Run the server

    - npm run start
    - default port is 3002 you can configure it in config/default.js

## auth endpoint

    - login in to this route to get your token
    - localhost:3002/presentation/v1/auth in postman or using curl
    - sample username and password
    - username : ashish
    - password : password
    - store this token somewhere you need to put it in yur front end env

# tecams-ui (front-end aap)
    - install dependencies
    - npm install
    - install env-cmd globally (npm install -g env-cmd)
    - put the token that you got in .env file in tecams-master (frontend)
     (at REACT_APP_AUTH_TOKEN)
    - put the port number in the .env at REACT_APP_BASE_URL instead pf 3002 if you
      changed the PORT in backend
    - now run
        npm run start your front end will be visible at (localhost:3000)

## NOTE
    The dummy data is present in tecams-api/data/db.json if you want to change the data you can directly change there
