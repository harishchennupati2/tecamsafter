import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import style from './App.module.scss';
import Footer from './shared/components/Footer/Footer.component';
import Header from './shared/components/Header/Header.component';
import MyWorkSchedule from 'containers/MyWorkSchedule/MyWorkSchedule.component';
import ActionDetail from 'containers/ActionDetail/ActionDetail.component';
import Landing from 'containers/Landing/Landing';
import MyAppointments from 'containers/MyAppointments/MyAppointments.component';
import NewAppointment from 'containers/NewAppointment/NewAppointment.component';
import AppointmentDetails from 'containers/AppointmentDetails/AppointmentDetails.component';

function App() {
  return (
    <Router>
      <div
        className={`d-flex flex-column ${style['component-container']} justify-content-between`}
      >
        <Header />
        <div className={`${style['middle-content']}`}>
            <Switch>
              <Route path='/' exact>
                <Landing />
              </Route>
              <Route path='/my-appointments' exact>
                <MyAppointments />
              </Route>
              <Route path='/new-appointment' exact>
                <NewAppointment />
              </Route>
              <Route path='/appointment-details/:id' exact>
                <AppointmentDetails />
              </Route>
              <Route path='/action-detail'>
                <ActionDetail />
              </Route>
              <Route path='/my-work-schedule'>
                <MyWorkSchedule />
              </Route>
            </Switch>
        </div>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
