import { API_URL } from '../config/constants'
import axios from 'axios'

const instance = axios.create({
  baseURL: API_URL,
  timeout: 10000
})
// TODO here we need to change when we do the actual implementation
export const fetch = {
  get: instance.get,
  post: instance.post,
  put: instance.put,
  delete: instance.delete
}
