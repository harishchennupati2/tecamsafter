import React, { useEffect, useState } from 'react';
import './styles.scss';
import { useParams } from 'react-router-dom';
import { fetchAppointmentById } from 'redux/actions';
import { connect } from 'react-redux';
import icBlueExpand from 'assets/icons/ic-blue-collapse-expand.svg';
import moment from 'moment';
import ReactLoading from 'react-loading';

const AppointmentDetails = ({ appointment, fetchAppointmentById }) => {
  let { id } = useParams();
  const [boxDetail, setBoxDetail] = useState([
    { isShow: true },
    { isShow: true },
    { isShow: true },
    { isShow: true }
  ]);

  useEffect(() => {
    fetchAppointmentById(id);
  }, [fetchAppointmentById, id]);

  const showDetail = (index, value) => {
    const data = [...boxDetail];
    data[index] = { ...data[index], isShow: value }
    setBoxDetail([...data]);
  }

  if (appointment) {
    return (
      <div className="appointment-detail">
        <div className="top-page">
          <div className="cover-top-page">
            <span className="title-page">Appointment {appointment.id}</span>
            <span className="status-user">
              <span className={`circle-status ${appointment.status === 'Confirmed' ? 'confirmed' :
                appointment.status === 'Requested' ? 'requested' :
                  appointment.status === 'Reserved' ? 'reserved' :
                    appointment.status === 'In Progress' ? 'in-progress' :
                      appointment.status === 'Complete' ? 'complete' :
                        appointment.status === 'Incompleted' ? 'incomplete' : 'canceled'}`}></span>
              {appointment.status}
            </span>
          </div>
          {/* end/cover top page  */}
        </div>

        <div className='tab-item review'>
          <div className={`cover-content ${!boxDetail[0].isShow ? 'collapse-ver' : ''}`}>
            <div className='top-title'>
              <span className='title-box'>Appointment Details</span>
              <div className='right-top'>
                <span className='click-expand' onClick={() => showDetail(0, !boxDetail[0].isShow)}>
                  <img src={icBlueExpand} alt='icon' />
                </span>
              </div>
            </div>
            <div className="box-content">
              <div className="content-box">
                <div className="box-detail">
                  <ul className="list-title">
                    <li className="item-title">
                      <span className="name-item">Appointment Type
                          <span className="two-dots">:</span>
                      </span>
                      <span className="text-right-input">{appointment.appointmentType}</span>
                    </li>
                    <li className="item-title">
                      <span className="name-item">Appointment Sub type
                          <span className="two-dots">:</span>
                      </span>
                      <span className="text-right-input">{appointment.appointmentSubType}</span>
                    </li>
                    <li className="item-title">
                      <span className="name-item">Appointment Priority
                          <span className="two-dots">:</span>
                      </span>
                      <span className="text-right-input">{appointment.appointmentPriority}</span>
                    </li>
                  </ul>
                </div>
                {
                  appointment.appointmentType === 'After Hours' && (
                    <div className="group-after-hours">
                      <div className="box-detail">
                        <span className="title-b-detail">Additional After Hours Information</span>
                        <ul className="list-title">
                          <li className="item-title">
                            <span className="name-item">Project Name
                              <span className="two-dots">:</span>
                            </span>
                            <span className="text-right-input">{appointment.additionalAfterHoursAppointment.projectName}</span>
                          </li>
                          <li className="item-title">
                            <span className="name-item">Funding Code Type
                              <span className="two-dots">:</span>
                            </span>
                            <span className="text-right-input">{appointment.additionalAfterHoursAppointment.fundingCodeType}</span>
                          </li>
                          <li className="item-title">
                            <span className="name-item">Funding Code
                              <span className="two-dots">:</span>
                            </span>
                            <span className="text-right-input">{appointment.additionalAfterHoursAppointment.fundingCode}</span>
                          </li>
                        </ul>
                      </div>
                      <div className="box-detail">
                        <span className="title-b-detail">Customer Funding Approver</span>
                        <ul className="list-title">
                          <li className="item-title">
                            <span className="name-item">First Name
                              <span className="two-dots">:</span>
                            </span>
                            <span className="text-right-input">{appointment.customerFundingApprover.firstName}</span>
                          </li>
                          <li className="item-title">
                            <span className="name-item">Last Name
                              <span className="two-dots">:</span>
                            </span>
                            <span className="text-right-input">{appointment.customerFundingApprover.lastName}</span>
                          </li>
                          <li className="item-title">
                            <span className="name-item">Phone Number
                              <span className="two-dots">:</span>
                            </span>
                            <span className="text-right-input">{appointment.customerFundingApprover.phoneNumber}</span>
                          </li>
                          <li className="item-title">
                            <span className="name-item">Alternative Phone Number
                              <span className="two-dots">:</span>
                            </span>
                            <span className="text-right-input">{appointment.customerFundingApprover.alternativePhoneNumber}</span>
                          </li>
                          <li className="item-title">
                            <span className="name-item">Email Address
                              <span className="two-dots">:</span>
                            </span>
                            <span className="text-right-input">{appointment.customerFundingApprover.emailAddress}</span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  )
                }
                {
                  appointment.additionalAppointment && (
                    <div className="box-detail">
                      <span className="first-title">Additional Appointment Details</span>
                      <span className="title-b-detail">Cut-over</span>
                      <ul className="list-title">
                        <li className="item-title">
                          <span className="name-item">Cut-over from Product
                              <span className="two-dots">:</span>
                          </span>
                          <span className="text-right-input">{appointment.additionalAppointment.fromProduct}</span>
                        </li>
                        <li className="item-title">
                          <span className="name-item">Cut-over to Product
                              <span className="two-dots">:</span>
                          </span>
                          <span className="text-right-input">{appointment.additionalAppointment.toProduct}</span>
                        </li>
                        <li className="item-title">
                          <span className="name-item">Exiting FNN
                              <span className="two-dots">:</span>
                          </span>
                          <span className="text-right-input">{appointment.additionalAppointment.exitingFNN}</span>
                        </li>
                        <li className="item-title">
                          <span className="name-item">New FNN
                              <span className="two-dots">:</span>
                          </span>
                          <span className="text-right-input">{appointment.additionalAppointment.newFNN}</span>
                        </li>
                      </ul>
                    </div>
                  )
                }
                {
                  appointment.relatedAppointments && appointment.relatedAppointments.length > 0 && (
                    <div className="box-detail business">
                      <span className="title-b-detail">Extention Group Details</span>
                      <ul className="list-title">
                        <li className="item-title">
                          <span className="name-item">Extension 1
                              <span className="two-dots">:</span>
                          </span>
                          <span className="text-right-input">{appointment.extensionGroupDetails.extension1}</span>
                        </li>
                        <li className="item-title">
                          <span className="name-item">Extension 2
                              <span className="two-dots">:</span>
                          </span>
                          <span className="text-right-input">{appointment.extensionGroupDetails.extension2}</span>
                        </li>
                        <li className="item-title">
                          <span className="name-item">Extension 3
                              <span className="two-dots">:</span>
                          </span>
                          <span className="text-right-input">{appointment.extensionGroupDetails.extension3}</span>
                        </li>
                        <li className="item-title">
                          <span className="name-item">Extension 4
                              <span className="two-dots">:</span>
                          </span>
                          <span className="text-right-input">{appointment.extensionGroupDetails.extension4}</span>
                        </li>
                      </ul>
                    </div>
                  )
                }
                {
                  appointment.orderDetails && appointment.orderDetails.length > 0 && appointment.orderDetails.map((item, index) => (
                    <div className="box-detail" key={`order-details_${index}`}>
                      <span className="title-b-detail">{index === 0 ? 'Order Details' : index > 0 && 'Secondary Details'}</span>
                      <ul className="list-title">
                        <li className="item-title">
                          <span className="name-item">IP Network FNN
                              <span className="two-dots">:</span>
                          </span>
                          <span className="text-right-input">{item.ipNetworkFNN}</span>
                        </li>
                        <li className="item-title">
                          <span className="name-item">Primary Order#
                              <span className="two-dots">:</span>
                          </span>
                          <span className="text-right-input">{item.primaryOrderNumber}</span>
                        </li>
                        <li className="item-title">
                          <span className="name-item">Ordering System
                              <span className="two-dots">:</span>
                          </span>
                          <span className="text-right-input">{item.orderingSystem}</span>
                        </li>
                      </ul>
                    </div>
                  ))
                }
                {
                  appointment.relatedAppointments && appointment.relatedAppointments.length > 0 && (
                    <div className="box-detail business">
                      <span className="first-title">Related Appointments</span>
                      {
                        appointment.relatedAppointments.map((item, index) => (
                          <div className="related-appointment-item" key={`related-appointment_${index}`}>
                            <span className="title-b-detail">Related Appointments #{index + 1}</span>
                            <ul className="list-title">
                              <li className="item-title">
                                <span className="name-item">Related Appointment Category
                                  <span className="two-dots">:</span>
                                </span>
                                <span className="text-right-input">{item.relatedAppointmentCategory}</span>
                              </li>
                              <li className="item-title">
                                <span className="name-item">Related Appointment Location
                                  <span className="two-dots">:</span>
                                </span>
                                <span className="text-right-input">{item.relatedAppointmentLocation}</span>
                              </li>
                              <li className="item-title">
                                <span className="name-item">Related Appointment ID
                                  <span className="two-dots">:</span>
                                </span>
                                <span className="text-right-input">{item.relatedAppointmentId}</span>
                              </li>
                            </ul>
                          </div>
                        ))
                      }
                    </div>
                  )
                }
              </div>
            </div>
            <div className='bottom-btn'>
              <button className='btn-cancel'>Allocate</button>
              <button className='btn-cancel next'>
                Reject
              </button>
            </div>
          </div>
          {/* end/cover content  */}
          <div className={`cover-content ${!boxDetail[1].isShow ? 'collapse-ver' : ''}`}>
            <div className='top-title'>
              <span className='title-box'>Customer & Contacts</span>
              <div className='right-top'>
                <span className='click-expand' onClick={() => showDetail(1, !boxDetail[1].isShow)}>
                  <img src={icBlueExpand} alt='icon' />
                </span>
              </div>
            </div>
            <div className='box-content'>
              {
                appointment.customerAndContacts && (
                  <div className='content-box'>
                    <div className='box-detail'>
                      <span className='title-b-detail'>Customer Details</span>
                      <ul className='list-title'>
                        <li className='item-title'>
                          <span className='name-item'>
                            Customer Name
                          <span className='two-dots'>:</span>
                          </span>
                          <span className='text-right-input'>{appointment.customerAndContacts.customerDetails.customerName}</span>
                        </li>
                        <li className='item-title'>
                          <span className='name-item'>
                            Customer Type
                          <span className='two-dots'>:</span>
                          </span>
                          <span className='text-right-input'>{appointment.customerAndContacts.customerDetails.customerType}</span>
                        </li>
                        <li className='item-title'>
                          <span className='name-item'>
                            Customer ID
                          <span className='two-dots'>:</span>
                          </span>
                          <span className='text-right-input'>{appointment.customerAndContacts.customerDetails.customerId}</span>
                        </li>
                        <li className='item-title'>
                          <span className='name-item'>
                            Is customer offshore consenting?
                          <span className='two-dots'>:</span>
                          </span>
                          <span className='text-right-input'>{appointment.customerAndContacts.customerDetails.isCustomerOffShoreConsenting}</span>
                        </li>
                        <li className='item-title'>
                          <span className='name-item'>
                            Customer Address
                          <span className='two-dots'>:</span>
                          </span>
                          <span className='text-right-input'>
                            {appointment.customerAndContacts.customerDetails.addressLine1},
                            {appointment.customerAndContacts.customerDetails.addressLine2},
                            {appointment.customerAndContacts.customerDetails.addressLine3}
                          </span>
                        </li>
                        <li className='item-title'>
                          <span className='name-item'>
                            State
                          <span className='two-dots'>:</span>
                          </span>
                          <span className='text-right-input'>{appointment.customerAndContacts.customerDetails.state}</span>
                        </li>
                      </ul>
                    </div>
                    <div className='box-detail'>
                      <span className='first-title'>Customer Contact(s)</span>
                      {
                        appointment.customerAndContacts.customerContacts.map((item, index) => (
                          <div className="customer-contact-item" key={`customer-contact-item__${index}`}>
                            <span className='title-b-detail'>{item.title}</span>
                            <ul className='list-title'>
                              <li className='item-title'>
                                <span className='name-item'>
                                  First Name
                                  <span className='two-dots'>:</span>
                                </span>
                                <span className='text-right-input'>{item.firstName}</span>
                              </li>
                              <li className='item-title'>
                                <span className='name-item'>
                                  Last Name
                                  <span className='two-dots'>:</span>
                                </span>
                                <span className='text-right-input'>{item.lastName}</span>
                              </li>
                              <li className='item-title'>
                                <span className='name-item'>
                                  Phone Number
                                  <span className='two-dots'>:</span>
                                </span>
                                <span className='text-right-input'>{item.phoneNumber}</span>
                              </li>
                              <li className='item-title'>
                                <span className='name-item'>
                                  Alternative Phone Number
                                  <span className='two-dots'>:</span>
                                </span>
                                <span className='text-right-input'>{item.alternativePhoneNumber}</span>
                              </li>
                              <li className='item-title'>
                                <span className='name-item'>
                                  Email Address
                                <span className='two-dots'>:</span>
                                </span>
                                <span className='text-right-input'>{item.emailAddress}</span>
                              </li>
                            </ul>
                          </div>
                        ))
                      }
                    </div>
                    <div className='box-detail'>
                      <span className='first-title'>Tecams Contact(s)</span>
                      {
                        appointment.customerAndContacts.tecamsContacts.map((item, index) => (
                          <div className="tecams-contact-item" key={`tecams-contact-item__${index}`}>
                            <span className='title-b-detail'>Primary Tecams Contact</span>
                            <ul className='list-title'>
                              <li className='item-title'>
                                <span className='name-item'>
                                  First Name
                                  <span className='two-dots'>:</span>
                                </span>
                                <span className='text-right-input'>{item.firstName}</span>
                              </li>
                              <li className='item-title'>
                                <span className='name-item'>
                                  Last Name
                                  <span className='two-dots'>:</span>
                                </span>
                                <span className='text-right-input'>{item.lastName}</span>
                              </li>
                              <li className='item-title'>
                                <span className='name-item'>
                                  Phone Number
                                  <span className='two-dots'>:</span>
                                </span>
                                <span className='text-right-input'>{item.phoneNumber}</span>
                              </li>
                              <li className='item-title'>
                                <span className='name-item'>
                                  Alternative Phone Number
                                  <span className='two-dots'>:</span>
                                </span>
                                <span className='text-right-input'>{item.alternativePhoneNumber}</span>
                              </li>
                              <li className='item-title'>
                                <span className='name-item'>
                                  Email Address
                                  <span className='two-dots'>:</span>
                                </span>
                                <span className='text-right-input'>{item.emailAddress}</span>
                              </li>
                              <li className='item-title'>
                                <span className='name-item'>
                                  User ID
                                  <span className='two-dots'>:</span>
                                </span>
                                <span className='text-right-input'>{item.userId}</span>
                              </li>
                            </ul>
                          </div>
                        ))
                      }
                    </div>
                  </div>
                )
              }
            </div>
            <div className='bottom-btn'>
              <button className='btn-cancel'>Allocate</button>
              <button className='btn-cancel next'>
                Reject
              </button>
            </div>
          </div>
          {/* end/cover content  */}
          <div className={`cover-content ${!boxDetail[2].isShow ? 'collapse-ver' : ''}`}>
            <div className='top-title'>
              <span className='title-box'>Appointment Tasks</span>
              <div className='right-top'>
                <span className='click-expand' onClick={() => showDetail(2, !boxDetail[2].isShow)}>
                  <img src={icBlueExpand} alt='icon' />
                </span>
              </div>
            </div>
            <div className='box-content'>
              <div className='content-box'>
                {
                  appointment.appointmentTasks && appointment.appointmentTasks.map((item, index) => (
                    <div className="task-item" key={`task-item__${index}`}>
                      <div className='box-detail business'>
                        <span className='title-b-detail'>Task No. {index + 1}</span>
                        <ul className='list-title'>
                          <li className='item-title'>
                            <span className='name-item'>
                              Work Category
                              <span className="two-dots">:</span>
                            </span>
                            <span className='text-right-input'>{item.workCategory}</span>
                          </li>
                          <li className='item-title'>
                            <span className='name-item'>
                              Type of work
                              <span className="two-dots">:</span>
                            </span>
                            <span className='text-right-input'>{item.typeOfWork}</span>
                          </li>
                        </ul>
                      </div>
                      {
                        item.typeOfWork === 'NBN Transition: Transition BIP NBN + IOM' ? (
                          <div className='box-detail'>
                            <span className='first-title'>Additional Task Details</span>
                            <span className='title-b-detail'>IOM MDN Details</span>
                            <ul className='list-title'>
                              <li className='item-title'>
                                <span className='name-item'>
                                  MDN Router Action</span>
                                <span className='text-right-input'>{item.additionalTaskDetails.mdnRouterAction}</span>
                              </li>
                              <li className='item-title'>
                                <span className='name-item'>
                                  Exiting MDN FNN</span>
                                <span className='text-right-input'>{item.additionalTaskDetails.exitingMdnFnn}</span>
                              </li>
                              <li className='item-title'>
                                <span className='name-item'>
                                  New MDN FNN</span>
                                <span className='text-right-input'>{item.additionalTaskDetails.newMdnFnn}</span>
                              </li>
                            </ul>
                          </div>
                        ) : (
                            <div className='box-detail'>
                              <span className='first-title'>Additional Task Details</span>
                              <span className='title-b-detail'>Extension Group Details</span>
                              <ul className='list-title'>
                                <li className='item-title'>
                                  <span className='name-item'>
                                    Ext Name 1</span>
                                  <span className='text-right-input'>{item.additionalTaskDetails.extName1}</span>
                                </li>
                                <li className='item-title'>
                                  <span className='name-item'>
                                    Ext Name 2</span>
                                  <span className='text-right-input'>{item.additionalTaskDetails.extName2}</span>
                                </li>
                                <li className='item-title'>
                                  <span className='name-item'>
                                    Ext Name 3</span>
                                  <span className='text-right-input'>{item.additionalTaskDetails.extName3}</span>
                                </li>
                              </ul>
                            </div>
                          )
                      }
                    </div>
                  ))
                }
              </div>
            </div>
            <div className='bottom-btn'>
              <button className='btn-cancel'>Allocate</button>
              <button className='btn-cancel next'>
                Reject
              </button>
            </div>
          </div>
          {/* end/cover content  */}
          <div className={`cover-content ${!boxDetail[3].isShow ? 'collapse-ver' : ''}`}>
            <div className='top-title'>
              <span className="title-box">{appointment.appointmentType === 'After Hours' ? 'Requested Appointment Date' : 'Preferred Appointment Date'}</span>
              <div className='right-top'>
                <span className='click-expand' onClick={() => showDetail(3, !boxDetail[3].isShow)}>
                  <img src={icBlueExpand} alt='icon' />
                </span>
              </div>
            </div>
            <div className='box-content'>
              <div className='content-box'>
                {
                  appointment.appointmentType === 'After Hours' && appointment.requestedAppointmentDate ? (
                    <div className='box-detail'>
                      <ul className='list-title'>
                        <li className='item-title'>
                          <span className='name-item'>
                            Start After Date/ Time
                          </span>
                          <span className='text-right-input'>{appointment.requestedAppointmentDate.startAfterDate.date} {moment(appointment.requestedAppointmentDate.startAfterDate.time).format('HH:mm')} {appointment.requestedAppointmentDate.startAfterDate.period}</span>
                        </li>
                        <li className='item-title'>
                          <span className='name-item'>
                            Start Before Date/ Time
                          </span>
                          <span className='text-right-input'>{appointment.requestedAppointmentDate.startBeforeDate.date} {moment(appointment.requestedAppointmentDate.startBeforeDate.time).format('HH:mm')} {appointment.requestedAppointmentDate.startBeforeDate.period}</span>
                        </li>
                      </ul>
                    </div>
                  ) : appointment.appointmentType === 'Business Hours' && appointment.preferredAppointment && (
                    <div className='box-detail business'>
                      <ul className='list-title'>
                        <li className='item-title'>
                          <span className='name-item'>
                            Selected date
                          </span>
                          <span className='text-right-input'>{appointment.preferredAppointment.startDate}</span>
                        </li>
                      </ul>
                    </div>
                  )
                }
                {
                  appointment.requestedAppointmentDate && appointment.requestedAppointmentDate.alternativeAppointmentWindows && appointment.requestedAppointmentDate.alternativeAppointmentWindows.length > 0 && (
                    <div className="box-detail">
                      <span className="first-title">Alternative Appointment Windows</span>
                      {
                        appointment.requestedAppointmentDate.alternativeAppointmentWindows.map((item, index) => (
                          <div className="alternative-appointment-window-item" key={`alternative-appointment-window-item__${index}`}>
                            <span className="title-b-detail">Alternative Appointment Window #{index + 1}
                            </span>
                            <div className="box-day after-hourse">
                              <ul className="list-title ">
                                <li className="item-title">
                                  <span className="name-item">Start After Date/ Time <span className="red-symbol">*</span></span>
                                  <span className='text-right-input'>{item.startAfterDate.date} {moment(item.startAfterDate.time).format('HH:mm')} {item.startAfterDate.period}</span>
                                </li>
                                <li className="item-title">
                                  <span className="name-item">Start Before Date/ Time <span className="red-symbol">*</span></span>
                                  <span className='text-right-input'>{item.startBeforeDate.date} {moment(item.startBeforeDate.time).format('HH:mm')} {item.startBeforeDate.period}</span>
                                </li>
                              </ul>
                            </div>
                          </div>
                        ))
                      }
                    </div>
                  )
                }
                <div className='box-detail'>
                  <span className='title-b-detail'>Notes</span>
                  <ul className='list-title'>
                    <li className='item-title'>
                      {
                        appointment.appointmentType === 'Business Hours' ? (
                          <span className='text-area'>{appointment.preferredAppointment ? appointment.preferredAppointment.notes : ''}{' '}</span>
                        ) : (
                            <span className='text-area'>{appointment.requestedAppointmentDate ? appointment.requestedAppointmentDate.notes : ''}{' '}</span>
                          )
                      }
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className='bottom-btn'>
              <button className='btn-cancel'>Allocate</button>
              <button className='btn-cancel next'>
                Reject
              </button>
            </div>
          </div>
          {/* end/cover content  */}
        </div>
      </div>
    )
  } else {
    return (
      <div
        style={{
          margin: 0,
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: "translateY(-50%)"
        }}
        className="d-flex justify-content-center align-items-center vertical-center"
      >
        <ReactLoading
          type="spin"
          color="rgba(0, 155, 255, 0.93)"
          height={50}
          width={50}
        />
      </div>
    );
  }


}

const mapStateToProps = ({ appointment }) => {
  return {
    appointment: { ...appointment }
  };
};

const mapDispatchToProps = {
  fetchAppointmentById
};

export default connect(mapStateToProps, mapDispatchToProps)(AppointmentDetails);
