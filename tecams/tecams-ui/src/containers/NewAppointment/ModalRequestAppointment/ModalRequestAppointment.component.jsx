import './ModalRequestAppointment.styles.scss';
import iconTick from 'assets/icons/ic-green-tick-modal.svg';

import React from 'react';
import Modal from 'react-bootstrap/Modal';
import { Link } from 'react-router-dom';
import moment from 'moment';

const ModalRequestAppointment = ({ appointment, appointmentID }) => {

  return (
    <Modal show dialogClassName='modal-reserve-apt' centered>
      <div className='modal-container'>
        <div className='group-title'>
          <span className='big-title'>Thank you!</span>
          <span className='status'>{
            appointment.appointmentType === 'Business Hours' ? 'Your appointment is reserved successfully' : 'Your appointment request has been submitted'
          }</span>
          {
            appointment.appointmentType === 'After Hours' && (
              <span className='sub-status'>Additional info about approval lorem ipsum dolor sit amet</span>
            )
          }
          <span className='btn-close center-child'>
            <img src={iconTick} alt='' />
          </span>
        </div>
        <div className='details'>
          <ul className='rows'>
            <li className='row-item'>
              <span className='row-item-name'>Appointment ID :</span>
              <span className='row-item-value'>{appointmentID}</span>
            </li>
            <li className='row-item'>
              <span className='row-item-name'>Priority</span>
              <span className='row-item-value'>{appointment.appointmentPriority}</span>
            </li>
            <li className='row-item'>
              <span className='row-item-name'>Type</span>
              <span className='row-item-value'>{appointment.appointmentType}</span>
            </li>
            <li className='row-item'>
              <span className='row-item-name'>Sub-type</span>
              <span className='row-item-value'>{appointment.appointmentSubType}</span>
            </li>
            <li className='row-item'>
              <span className='row-item-name'>When</span>
              <div className='wrap'>
                {
                  appointment.appointmentType === 'After Hours' ? (
                    <span className='row-item-value'>{moment().format('MMMM DD, YYYY HH:mm A')}</span>
                  ) : (
                    <span className='row-item-value'>{moment(appointment.startDate).format('MMMM DD, YYYY HH:mm A')} - {moment(appointment.endDate).format('HH:mm A')}</span>
                  )
                }
                <span className='row-item-value gray-text'>NSW Local Time</span>
              </div>
            </li>
          </ul>
        </div>
        <div className='buttons'>
          <Link to="/my-appointments" className='btn-back center-child'>Return Home</Link>
        </div>
      </div>
    </Modal>
  );
};

export default ModalRequestAppointment;
