import React, { useState } from 'react';
import './styles.scss';
import icAppointWhite from 'assets/icons/ic-appoint-white.svg';
import icAppointGreen from 'assets/icons/ic-appoint-green.svg';
import icCustomeGray from 'assets/icons/ic-custome-gray.svg';
import icCustomeWhite from 'assets/icons/ic-custome-white.svg';
import iCustomeGreen from 'assets/icons/ic-custome-green.svg';
import icTaskWhite from 'assets/icons/ic-task-white.svg';
import icTaskGray from 'assets/icons/ic-task-gray.svg';
import icTaskGreen from 'assets/icons/ic-task-green.svg';
import icGeviewWhite from 'assets/icons/ic-review-white.svg';
import icReviewGray from 'assets/icons/ic-review-gray.svg';
import AppointmentDetail from './AppointmentDetail/AppointmentDetail';
import CustomerAndContacts from './CustomerAndContacts/CustomerAndContacts';
import TaskAndDates from './TaskAndDates/TaskAndDates';
import ReviewDetails from './ReviewDetails/ReviewDetails';
import ModalCancelAppointment from './ModalCancelAppointment/ModalCancelAppointment.component';

const NewAppointment = () => {
  const [step, setStep] = useState(1);
  const [showModalCancel, setShowModalCancel] = useState(false);

  const changeStep = (stepContent) => {
    setStep(stepContent);
  }

  const cancelNewAppointment = () => {
    setShowModalCancel(true);
  }

  return (
    <div className='new-appointment'>
      <div className='top-page'>
        <div className="cover-top-page">
          <span className='title-page'>New Appointment</span>
        </div>
      </div>
      <div className='second-page'>

        <div className='top-s-page'>
          <ul className='list-timeline'>
            <li className={`item-list ${step === 1 ? 'active' : step > 1 && 'done'}`}>
              <div className='cover-image'>
                <img className='one' src={icAppointWhite} alt='icon' />
                <img className='two' src={icAppointWhite} alt='icon' />
                <img className='three' src={icAppointGreen} alt='icon' />
                <span className='name-list'>Appointment Details</span>
              </div>
              <span className='line-gray'></span>
            </li>
            <li className={`item-list ${step === 2 ? 'active' : step > 2 && 'done'}`}>
              <div className='cover-image'>
                <img className='one' src={icCustomeGray} alt='icon' />
                <img className='two' src={icCustomeWhite} alt='icon' />
                <img className='three' src={iCustomeGreen} alt='icon' />
                <span className='name-list'>Customer &amp; Contacts</span>
              </div>
              <span className='line-gray'></span>
            </li>
            <li className={`item-list ${step === 3 ? 'active' : step > 3 && 'done'}`}>
              <div className='cover-image'>
                <img className='one' src={icTaskGray} alt='icon' />
                <img className='two' src={icTaskWhite} alt='icon' />
                <img className='three' src={icTaskGreen} alt='icon' />
                <span className='name-list'>Tasks and Dates</span>
              </div>
              <span className='line-gray'></span>
            </li>
            <li className={`item-list ${step === 4 ? 'active' : step > 4 && 'done'}`}>
              <div className='cover-image'>
                <img className='one' src={icReviewGray} alt='icon' />
                <img className='two' src={icGeviewWhite} alt='icon' />
                <img className='three' src={icGeviewWhite} alt='icon' />
                <span className='name-list'>Review Details</span>
              </div>
              <span className='line-gray'></span>
            </li>
          </ul>
        </div>
        {/* end/top s page  */}
        <div className='cover-tab after-ver'>
          {
            step === 1 ? <AppointmentDetail changeStep={changeStep} cancel={cancelNewAppointment} /> :
              step === 2 ? <CustomerAndContacts changeStep={changeStep} cancel={cancelNewAppointment} /> :
                step === 3 ? <TaskAndDates changeStep={changeStep} cancel={cancelNewAppointment} /> : <ReviewDetails cancel={cancelNewAppointment} />
          }
        </div>
        {/* end/cover tab  */}
      </div>
      {/* end/cover page  */}
      {showModalCancel && <ModalCancelAppointment show={showModalCancel} onHide={() => setShowModalCancel(false)} />}
    </div>
  );
}

export default NewAppointment;
