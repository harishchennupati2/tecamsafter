import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import './styles.scss'
import icGreenTick from 'assets/icons/ic-green-tick-modal.svg'
import icWhiteRequest from 'assets/icons/ic-white-reserve.svg'
import icGrayCalen from 'assets/icons/ic-gray-calen.svg'
import icWhiteNext from 'assets/icons/ic-white-arrow-next.svg'
import DatePicker from 'react-datepicker'
import CalendarMultipleActive from '../../../shared/components/CalendarMultipleActive/CalendarMultipleActive.component'
import Modal from 'react-bootstrap/Modal'
import moment from 'moment'
import { getSlots } from '../../../redux/actions/appointment'
import { DATE_FORMAT } from '../../../config/constants'
import ReactLoading from 'react-loading'

const ModalCalendar = ({ appointment, reserveAppointment, availableSlots, getSlots, show, onHide }) => {
  const dateFormat = 'MM/DD/YYYY'
  const [showModal] = useState(true)
  const [preferredDate, setPreferredDate] = useState({
    date: new Date(moment(appointment.preferredAppointment.startDate).format(dateFormat))
  })
  const [startDate, setStartDate] = useState(null)
  const [selectedSlot, setSelectedSlot] = useState({})
  const [showSearch, setShowSearch] = useState(false)
  const timeSlots = availableSlots.slots.map(slot => moment(slot.startDate, DATE_FORMAT).toDate())
  const groupedTimeSlots = _.groupBy(availableSlots.slots, (slot) => moment(slot.startDate).format(dateFormat))

  useEffect(() => {
    getSlots(appointment)
    if (preferredDate.date !== appointment.preferredAppointment.startDate) {
      setPreferredDate({ date: new Date(moment(appointment.preferredAppointment.startDate).format(dateFormat)) })
    }
  }, [appointment])
  const handleClose = () => onHide()

  const renderDayContents = (day, date) => {
    return <span>{new Date(date).getDate()}</span>
  }

  const onChangeDate = (date, type) => {
    if (type !== 'new') {
      setPreferredDate({
        date: moment(date).toDate()
      })
    } else {
      setStartDate(date)
    }
  }

  const onSearchAvailableSlots = () => {
    setPreferredDate({
      date: startDate
    })
    getSlots({ ...appointment, preferredAppointment: { ...appointment.preferredAppointment, startDate: startDate } })
    setShowSearch(false)
  }

  const onSlotSelected = (slot) => {
    setSelectedSlot({ ...slot })
  }

  const onReserveAppointment = () => {
    reserveAppointment({
      ...appointment,
      slotID: selectedSlot.slotID,
      startDate: selectedSlot.startDate,
      endDate: selectedSlot.endDate
    })
  }

  const currentDateSlots = groupedTimeSlots[moment(preferredDate.date).format(dateFormat)]
  const amPmGroupedSlots = _.groupBy(currentDateSlots, (date) => moment(date.startDate).format('A'))

  return !availableSlots.fetched ? (
    <div
      style={{
        margin: 0,
        position: 'fixed',
        top: '50vh',
        left: '50%'
      }}
      className="d-flex justify-content-center align-items-center vertical-center"
    >
      <ReactLoading
        type="spin"
        color="rgba(0, 155, 255, 0.93)"
        height={50}
        width={50}
      />
    </div>
  ) : (
    <Modal show={showModal} onHide={handleClose} dialogClassName='modal-reserve-apt' centered>
      <div className="modal-calen">
        <div className="cover-modal">

          <div className="left-modal">
            <div className="calen-here">
              <CalendarMultipleActive startDate={preferredDate.date} timeSlots={timeSlots} setStartDate={onChangeDate}/>
            </div>
          </div>
          <div className="right-modal">
            <span
              className="title-day">{moment(preferredDate.date).format('DD MMMM YYYY')}</span>
            {
              !currentDateSlots && (
                <div className="group-slot-available">
                  <span className="no-slot-text">No slots available on your preferred date</span>
                  {
                    !showSearch &&
                    <div className="group-unmatching">
                      <span className='hint'>
                        Can't find suitable time slot? <span className='click-here' onClick={() => setShowSearch(true)}>Click here?</span>
                      </span>
                      {
                        timeSlots.length > 0 && (
                          <div className="box-avaliable">
                            <span className="title-box">Next Available Slots</span>
                            <ul className="list-avaliable">
                              {
                                Object.keys(groupedTimeSlots).map((item, index) => (
                                  <li className="item-list" key={`item___${index}`}>
                                    <button className="day-march"
                                            onClick={() => onChangeDate(item)}>{moment(item).format('DD MMM')}</button>
                                  </li>
                                ))
                              }
                            </ul>
                          </div>
                        )
                      }
                      <span className="txt-select">Or select blue dates on calendar to view available time slots.</span>
                    </div>
                  }
                  {
                    showSearch && (
                      <div className='box-input'>
                        <span className='title-input'>New Preferred Date</span>
                        <div className='cover-input'>
                          <DatePicker
                            renderDayContents={renderDayContents}
                            selected={startDate ? new Date(moment(startDate).format(dateFormat)) : null}
                            onChange={date => onChangeDate(date, 'new')}
                            placeholderText="DD/MM/YYYY"
                            dateFormat="dd/MM/yyyy"
                          />
                          <span className='click-calen'>
                            <img src={icGrayCalen} alt='icon'/>
                          </span>
                        </div>
                      </div>
                    )
                  }
                </div>
              )
            }
            {
              (currentDateSlots && currentDateSlots.length > 0) && (
                <div className="box-found">
                  <span className="found-slot-text">We’ve found time slots available on your preferred date:</span>
                  <ul className="list-title">
                    {amPmGroupedSlots.AM &&
                    <li className="item-title">
                      <span className="title-item">AM</span>
                      <ul className="list-sub">
                        {
                          amPmGroupedSlots.AM.map((slot, index) => (
                            <li className={`item-sub ${selectedSlot.slotID === slot.slotID && 'active'}`}
                                key={`item__${index}`}
                                onClick={() => onSlotSelected(slot)}>
                              <span
                                className="text-time">{moment(slot.startDate).format('hh:mm A')} - {moment(slot.endDate).format('hh:mm A')}</span>
                              <span className="click-green-tick">
                                <img className="one" src={icGreenTick} alt="icon"/>
                              </span>
                            </li>
                          ))
                        }
                      </ul>
                    </li>
                    }
                    {amPmGroupedSlots.PM &&
                    <li className="item-title">
                      <span className="title-item">PM</span>
                      <ul className="list-sub">
                        {
                          amPmGroupedSlots.PM.map((slot, index) => (
                            <li className={`item-sub ${selectedSlot.slotID === slot.slotID && 'active'}`} key={`item__${index}`}
                                onClick={() => onSlotSelected(slot)}>
                              <span
                                className="text-time">{moment(slot.startDate).format('hh:mm A')} - {moment(slot.endDate).format('hh:mm A')}</span>
                              <span className="click-green-tick">
                                <img className="one" src={icGreenTick} alt="icon"/>
                              </span>
                            </li>
                          ))
                        }
                      </ul>
                    </li>
                    }
                  </ul>
                </div>
              )
            }
            {/* end /group-unmatching */}
            <div className="bottom-btn">
              <button className="btn-cancel" onClick={onHide}>cancel</button>
              {
                (showSearch && !currentDateSlots) && (
                  <button className='btn-cancel next' onClick={() => onSearchAvailableSlots()}>
                    Search Available Slots
                    <img className='one' src={icWhiteNext} alt='icon'/>
                  </button>
                )
              }
              {
                (currentDateSlots && currentDateSlots.length > 0) && (
                  <button className="btn-cancel request after-bussines"
                          disabled={!selectedSlot.startDate || !selectedSlot.endDate}
                          onClick={onReserveAppointment}>Reserve Appointment
                    <img className="one" src={icWhiteRequest} alt="icon"/>
                  </button>
                )
              }
            </div>
          </div>
        </div>
      </div>
    </Modal>
  )
}

const mapStateToProps = ({ availableSlots }) => {
  return {
    availableSlots
  }
}

const mapDispatchToProps = {
  getSlots
}
export default connect(mapStateToProps, mapDispatchToProps)(ModalCalendar)
