import React, { useState, useEffect } from 'react'
import icWhiteNext from 'assets/icons/ic-white-arrow-next.svg'
import icGrayCalen from 'assets/icons/ic-gray-calen.svg'
import icRedWarning from 'assets/icons/ic-red-warning.svg'
import icRedCancel from 'assets/icons/ic-red-delete.svg'
import Dropdown from '../../../shared/components/Dropdown/Dropdown.component'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import * as constants from '../../../config/constants'
import * as _ from 'lodash'
import moment from 'moment'
import { fetchAvailableWorkTypes, fetchRequiredExtensionTypesForWorkType } from 'redux/actions/appointment'
import { connect } from 'react-redux'
import Input from '../../../shared/components/Input/Input.component'
import { INPUT_TYPES, MANDATORY_INDICATORS } from '../../../config/constants'

const TaskAndDates = ({
  changeStep,
  workTypes,
  fetchAvailableWorkTypes,
  workTypeRequiredExtensions,
  cancel,
  fetchRequiredExtensionTypesForWorkType
}) => {
  const [data] = useState(JSON.parse(localStorage.getItem('appointment')))
  const [appointment, setAppointment] = useState(
    data.appointmentTasks ? {
      ...JSON.parse(localStorage.getItem('appointment'))
    } : {
      ...data,
      appointmentTasks: [{
        additionalTaskDetails: {}
      }],
      preferredAppointment: {
        startDate: moment().startOf('day').add(3, 'days').format('MM/DD/YYYY')
      },
      requestedAppointmentDate: {
        startAfterDate: {
          period: 'AM'
        },
        startBeforeDate: {
          period: 'AM'
        },
        alternativeAppointmentWindows: []
      }
    }
  )
  const [periodOptions] = useState(constants.PERIODS)
  const [submitted, setSubmitted] = useState(false)
  const [showErrorDate, setShowErrorDate] = useState(false)

  useEffect(
    () => {
      if (!workTypes || workTypes.length === 0) {
        fetchAvailableWorkTypes()
      }
      _.forEach(appointment.appointmentTasks, (task) => {
        if (task.work && !workTypeRequiredExtensions[task.work.workTypeID]) {
          fetchRequiredExtensionTypesForWorkType(
            appointment.appointmentType,
            appointment.appointmentSubType,
            task.workCategory,
            task.work.workTypeID
          )
        }
      })
    }
  )

  const renderDayContents = (day, date) => {
    return <span>{new Date(date).getDate()}</span>
  }

  /**
   * on change value dropdown
   * @param {string | Object} value value dropdown callback
   * @param {string} label label
   */
  const onChangeValue = (value, label, labelObject, taskIndex, labelObjectNested) => {
    if (labelObject && labelObjectNested === undefined && taskIndex === undefined) {
      setAppointment({
        ...appointment,
        [label]: {
          ...appointment[label],
          [labelObject]: value
        }
      })
    } else if (labelObject && labelObjectNested === undefined && taskIndex !== undefined) {
      const data = [...appointment[label]]
      data[taskIndex] = { ...data[taskIndex], [labelObject]: value }
      setAppointment({
        ...appointment,
        [label]: [
          ...data
        ]
      })
    } else if (labelObject === undefined && labelObjectNested === undefined && taskIndex !== undefined) {
      const data = [...appointment[label]]
      data[taskIndex] = { ...data[taskIndex], ...value }
      setAppointment({
        ...appointment,
        [label]: [
          ...data
        ]
      })
    } else if (labelObject && labelObjectNested !== undefined && taskIndex !== undefined) {
      const data = [...appointment[label]]
      data[taskIndex] = {
        ...data[taskIndex],
        [labelObjectNested]: {
          ...data[taskIndex][labelObjectNested],
          [labelObject]: value
        }
      }
      setAppointment({
        ...appointment,
        [label]: data
      })
    }

  }

  const onChangeDate = (value, label, type, labelObject, taskIndex) => {
    if (type === undefined && labelObject === undefined && taskIndex === undefined) {
      setAppointment({
        ...appointment,
        preferredAppointment: {
          ...appointment.preferredAppointment,
          [label]: moment(value).format('MM/DD/YYYY')
        }
      })
    } else if (type !== undefined && labelObject === undefined && taskIndex === undefined) {
      setAppointment({
        ...appointment,
        requestedAppointmentDate: {
          ...appointment.requestedAppointmentDate,
          [label]: {
            ...appointment.requestedAppointmentDate[label],
            [type]: value
          }
        }
      })
      if ((label === 'startAfterDate' || label === 'startBeforeDate') && type === 'date') {
        const today = new Date()
        const startDate = new Date(formatDate(value, 'time'))
        const endDate = new Date(moment(value).format('MM/DD/YYYY'))
        if (startDate.getTime() - today.getTime() >= 358552701 || endDate.getTime() - today.getTime() >= 358552701) {
          setShowErrorDate(false)
        } else {
          setShowErrorDate(true)
        }
      }
    } else if (type !== undefined && labelObject !== undefined && taskIndex !== undefined) {
      const data = [...appointment.requestedAppointmentDate[labelObject]]
      data[taskIndex] = { ...data[taskIndex], [label]: { ...data[taskIndex][label], [type]: value } }
      setAppointment({
        ...appointment,
        requestedAppointmentDate: {
          ...appointment.requestedAppointmentDate,
          [labelObject]: [...data]
        }
      })
    }
  }

  /**
   * add more task
   */
  const addMoreTask = () => {
    setAppointment({
      ...appointment,
      appointmentTasks: [...appointment.appointmentTasks, {}]
    })
  }

  /**
   * delete item order
   * @param {number} taskIndex taskIndex order
   */
  const removeTask = (taskIndex) => {
    setAppointment({
      ...appointment,
      appointmentTasks: appointment.appointmentTasks.filter((item, idx) => taskIndex !== idx)
    })
  }

  const addAlternativeAppointmentWindow = () => {
    setAppointment({
      ...appointment,
      requestedAppointmentDate: {
        ...appointment.requestedAppointmentDate,
        alternativeAppointmentWindows: [...appointment.requestedAppointmentDate.alternativeAppointmentWindows, {
          startAfterDate: {
            period: 'AM'
          },
          startBeforeDate: {
            period: 'AM'
          }
        }]
      }
    })
  }

  /**
   * delete item alternative appointment window
   * @param {number} taskIndex taskIndex alternative appointment window
   */
  const removeAlternativeAppointmentWindow = (taskIndex) => {
    setAppointment({
      ...appointment,
      requestedAppointmentDate: {
        ...appointment.requestedAppointmentDate,
        alternativeAppointmentWindows: appointment.requestedAppointmentDate.alternativeAppointmentWindows.filter((item, idx) => taskIndex !== idx)
      }
    })
  }

  const formatDate = (date, type) => {
    if (type === 'date') {
      return moment(date).format('MM/DD/YYYY')
    } else {
      return moment(date).format('MM/DD/YYYY HH:mm')
    }
  }

  /**
   * next step
   */
  const onNextStep = () => {
    setSubmitted(true)
    let check = false
    _.forEach(appointment.appointmentTasks, (item) => {
      if (!item.work) {
        check = true
        return
      }

      const extensions = workTypeRequiredExtensions[item.work.workTypeID]
      _.forEach(extensions, (key, value) => {
        if (MANDATORY_INDICATORS[value.mandatoryIndicator] && !item.additionalTaskDetails[value.name]) {
          check = true
        }
      })
    })

    if (appointment.appointmentType === 'Business Hours' && !appointment.preferredAppointment.startDate) {
      check = true
      return
    }
    if (appointment.appointmentType === 'After Hours' && (
      !appointment.requestedAppointmentDate.startAfterDate.date || !appointment.requestedAppointmentDate.startAfterDate.time || !appointment.requestedAppointmentDate.startAfterDate.period ||
      !appointment.requestedAppointmentDate.startBeforeDate.date || !appointment.requestedAppointmentDate.startBeforeDate.time || !appointment.requestedAppointmentDate.startBeforeDate.period
    )) {
      check = true
      return
    }
    if (!check) {
      changeStep(4)
      localStorage.setItem('appointment', JSON.stringify(appointment))
    }
  }

  const getRequiredMins = (task) => {
    const requiredMins = _.get(task, 'work.skillset.requiredMinutes')
    return requiredMins ? `${requiredMins} mins` : 'TBA'
  }

  const groupedWorkTypes = _.groupBy(workTypes, 'productFamily')
  const workCategoryOptions = Object.keys(groupedWorkTypes).map(category => ({ name: category }))
  const groupedWorkTypeRequiredExtensions = {}
  _.forEach(_.keys(workTypeRequiredExtensions), (workTypeID) => {
    const extensions = workTypeRequiredExtensions[workTypeID]
    groupedWorkTypeRequiredExtensions[workTypeID] = extensions ?
      _.groupBy(extensions, 'extensionType') : {}
  })

  const WorkTypeExtension = ({ task, taskIndex }) => {
    const groupedExtensions = groupedWorkTypeRequiredExtensions[task.work.workTypeID] || {}

    return Object.keys(groupedExtensions).length > 0 && (
      <div className="box-detail">
        <span className="first-title">Additional Task Details</span>
        {
          Object.keys(groupedExtensions).map(key => (
              <React.Fragment key={`extension-${task.work.workTypeID}-${key}`}>
                <span className='title-b-detail'>{key}</span>
                <ul className='list-title'>
                  {_.map(groupedExtensions[key], ({
                    inputType, extensionName, extensionNameDescription, mandatoryIndicator, valuesList, maxLength
                  }) => {
                    const currentExtensions = _.get(task, `additionalTaskDetails[${key}]`, {})
                    return (
                      <li className='item-title' key={`extension-${key}-${extensionName}`}>
                        <Input
                          type={INPUT_TYPES[inputType]}
                          name={extensionName}
                          description={extensionNameDescription}
                          isMandatory={MANDATORY_INDICATORS[mandatoryIndicator]}
                          options={valuesList && valuesList.map(value => ({ name: value }))}
                          maxLength={maxLength}
                          hasError={!currentExtensions[extensionName] && submitted}
                          value={currentExtensions[extensionName]}
                          selectedValue={currentExtensions[extensionName]}
                          onChange={inputType === INPUT_TYPES.LOV ?
                            (name) => onChangeValue({
                              ...currentExtensions,
                              [extensionName]: name
                            }, 'appointmentTasks', key, taskIndex, 'additionalTaskDetails')
                            : (e) => onChangeValue({
                              ...currentExtensions,
                              [extensionName]: e.target.value
                            }, 'appointmentTasks', key, taskIndex, 'additionalTaskDetails')
                          }
                        />
                      </li>)
                  })}
                </ul>
                <span className="line-bottom"/>

              </React.Fragment>
            )
          )}
      </div>
    )
  }

  return (
    <div className="tab-item tasks">
      <div className="cover-content edit-ver">
        <div className="top-title">
          <span className="title-box">Appointment Tasks</span>
        </div>
        <div className="box-content">
          <div className="content-box">
            {
              _.map(appointment.appointmentTasks, (task, taskIndex) => (
                <div className="group-task" key={`task__${taskIndex}`}>
                  <div className="box-detail">
                    <span className="title-b-detail">Task No. {taskIndex + 1}
                      <span className="sub-title">Estimated Duration : {getRequiredMins(task)}</span>
                      {
                        taskIndex >= 1 && (
                          <span className="click-delete" onClick={() => removeTask(taskIndex)}>
                            <img className="one" src={icRedCancel} alt="icon"/>
                          </span>
                        )
                      }
                    </span>
                    <ul className="list-title">
                      <li className="item-title">
                        <Input
                          type={INPUT_TYPES.LOV}
                          name='Work Category'
                          isMandatory
                          options={workCategoryOptions}
                          hasError={!task.workCategory && submitted}
                          selectedValue={task.workCategory}
                          onChange={
                            (value) => onChangeValue(
                              { workCategory: value, typeOfWork: '', work: null },
                              'appointmentTasks',
                              undefined,
                              taskIndex
                            )}
                        />
                      </li>
                      <li className="item-title">
                        <Input
                          name='Type of work'
                          type={INPUT_TYPES.LOV}
                          disabled={!task.workCategory}
                          hasError={!task.typeOfWork && submitted}
                          isMandatory
                          selectedValue={task.typeOfWork}
                          options={
                            _.get(groupedWorkTypes, task.workCategory, [])
                            .map(work => ({ ...work, name: work.workTypeDescription }))
                          }
                          onChange={(value, label, i) => {
                            const work = groupedWorkTypes[task.workCategory][i]
                            fetchRequiredExtensionTypesForWorkType(
                              appointment.appointmentType,
                              appointment.appointmentSubType,
                              task.workCategory,
                              work.workTypeID
                            )
                            onChangeValue({ work, typeOfWork: value }, 'appointmentTasks', undefined, taskIndex)
                          }}
                        />
                      </li>
                    </ul>
                  </div>
                  {task.work &&
                  WorkTypeExtension({ task, taskIndex })
                  }
                </div>
              ))
            }
            <div className="bottom-box">
              <button className="btn-contact" onClick={() => addMoreTask()}>+ More Task(s)</button>
            </div>
          </div>
          <div className="content-box">
            <div className="box-detail">
              <span
                className="title-b-detail">{appointment.appointmentType === 'After Hours' ? 'Requested Appointment Date' : 'Preferred Appointment Date'}</span>
              {
                appointment.appointmentType === 'After Hours' ? (
                  <div className={`box-day after-hourse ${showErrorDate ? 'error-ver' : ''}`}>
                    <ul className="list-title ">
                      <li className="item-title">
                        <span className="name-item">Start After Date/ Time <span className="red-symbol">*</span></span>
                        <div className="box-calen">
                          <div className="cover-input calendar">
                            <DatePicker
                              className={`box-datepicker ${!appointment.requestedAppointmentDate.startAfterDate.date && submitted ? 'error-ver' : ''}`}
                              renderDayContents={renderDayContents}
                              selected={
                                appointment.requestedAppointmentDate.startAfterDate && appointment.requestedAppointmentDate.startAfterDate.date ?
                                  new Date(moment(appointment.requestedAppointmentDate.startAfterDate.date).format('MM/DD/YYYY')) : null
                              }
                              onChange={date => onChangeDate(formatDate(date, 'date'), 'startAfterDate', 'date')}
                              placeholderText="DD/MM/YYYY"
                              dateFormat="dd/MM/yyyy"
                            />
                            <span className="click-calen">
                              <img src={icGrayCalen} alt="icon"/>
                            </span>
                          </div>
                          <div className="cover-input hours">
                            <DatePicker
                              className={`box-datepicker ${!appointment.requestedAppointmentDate.startAfterDate.time && submitted ? 'error-ver' : ''}`}
                              selected={appointment.requestedAppointmentDate.startAfterDate && appointment.requestedAppointmentDate.startAfterDate.time ?
                                new Date(moment(appointment.requestedAppointmentDate.startAfterDate.time).format('MM/DD/YYYY HH:mm')) : null
                              }
                              onChange={date => onChangeDate(formatDate(date, 'time'), 'startAfterDate', 'time')}
                              showTimeSelect
                              showTimeSelectOnly
                              timeIntervals={15}
                              timeCaption="Time"
                              dateFormat="hh:mm"
                              placeholderText="HH:MM"
                            />
                          </div>
                          <div className="cover-input drop-down">
                            <Dropdown
                              classes={`period ${appointment.requestedAppointmentDate.startAfterDate && !appointment.requestedAppointmentDate.startAfterDate.period && submitted && 'error-ver'}`}
                              label='requestedAppointmentDate'
                              selectedValue={appointment.requestedAppointmentDate.startAfterDate && appointment.requestedAppointmentDate.startAfterDate ? appointment.requestedAppointmentDate.startAfterDate.period : ''}
                              options={periodOptions}
                              onChangeDropdown={(value, label) => onChangeDate(value, 'startAfterDate', 'period')}/>
                          </div>
                        </div>
                      </li>
                      <li className="item-title">
                        <span className="name-item">Start Before Date/ Time <span className="red-symbol">*</span></span>
                        <div className="box-calen">
                          <div className="cover-input calendar">
                            <DatePicker
                              className={`box-datepicker ${!appointment.requestedAppointmentDate.startBeforeDate.date && submitted ? 'error-ver' : ''}`}
                              renderDayContents={renderDayContents}
                              selected={
                                appointment.requestedAppointmentDate.startBeforeDate && appointment.requestedAppointmentDate.startBeforeDate.date ?
                                  new Date(moment(appointment.requestedAppointmentDate.startBeforeDate.date).format('MM/DD/YYYY')) : null
                              }
                              onChange={date => onChangeDate(formatDate(date, 'date'), 'startBeforeDate', 'date')}
                              placeholderText="DD/MM/YYYY"
                              dateFormat="dd/MM/yyyy"
                            />
                            <span className="click-calen">
                              <img src={icGrayCalen} alt="icon"/>
                            </span>
                          </div>
                          <div className="cover-input hours">
                            <DatePicker
                              className={`box-datepicker ${!appointment.requestedAppointmentDate.startBeforeDate.time && submitted ? 'error-ver' : ''}`}
                              selected={appointment.requestedAppointmentDate.startBeforeDate && appointment.requestedAppointmentDate.startBeforeDate.time ? new Date(moment(appointment.requestedAppointmentDate.startBeforeDate.time).format('MM/DD/YYYY HH:mm')) : null}
                              onChange={date => onChangeDate(formatDate(date, 'time'), 'startBeforeDate', 'time')}
                              showTimeSelect
                              showTimeSelectOnly
                              timeIntervals={15}
                              timeCaption="Time"
                              dateFormat="hh:mm"
                              placeholderText="HH:MM"
                            />
                          </div>
                          <div className="cover-input drop-down">
                            <Dropdown
                              classes={`period ${appointment.requestedAppointmentDate.startBeforeDate && !appointment.requestedAppointmentDate.startBeforeDate.period && submitted && 'error-ver'}`}
                              label='requestedAppointmentDate'
                              selectedValue={appointment.requestedAppointmentDate.startBeforeDate && appointment.requestedAppointmentDate.startBeforeDate ? appointment.requestedAppointmentDate.startBeforeDate.period : ''}
                              options={periodOptions}
                              onChangeDropdown={(value, label) => onChangeDate(value, 'startBeforeDate', 'period')}/>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <div className="red-war">
                      <img src={icRedWarning} alt="icon"/>
                      <p className="text">For standard priority request. Start Date/ Time must be greater than 5 days
                        from current date/time</p>
                    </div>
                  </div>
                ) : (
                  <ul className="list-title">
                    <li className="item-title">
                      <span className="name-item">Start Date <span className="red-symbol">*</span></span>
                      <div className="cover-input calen">
                        <DatePicker
                          className={`box-datepicker ${!appointment.preferredAppointment.startDate && submitted ? 'error-ver' : ''}`}
                          renderDayContents={renderDayContents}
                          selected={
                            appointment.preferredAppointment.startDate ?
                              new Date(appointment.preferredAppointment.startDate)
                              : null
                          }
                          minDate={moment().startOf('day').add(3, 'days').toDate()}
                          onChange={date => onChangeDate(date, 'startDate')}
                          placeholderText="DD/MM/YYYY"
                          dateFormat="dd/MM/yyyy"
                        />
                        <span className="click-calen">
                            <img src={icGrayCalen} alt="icon"/>
                          </span>
                      </div>
                    </li>
                  </ul>
                )
              }
            </div>
            {
              appointment.requestedAppointmentDate && appointment.requestedAppointmentDate.alternativeAppointmentWindows && appointment.requestedAppointmentDate.alternativeAppointmentWindows.length > 0 && (
                <div className="box-detail">
                  <span className="first-title">Alternative Appointment Windows</span>
                  {
                    appointment.requestedAppointmentDate.alternativeAppointmentWindows.map((item, taskIndex) => (
                      <div className="alternative-appointment-window-item"
                           key={`alternative-appointment-window-item__${taskIndex}`}>
                        <span className="title-b-detail">Alternative Appointment Window #{taskIndex + 1}
                          <span className='click-delete' onClick={() => removeAlternativeAppointmentWindow(taskIndex)}>
                            <img className='one' src={icRedCancel} alt='icon'/>
                          </span>
                        </span>
                        <div className="box-day after-hourse">
                          <ul className="list-title ">
                            <li className="item-title">
                              <span className="name-item">Start After Date/ Time <span
                                className="red-symbol">*</span></span>
                              <div className="box-calen">
                                <div className="cover-input calendar">
                                  <DatePicker
                                    className={`box-datepicker ${!item.startAfterDate.date && submitted ? 'error-ver' : ''}`}
                                    renderDayContents={renderDayContents}
                                    selected={
                                      item.startAfterDate.date ?
                                        new Date(moment(item.startAfterDate.date).format('MM/DD/YYYY')) : null
                                    }
                                    onChange={date => onChangeDate(formatDate(date, 'date'), 'startAfterDate', 'date', 'alternativeAppointmentWindows', taskIndex)}
                                    placeholderText="DD/MM/YYYY"
                                    dateFormat="dd/MM/yyyy"
                                  />
                                  <span className="click-calen">
                                    <img src={icGrayCalen} alt="icon"/>
                                  </span>
                                </div>
                                <div className="cover-input hours">
                                  <DatePicker
                                    className={`box-datepicker ${!item.startAfterDate.time && submitted ? 'error-ver' : ''}`}
                                    selected={item.startAfterDate.time ? new Date(moment(item.startAfterDate.time).format('MM/DD/YYYY HH:mm')) : null}
                                    onChange={date => onChangeDate(formatDate(date, 'time'), 'startAfterDate', 'time', 'alternativeAppointmentWindows', taskIndex)}
                                    showTimeSelect
                                    showTimeSelectOnly
                                    timeIntervals={15}
                                    timeCaption="Time"
                                    dateFormat="hh:mm"
                                    placeholderText="HH:MM"
                                  />
                                </div>
                                <div className="cover-input drop-down">
                                  <Dropdown
                                    classes={`period ${!item.startAfterDate.period && submitted && 'error-ver'}`}
                                    label='preferredAppointment'
                                    selectedValue={item.startAfterDate ? item.startAfterDate.period : ''}
                                    options={periodOptions}
                                    onChangeDropdown={(value, label) => onChangeDate(value, 'startAfterDate', 'period', 'alternativeAppointmentWindows', taskIndex)}/>
                                </div>
                              </div>
                            </li>
                            <li className="item-title">
                              <span className="name-item">Start Before Date/ Time <span className="red-symbol">*</span></span>
                              <div className="box-calen">
                                <div className="cover-input calendar">
                                  <DatePicker
                                    className={`box-datepicker ${!item.startBeforeDate.date && submitted ? 'error-ver' : ''}`}
                                    renderDayContents={renderDayContents}
                                    selected={
                                      item.startBeforeDate.date ?
                                        new Date(moment(item.startBeforeDate.date).format('MM/DD/YYYY')) : null
                                    }
                                    onChange={date => onChangeDate(formatDate(date, 'date'), 'startBeforeDate', 'date', 'alternativeAppointmentWindows', taskIndex)}
                                    placeholderText="DD/MM/YYYY"
                                    dateFormat="dd/MM/yyyy"
                                  />
                                  <span className="click-calen">
                                    <img src={icGrayCalen} alt="icon"/>
                                  </span>
                                </div>
                                <div className="cover-input hours">
                                  <DatePicker
                                    className={`box-datepicker ${!item.startBeforeDate.time && submitted ? 'error-ver' : ''}`}
                                    selected={item.startBeforeDate.time ? new Date(moment(item.startBeforeDate.time).format('MM/DD/YYYY HH:mm')) : null}
                                    onChange={date => onChangeDate(formatDate(date, 'time'), 'startBeforeDate', 'time', 'alternativeAppointmentWindows', taskIndex)}
                                    showTimeSelect
                                    showTimeSelectOnly
                                    timeIntervals={15}
                                    timeCaption="Time"
                                    dateFormat="hh:mm"
                                    placeholderText="HH:MM"
                                  />
                                </div>
                                <div className="cover-input drop-down">
                                  <Dropdown
                                    classes={`period ${!item.startBeforeDate.period && submitted && 'error-ver'}`}
                                    label='preferredAppointment'
                                    selectedValue={item.startBeforeDate ? item.startBeforeDate.period : ''}
                                    options={periodOptions}
                                    onChangeDropdown={(value, label) => onChangeDate(value, 'startBeforeDate', 'period', 'alternativeAppointmentWindows', taskIndex)}/>
                                </div>
                              </div>
                            </li>
                          </ul>
                          <div className="red-war">
                            <img src={icRedWarning} alt="icon"/>
                            <p className="text">For standard priority request. Start Date/ Time must be greater than 5
                              days from current date/time</p>
                          </div>
                        </div>
                      </div>
                    ))
                  }
                </div>
              )
            }
            {
              appointment.appointmentType === 'After Hours' && (
                <div className="bottom-box">
                  <button className="btn-contact" onClick={() => addAlternativeAppointmentWindow()}>+ Alternate
                    Appointment Window(s)
                  </button>
                </div>
              )
            }
          </div>
          <div className="content-box">
            <div className="box-detail">
              <span className="title-b-detail">Notes</span>
              <ul className="list-title">
                <li className="item-title">
                  <div className="cover-area">
                    {
                      appointment.appointmentType === 'Business Hours' ? (
                        <textarea placeholder="Add any additional notes relevant for the appointment…." maxLength="4000"
                                  value={appointment.preferredAppointment.notes ? appointment.preferredAppointment.notes : ''}
                                  onChange={(e) => onChangeValue(e.target.value, 'preferredAppointment', 'notes')}/>
                      ) : (
                        <textarea placeholder="Add any additional notes relevant for the appointment…." maxLength="4000"
                                  value={appointment.requestedAppointmentDate.notes ? appointment.requestedAppointmentDate.notes : ''}
                                  onChange={(e) => onChangeValue(e.target.value, 'requestedAppointmentDate', 'notes')}/>
                      )
                    }
                  </div>
                </li>
              </ul>
              <span className="line-bottom"/>
            </div>
          </div>
        </div>
        <div className="bottom-btn">
          <button className="btn-cancel" onClick={() => cancel()}>Cancel</button>
          <button className="btn-cancel next" onClick={() => onNextStep()}>next
            <img className="one" src={icWhiteNext} alt="icon"/>
          </button>
        </div>
      </div>
      {/* end/cover content  */}
    </div>
  )
}
const mapStateToProps = ({ workTypes, workTypeRequiredExtensions }) => {
  return {
    workTypes,
    workTypeRequiredExtensions
  }
}

const mapDispatchToProps = {
  fetchAvailableWorkTypes,
  fetchRequiredExtensionTypesForWorkType
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskAndDates)
