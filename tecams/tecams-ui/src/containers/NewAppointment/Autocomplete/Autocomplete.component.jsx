import React, { useState } from 'react'
import icGraySearch from 'assets/icons/ic-gray-search.svg'
import { connect } from 'react-redux'
import { fetchUsers } from 'redux/actions'
import ClickOutHandler from 'react-clickout-handler'

const Autocomplete = ({ value, type, maxLength, autocompleteUsers, fetchUsers, onChangeValue, onSelectOption }) => {
  const [showSearch, setShowSearch] = useState(false)

  const onChangeSearch = (value) => {
    onChangeValue(value)
    if (value.length > 0) {
      fetchUsers(value)
      setShowSearch(true)
    } else {
      setShowSearch(false)
    }
  }

  return (
    <React.Fragment>
      <ClickOutHandler onClickOut={() => setShowSearch(false)}>
        <input type="text" placeholder="Enter…" maxLength={maxLength}
               value={value ? value : ''}
               onChange={(e) => onChangeSearch(e.target.value)}
        />
        {
          showSearch && autocompleteUsers && (
            <ul className="list-search">
              {
                autocompleteUsers.map((user, index) => (
                  <li className="item-search" key={`customer__${index}`}>
                    {
                      type === 'firstName' || type === 'lastName' ? (
                        <span className="click-item" onClick={() => {
                          onSelectOption(user)
                          setShowSearch(false)
                        }}>{user.firstName} {user.lastName}</span>
                      ) : (
                        <span className="click-item" onClick={() => {
                          onSelectOption(user)
                          setShowSearch(false)
                        }}>{user.userID}</span>
                      )
                    }
                  </li>
                ))
              }
            </ul>
          )
        }
      </ClickOutHandler>
      <span className="click-search" onClick={() => onChangeSearch(value)}>
        <img src={icGraySearch} alt="icon"/>
      </span>
    </React.Fragment>
  )
}

const mapStateToProps = ({ autocompleteUsers }) => {
  return {
    autocompleteUsers
  }
}

const mapDispatchToProps = {
  fetchUsers
}

export default connect(mapStateToProps, mapDispatchToProps)(Autocomplete)
