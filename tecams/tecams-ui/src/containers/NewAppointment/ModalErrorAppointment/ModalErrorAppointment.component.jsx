import './styles.scss';
import _ from 'lodash';
import iconClose from 'assets/icons/ic-red-close.svg';

import React, { useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';

const ModalErrorAppointment = ({ tryAgain, error }) => {
  const [showModal, setShowModal] = useState(true);
  const [showError, setShowError] = useState(false);
  useEffect(() => {
    setShowError(false)
  }, [error])

  const handleClose = () => setShowModal(false);

  const showErrorDetails = () => {
    setShowError(!showError);
  }

  return (
    <>
      <Modal show={showModal} onHide={handleClose} dialogClassName='modal-error-apt' centered>
        <div className='modal-container'>
          <div className='group-title'>
            <span className='big-title'>Error</span>
            <span className='warning'>Unknown error occured</span>
            <span className='btn-close center-child'>
              <img src={iconClose} alt='' />
            </span>
          </div>
          <div className='group-description'>
            <span className='notice'>We are not able to reserve your appointment</span>
            <span className='status' onClick={() => showErrorDetails()}>{showError ? 'Hide' : 'Show'} Error Details</span>
            {
              showError && (
                <div className='error-details-content'>
                  {
                      <div className="group-content">
                        <p>Error code {_.get(error, 'response.data.status', 500)}</p>
                        <p>
                          {_.get(error, 'response.data.message',error.message)}
                        </p>
                      </div>

                  }
                </div>
              )
            }
          </div>
          <div className='buttons'>
            <div className='style-btn btn-cancel center-child' onClick={handleClose}>Cancel</div>
            <div className='style-btn btn-back center-child' onClick={tryAgain}>Try Again</div>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default ModalErrorAppointment;
