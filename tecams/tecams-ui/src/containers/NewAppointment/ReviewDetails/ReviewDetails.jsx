import React, { useState } from 'react'
import { connect } from 'react-redux'
import icWhiteNext from 'assets/icons/ic-white-arrow-next.svg'
import icWhiteRequest from 'assets/icons/ic-white-reserve.svg'
import ModalRequestAppointment
  from '../ModalRequestAppointment/ModalRequestAppointment.component'
import ModalErrorAppointment
  from '../ModalErrorAppointment/ModalErrorAppointment.component'
import ModalCalendar from '../ModalCalendar/ModalCalendar.component'
import ReviewAppointmentDetails
  from './ReviewAppointmentDetails/ReviewAppointmentDetails'
import ReviewCustomerAndContacts
  from './ReviewCustomerAndContacts/ReviewCustomerAndContacts'
import ReviewAppointmentTask
  from './ReviewAppointmentTask/ReviewAppointmentTask'
import ReviewAppointmentDate
  from './ReviewAppointmentDate/ReviewAppointmentDate'
import { postAppointment } from '../../../redux/actions/appointment'
import ReactLoading from 'react-loading'

const ReviewDetails = ({ cancel, postAppointment, reservedAppointment }) => {
  const [data] = useState(JSON.parse(localStorage.getItem('appointment')))
  const [appointment, setAppointment] = useState(data)
  const [boxDetail, setBoxDetail] = useState([
    { isEdit: false, isShow: true },
    { isEdit: false, isShow: true },
    { isEdit: false, isShow: true },
    { isEdit: false, isShow: true },
  ])
  const [modalError, setModalError] = useState(false)
  const [modalRequest, setModalRequest] = useState(false)
  const [modalCalendar, setModalCalendar] = useState(false)
  const [disabledButton, setDisabledButton] = useState(false)

  /**
   * generate key
   * @param length length character
   */
  const generateKey = length => {
    let result = 'TECAM'
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    const charactersLength = characters.length
    for (let i = 0; i < length; i++) {
      result += characters.charAt(
        Math.floor(Math.random() * charactersLength)
      )
    }
    return result
  }

  const actionDetail = (index, result) => {
    setDisabledButton(false)
    if (result.event === 'edit') {
      const data = [...boxDetail]
      data[index] = { ...data[index], isEdit: result.isEdit }
      data.forEach(element => {
        if (element.isEdit) {
          setDisabledButton(true)
          return
        }
      })
      setBoxDetail([...data])
    } else {
      const data = [...boxDetail]
      data[index] = { ...data[index], isShow: result.isShow }
      setBoxDetail([...data])
    }
  }

  /**
   * reserve appointment
   */
  const reserveAppointment = result => {
    postAppointment(result)
    setAppointment({
      ...result
    })
    setModalCalendar(false)
    setModalRequest(true)
    // setModalError (true);
  }

  const onRequestAppointment = (data) => {
    reserveAppointment(data)
    setAppointment({
      ...appointment,
      id: generateKey(15),
      appointmentId: generateKey(15),
    })
    setModalRequest(true)
  }

  const onTryAgain = () => {
    reserveAppointment(appointment)
  }

  return (
    <div className="tab-item review">
      <ReviewAppointmentDetails
        appointment={appointment}
        setAppointment={setAppointment}
        isShow={boxDetail[0].isShow}
        isEdit={boxDetail[0].isEdit}
        actionDetail={result => actionDetail(0, result)}
      />
      <ReviewCustomerAndContacts
        appointment={appointment}
        setAppointment={setAppointment}
        isShow={boxDetail[1].isShow}
        isEdit={boxDetail[1].isEdit}
        actionDetail={result => actionDetail(1, result)}
      />
      <ReviewAppointmentTask
        appointment={appointment}
        setAppointment={setAppointment}
        isShow={boxDetail[2].isShow}
        isEdit={boxDetail[2].isEdit}
        actionDetail={result => actionDetail(2, result)}
      />
      <ReviewAppointmentDate
        appointment={appointment}
        setAppointment={setAppointment}
        isShow={boxDetail[3].isShow}
        isEdit={boxDetail[3].isEdit}
        actionDetail={result => actionDetail(3, result)}
      />
      <div className="bottom-tab">
        <button className="btn-cancel" onClick={() => cancel()}>Cancel</button>
        {appointment.appointmentType === 'Business Hours'
          ? <button
            className="btn-cancel next"
            disabled={disabledButton}
            onClick={() => setModalCalendar(true)}
          >
            Search Available Slots
            <img className="one" src={icWhiteNext} alt="icon"/>
          </button>
          : <button
            className="btn-cancel request after-bussines"
            onClick={() => onRequestAppointment()}
          >
            Request Appointment
            <img className="one" src={icWhiteRequest} alt="icon"/>
          </button>}
      </div>
      {reservedAppointment.appointmentID &&
      <ModalRequestAppointment
        appointment={appointment}
        appointmentID={reservedAppointment.appointmentID}
      />}
      {reservedAppointment.error &&
      <ModalErrorAppointment
        appointment={appointment}
        error={reservedAppointment.error}
        tryAgain={onTryAgain}
        show={reservedAppointment.error}
      />}
      {modalCalendar &&
      <ModalCalendar
        appointment={appointment}
        reserveAppointment={reserveAppointment}
        show={modalCalendar}
        onHide={() => setModalCalendar(false)}
      />}
      {reservedAppointment.reserving && (
        <div
          style={{
            margin: 0,
            position: 'fixed',
            top: '50vh',
            left: '50%'
          }}
          className="d-flex justify-content-center align-items-center vertical-center"
        >
          <ReactLoading
            type="spin"
            color="rgba(0, 155, 255, 0.93)"
            height={50}
            width={50}
          />
        </div>
      )}
    </div>
  )
}

const mapStateToProps = ({ reservedAppointment }) => {
  return {
    reservedAppointment
  }
}

const mapDispatchToProps = {
  postAppointment
}

export default connect(mapStateToProps, mapDispatchToProps)(ReviewDetails)
