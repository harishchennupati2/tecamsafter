import React, { useEffect, useState } from 'react'
import icBlueUpdate from 'assets/icons/ic-blue-update.svg'
import icBlueExpand from 'assets/icons/ic-blue-collapse-expand.svg'
import icGrayHelp from 'assets/icons/help.svg'
import icGreenTick from 'assets/icons/ic-green-tick-modal.svg'
import icRedCancel from 'assets/icons/ic-red-delete.svg'
import Dropdown from '../../../../shared/components/Dropdown/Dropdown.component'
import * as constants from '../../../../config/constants'
import _ from 'lodash'
import {
  APPOINTMENT_TYPES,
  APPOINTMENT_SUB_TYPES,
  MANDATORY_INDICATORS,
  LEVEL,
  INPUT_TYPES
} from '../../../../config/constants'
import Input from '../../../../shared/components/Input/Input.component'
import { connect } from 'react-redux'
import { fetchRequiredExtensionTypes } from 'redux/actions/appointment'

const ReviewAppointmentDetails = ({
  appointment,
  setAppointment,
  isShow,
  isEdit,
  actionDetail,
  fetchRequiredExtensionTypes,
  requiredExtensionTypes
}) => {
  const [appointmentDetails, setAppointmentDetails] = useState({ ...appointment })
  const [appointmentTypeOptions] = useState(constants.APPOINTMENT_TYPES)
  const [appointmentSubTypeOptions] = useState(constants.APPOINTMENT_SUB_TYPES)
  const [appointmentPriorityOptions] = useState(constants.APPOINTMENT_PRIORITIES)
  const [fundingCodeTypeOptions] = useState(constants.FUNDING_CODE_TYPES)
  const [orderingSystemOptions] = useState(constants.ORDERING_SYSTEM)
  const [relatedCategoryOptions] = useState(constants.RELATED_CATEGORIES)
  const [relatedLocationOptions] = useState(constants.RELATED_LOCATIONS)
  const [submitted, setSubmitted] = useState(false)
  useEffect(() => {
    if (appointmentDetails.appointmentType && appointmentDetails.appointmentSubType) {
      fetchRequiredExtensionTypes(
        LEVEL.APPT_LEVEL,
        _.find(APPOINTMENT_TYPES, { name: appointmentDetails.appointmentType }).value,
        _.find(APPOINTMENT_SUB_TYPES, { name: appointmentDetails.appointmentSubType }).value,
      )
    }
  }, [appointment])
  /**
   * on change value
   * @param {string} value value callback
   * @param {string} label label
   */
  const onChangeValue = (value, label, labelObject, index, labelObjectNested) => {

    if (!labelObject) {
      setAppointmentDetails({
        ...appointmentDetails,
        [label]: value,
      })
    } else if (labelObject && labelObjectNested === undefined && index === undefined) {
      setAppointmentDetails({
        ...appointmentDetails,
        [label]: {
          ...appointmentDetails[label],
          [labelObject]: value,
        },
      })
    } else if (labelObject && labelObjectNested === undefined && index !== undefined) {
      const data = [...appointmentDetails[label]]
      data[index] = { ...data[index], [labelObject]: value }
      setAppointmentDetails({
        ...appointmentDetails,
        [label]: [...data],
      })
    } else if (labelObject && labelObjectNested !== undefined && index === undefined) {
      const data = {
        ...appointmentDetails[label],
        [labelObjectNested]: {
          ...appointmentDetails[label][labelObjectNested],
          [labelObject]: value
        }
      }
      setAppointmentDetails({
        ...appointmentDetails,
        [label]: {
          ...data
        }
      })
    }
    if (label === 'appointmentSubType') {
      fetchRequiredExtensionTypes(
        LEVEL.APPT_LEVEL,
        _.find(APPOINTMENT_TYPES, { name: appointmentDetails.appointmentType }).value,
        _.find(APPOINTMENT_SUB_TYPES, { name: value }).value,
      )
    }
  }

  /**
   * add order details
   */
  const addOrderDetails = () => {
    setAppointmentDetails({
      ...appointmentDetails,
      orderDetails: [...appointmentDetails.orderDetails, {}],
    })
  }

  /**
   * add related appointment
   */
  const addRelatedAppointments = () => {
    setAppointmentDetails({
      ...appointmentDetails,
      relatedAppointments: [...appointmentDetails.relatedAppointments, {}],
    })
  }

  /**
   * delete item related appointment
   * @param {number} index index related appointment
   */
  const removeRelatedAppointment = index => {
    setAppointmentDetails({
      ...appointmentDetails,
      relatedAppointments: appointmentDetails.relatedAppointments.filter((item, idx) => index !== idx),
    })
  }

  /**
   * delete item order
   * @param {number} index index order
   */
  const removeOrder = index => {
    setAppointmentDetails({
      ...appointmentDetails,
      orderDetails: appointmentDetails.orderDetails.filter((item, idx) => index !== idx),
    })
  }

  const cancel = () => {
    setAppointmentDetails({ ...appointment })
    actionDetail({ event: 'edit', isEdit: false })
  }

  const onSave = () => {
    setSubmitted(true)

    let check = false
    if (
      !appointmentDetails.appointmentType ||
      !appointmentDetails.appointmentSubType ||
      !appointmentDetails.appointmentPriority
    ) {
      check = true
      return
    }
    if (requiredExtensionTypes) {
      _.forEach(requiredExtensionTypes, (value, key) => {
        if (MANDATORY_INDICATORS[value.mandatoryIndicator] && !appointmentDetails.additionalAppointment[value.name]) {
          check = true
        }
      })
    }

    _.forEach(appointmentDetails.orderDetails, order => {
      if (!order.ipNetworkFNN || !order.primaryOrderNumber || !order.orderSystem) {
        check = true
      }
    })
    if (appointmentDetails.relatedAppointments.length > 0) {
      _.forEach(appointmentDetails.relatedAppointments, related => {
        if (
          !related.relatedAppointmentCategory ||
          !related.relatedAppointmentLocation ||
          !related.relatedAppointmentId
        ) {
          check = true
        }
      })
    }
    if (
      appointmentDetails.appointmentType === 'After Hours' &&
      (!appointmentDetails.additionalAfterHoursAppointment.projectName ||
        !appointmentDetails.additionalAfterHoursAppointment.fundingCodeType ||
        !appointmentDetails.additionalAfterHoursAppointment.fundingCode ||
        !appointmentDetails.customerFundingApprover.firstName ||
        !appointmentDetails.customerFundingApprover.lastName ||
        !appointmentDetails.customerFundingApprover.phoneNumber)
    ) {
      check = true
      return
    }
    if (!check) {
      setAppointment({ ...appointment, ...appointmentDetails })
      setAppointmentDetails({ ...appointment, ...appointmentDetails })
      localStorage.setItem('appointment', JSON.stringify({ ...appointment, ...appointmentDetails }))
      actionDetail({ event: 'edit', isEdit: false })
    }
  }

  const groupedExtensions = requiredExtensionTypes && _.groupBy(requiredExtensionTypes.extensions, 'extensionType')
  const extensionTypeComponents = groupedExtensions && Object.keys(groupedExtensions).map(key => (
    <React.Fragment key={`extension-${key}`}>
      <span className='title-b-detail'>{key}</span>
      <ul className='list-title'>
        {groupedExtensions[key].map(({
          inputType, extensionName, extensionNameDescription, mandatoryIndicator, valuesList, maxLength
        }) => (
          <li className='item-title' key={`extension-${key}-${extensionName}`}>
            <Input
              type={INPUT_TYPES[inputType]}
              name={extensionName}
              description={extensionNameDescription}
              isMandatory={MANDATORY_INDICATORS[mandatoryIndicator]}
              options={valuesList && valuesList.map(value => ({ name: value }))}
              maxLength={maxLength}
              hasError={!_.get(appointmentDetails, `additionalAppointment[${key}][${extensionName}]`) && submitted}
              value={_.get(appointmentDetails, `additionalAppointment[${key}][${extensionName}]`)}
              selectedValue={_.get(appointmentDetails, `additionalAppointment[${key}][${extensionName}]`)}
              onChange={inputType === INPUT_TYPES.LOV ?
                (name) => onChangeValue(name, 'additionalAppointment', extensionName, undefined, key)
                : (e) => onChangeValue(e.target.value, 'additionalAppointment', extensionName, undefined, key)
              }
            />
          </li>))}
      </ul>
    </React.Fragment>
  ))

  return (
    <div
      className={`cover-content box-appointment-details ${!isShow ? 'collapse-ver' : ''} ${isEdit ? 'edit-ver' : ''}`}>
      <div className='top-title'>
        <span className='title-box'>Appointment Details</span>
        <div className='right-top'>
          <span className='click-item' onClick={() => actionDetail({ event: 'edit', isEdit: true })}>
            <img src={icBlueUpdate} alt='icon'/>
          </span>
          <span className='click-expand' onClick={() => actionDetail({ event: 'expand', isShow: !isShow })}>
            <img src={icBlueExpand} alt='icon'/>
          </span>
        </div>
      </div>
      <div className="box-content">
        <div className="content-box">
          <div className="box-detail">
            <ul className="list-title">
              <li className="item-title">
                <span className="name-item">Appointment Type
                  <span className="two-dots">:</span>
                  <span className="red-symbol">*</span>
                </span>
                <span className="text-right-input">{appointmentDetails.appointmentType}</span>
                <div className="cover-input dropdown-ver">
                  <Dropdown
                    classes='appointment-type disabled'
                    label='appointmentType'
                    selectedValue={appointmentDetails.appointmentType}
                    options={appointmentTypeOptions}
                    onChangeDropdown={(name,label) => onChangeValue(name, label)}
                  />
                </div>
              </li>
              <li className="item-title">
                <span className="name-item">Appointment Sub type
                  <span className="two-dots">:</span>
                  <span className="red-symbol">*</span>
                </span>
                <span className="text-right-input">{appointmentDetails.appointmentSubType}</span>
                <div className="cover-input dropdown-ver">
                  <Dropdown
                    classes='appointment-sub-type disabled'
                    label='appointmentSubType'
                    selectedValue={appointmentDetails.appointmentSubType}
                    options={appointmentSubTypeOptions}
                    onChangeDropdown={(name,label) => onChangeValue(name, label)}
                  />
                </div>
              </li>
              <li className="item-title">
                <span className="name-item">Appointment Priority
                  <span className="two-dots">:</span>
                  <span className="red-symbol">*</span>
                </span>
                <span className="text-right-input">{appointmentDetails.appointmentPriority}</span>
                <div className="cover-input dropdown-ver">
                  <Dropdown
                    classes='appointment-priority disabled'
                    label='appointmentPriority'
                    selectedValue={appointmentDetails.appointmentPriority}
                    options={appointmentPriorityOptions}
                    onChangeDropdown={(name,label) => onChangeValue(name, label)}
                  />
                </div>
              </li>
            </ul>
          </div>
          {
            appointment.appointmentType === 'After Hours' && (
              <div className="group-after-hours-ver">
                <div className="box-detail after-ver">
                  <span className="title-b-detail">Additional After Hours Information</span>
                  <ul className="list-title">
                    <li className="item-title">
                      <span className="name-item">Project Name
                      <span className="two-dots">:</span>
                        <span className="red-symbol">*</span>
                      </span>
                      <span
                        className="text-right-input">{appointmentDetails.additionalAfterHoursAppointment.projectName}</span>
                      <div
                        className={`cover-input ${!appointmentDetails.additionalAfterHoursAppointment.projectName &&
                        submitted &&
                        'error-ver'}`}>
                        <input
                          type='text'
                          placeholder='Enter...'
                          maxLength="50"
                          value={appointmentDetails.additionalAfterHoursAppointment.projectName ? appointmentDetails.additionalAfterHoursAppointment.projectName : ''}
                          onChange={e =>
                            onChangeValue(e.target.value, 'additionalAfterHoursAppointment', 'projectName')
                          }
                        />
                      </div>
                    </li>
                    <li className="item-title">
                      <span className="name-item">Funding Code Type
                      <span className="two-dots">:</span>
                        <span className="red-symbol">*</span>
                      </span>
                      <span
                        className="text-right-input">{appointmentDetails.additionalAfterHoursAppointment.fundingCodeType}</span>
                      <div className="cover-input dropdown-ver">
                        <Dropdown
                          classes={`funding-code-type ${!appointmentDetails.additionalAfterHoursAppointment
                            .fundingCodeType &&
                          submitted &&
                          'error-ver'}`}
                          label='additionalAfterHoursAppointment'
                          selectedValue={appointmentDetails.additionalAfterHoursAppointment.fundingCodeType}
                          options={fundingCodeTypeOptions}
                          onChangeDropdown={(value, label) => onChangeValue(value, label, 'fundingCodeType')}
                        />
                      </div>
                    </li>
                    <li className="item-title">
                      <span className="name-item">Funding Code
                      <span className="two-dots">:</span>
                        <span className="red-symbol">*</span>
                        <span className="click-noti">
                          <img className="one" src={icGrayHelp} alt="icon"/>
                          <div className="noti-content">
                            <p className="text-noti">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>
                          </div>
                        </span>
                      </span>
                      <span
                        className="text-right-input">{appointmentDetails.additionalAfterHoursAppointment.fundingCode}</span>
                      <div className="box-input">
                        <div
                          className={`cover-input ${!appointmentDetails.additionalAfterHoursAppointment.fundingCode &&
                          submitted &&
                          'error-ver'}`}>
                          <input
                            type='text'
                            placeholder='Enter...'
                            maxLength="9"
                            value={
                              appointmentDetails.additionalAfterHoursAppointment.fundingCode
                                ? appointmentDetails.additionalAfterHoursAppointment.fundingCode
                                : ''
                            }
                            onChange={e =>
                              onChangeValue(e.target.value, 'additionalAfterHoursAppointment', 'fundingCode')
                            }
                          />
                        </div>
                        <div className="cover-checkbox">
                          <input type="checkbox" id="cb-01"/>
                          <label htmlFor="cb-01" className="tick-me">
                            <img className="one" src={icGreenTick} alt="icon"/>
                            <span>The customer has been informed of the after hours charges that will be applicable and has approved<span
                              className="red-symbol">*</span></span>
                          </label>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <div className="box-detail after-ver">
                  <span className="title-b-detail">Customer Funding Approver</span>
                  <ul className="list-title">
                    <li className="item-title">
                      <span className="name-item">First Name<span className="red-symbol">*</span>
                        <span className="two-dots">:</span>
                      </span>
                      <span className="text-right-input">{appointmentDetails.customerFundingApprover.firstName}</span>
                      <div
                        className={`cover-input ${!appointmentDetails.customerFundingApprover.firstName &&
                        submitted &&
                        'error-ver'}`}>
                        <input
                          type='text'
                          placeholder='Enter…'
                          maxLength="50"
                          value={
                            appointmentDetails.customerFundingApprover.firstName
                              ? appointmentDetails.customerFundingApprover.firstName
                              : ''
                          }
                          onChange={e => onChangeValue(e.target.value, 'customerFundingApprover', 'firstName')}
                        />
                      </div>
                    </li>
                    <li className="item-title">
                      <span className="name-item">Last Name<span className="red-symbol">*</span>
                        <span className="two-dots">:</span>
                      </span>
                      <span className="text-right-input">{appointmentDetails.customerFundingApprover.lastName}</span>
                      <div
                        className={`cover-input ${!appointmentDetails.customerFundingApprover.lastName &&
                        submitted &&
                        'error-ver'}`}>
                        <input
                          type='text'
                          placeholder='Enter…'
                          maxLength="50"
                          value={
                            appointmentDetails.customerFundingApprover.lastName
                              ? appointmentDetails.customerFundingApprover.lastName
                              : ''
                          }
                          onChange={e => onChangeValue(e.target.value, 'customerFundingApprover', 'lastName')}
                        />
                      </div>
                    </li>
                    <li className="item-title">
                      <span className="name-item">Phone Number<span className="red-symbol">*</span>
                        <span className="two-dots">:</span>
                      </span>
                      <span className="text-right-input">{appointmentDetails.customerFundingApprover.phoneNumber}</span>
                      <div
                        className={`cover-input ${!appointmentDetails.customerFundingApprover.phoneNumber &&
                        submitted &&
                        'error-ver'}`}>
                        <input
                          type='text'
                          placeholder='Enter…'
                          maxLength="15"
                          value={
                            appointmentDetails.customerFundingApprover.phoneNumber
                              ? appointmentDetails.customerFundingApprover.phoneNumber
                              : ''
                          }
                          onChange={e => onChangeValue(e.target.value, 'customerFundingApprover', 'phoneNumber')}
                        />
                      </div>
                    </li>
                    <li className="item-title">
                      <span className="name-item">Alternate Phone Number
                        <span className="two-dots">:</span>
                      </span>
                      <span
                        className="text-right-input">{appointmentDetails.customerFundingApprover.alternativePhoneNumber}</span>
                      <div className='cover-input'>
                        <input
                          type='text'
                          placeholder='Enter…'
                          maxLength="15"
                          value={
                            appointmentDetails.customerFundingApprover.alternativePhoneNumber
                              ? appointmentDetails.customerFundingApprover.alternativePhoneNumber
                              : ''
                          }
                          onChange={e =>
                            onChangeValue(e.target.value, 'customerFundingApprover', 'alternativePhoneNumber')
                          }
                        />
                      </div>
                    </li>
                    <li className="item-title">
                      <span className="name-item">Email Address
                        <span className="two-dots">:</span>
                      </span>
                      <span
                        className="text-right-input">{appointmentDetails.customerFundingApprover.emailAddress}</span>
                      <div className='cover-input'>
                        <input
                          type='text'
                          placeholder='Enter…'
                          maxLength="100"
                          value={
                            appointmentDetails.customerFundingApprover.emailAddress
                              ? appointmentDetails.customerFundingApprover.emailAddress
                              : ''
                          }
                          onChange={e => onChangeValue(e.target.value, 'customerFundingApprover', 'emailAddress')}
                        />
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            )
          }
          {(appointmentDetails.appointmentSubType && requiredExtensionTypes) && (
            <div className='box-detail'>
              <span className='first-title'>Additional Appointment Details</span>
              {extensionTypeComponents}
            </div>
          )}
          {
            _.map(appointment.orderDetails, (order, index) => (
              <div className="box-detail" key={`order__${index}`}>
                <span className='title-b-detail'>
                  {index === 0 ? 'Order Details' : index > 0 && 'Secondary Details'}
                  {index >= 1 && (
                    <span className='click-delete' onClick={() => removeOrder(index)}>
                      <img className='one' src={icRedCancel} alt='icon'/>
                    </span>
                  )}
                </span>
                <ul className="list-title">
                  <li className="item-title">
                    <span className="name-item">IP Network FNN
                  <span className="two-dots">:</span>
                      <span className="red-symbol">*</span>
                    </span>
                    <span className="text-right-input">{order.ipNetworkFNN}</span>
                    <div className={`cover-input ${!order.ipNetworkFNN && submitted && 'error-ver'}`}>
                      <input
                        type='text'
                        placeholder='Enter…'
                        maxLength="9"
                        value={order.ipNetworkFNN ? order.ipNetworkFNN : ''}
                        onChange={e => onChangeValue(e.target.value, 'orderDetails', 'ipNetworkFNN', index)}
                      />
                    </div>
                  </li>
                  <li className="item-title">
                    <span className="name-item">Primary Order#
                  <span className="two-dots">:</span>
                      <span className="red-symbol">*</span>
                    </span>
                    <span className="text-right-input">{order.primaryOrderNumber}</span>
                    <div className={`cover-input ${!order.primaryOrderNumber && submitted && 'error-ver'}`}>
                      <input
                        type='text'
                        placeholder='Enter…'
                        maxLength="50"
                        value={order.primaryOrderNumber ? order.primaryOrderNumber : ''}
                        onChange={e => onChangeValue(e.target.value, 'orderDetails', 'primaryOrderNumber', index)}
                      />
                    </div>
                  </li>
                  <li className="item-title">
                    <span className="name-item">Ordering System
                  <span className="two-dots">:</span>
                      <span className="red-symbol">*</span>
                    </span>
                    <span className="text-right-input">{order.orderSystem}</span>
                    <div className="cover-input dropdown-ver">
                      <Dropdown
                        classes={`order-system ${!order.orderSystem && submitted && 'error-ver'}`}
                        label='orderDetails'
                        selectedValue={order.orderSystem}
                        options={orderingSystemOptions}
                        onChangeDropdown={(value, label) => onChangeValue(value, label, 'orderSystem', index)}
                      />
                    </div>
                  </li>
                </ul>
              </div>
            ))
          }
          <div className="bottom-box">
            <button className="btn-contact" onClick={() => addOrderDetails()}>+ Secondary Order(s)</button>
          </div>
        </div>
        <div className="content-box">
          <div className="box-detail">
            <span className="first-title">Related Appointments</span>
            {
              appointment.relatedAppointments.length > 0 && appointment.relatedAppointments.map((item, index) => (
                <div className="related-appointment-item" key={`related-appointment-item__${index}`}>
                  <span className="title-b-detail">Related Appointment #{index + 1}
                    <span className='click-delete' onClick={() => removeRelatedAppointment(index)}>
                      <img className='one' src={icRedCancel} alt='icon'/>
                    </span>
                  </span>
                  <ul className="list-title">
                    <li className="item-title">
                      <span className="name-item">Related Appointment’s Category
                      <span className="two-dots">:</span>
                        <span className="red-symbol">*</span>
                      </span>
                      <span className="text-right-input">{item.relatedAppointmentCategory}</span>
                      <div className="cover-input dropdown-ver">
                        <Dropdown
                          classes={`related-appointments-category ${!item.relatedAppointmentCategory &&
                          submitted &&
                          'error-ver'}`}
                          label='relatedAppointments'
                          selectedValue={item.relatedAppointmentCategory}
                          options={relatedCategoryOptions}
                          onChangeDropdown={(value, label) =>
                            onChangeValue(value, label, 'relatedAppointmentCategory', index)
                          }
                        />
                      </div>
                    </li>
                    <li className="item-title">
                      <span className="name-item">Related Appointment’s Location
                      <span className="two-dots">:</span>
                        <span className="red-symbol">*</span>
                      </span>
                      <span className="text-right-input">{item.relatedAppointmentLocation}</span>
                      <div className="cover-input dropdown-ver">
                        <Dropdown
                          classes={`related-appointments-location ${!item.relatedAppointmentLocation &&
                          submitted &&
                          'error-ver'}`}
                          label='relatedAppointments'
                          selectedValue={item.relatedAppointmentLocation}
                          options={relatedLocationOptions}
                          onChangeDropdown={(value, label) =>
                            onChangeValue(value, label, 'relatedAppointmentLocation', index)
                          }
                        />
                      </div>
                    </li>
                    <li className="item-title">
                      <span className="name-item">Related Appointment’s ID
                      <span className="two-dots">:</span>
                        <span className="red-symbol">*</span>
                      </span>
                      <span className="text-right-input">{item.relatedAppointmentId}</span>
                      <div className={`cover-input ${!item.relatedAppointmentId && submitted && 'error-ver'}`}>
                        <input
                          type='text'
                          placeholder='Enter…'
                          maxLength="50"
                          value={item.relatedAppointmentId ? item.relatedAppointmentId : ''}
                          onChange={e =>
                            onChangeValue(e.target.value, 'relatedAppointments', 'relatedAppointmentId', index)
                          }
                        />
                      </div>
                    </li>
                  </ul>
                </div>
              ))
            }
          </div>
          <div className="bottom-box">
            <button className="btn-contact" onClick={() => addRelatedAppointments()}>+ Related Appointment(s)</button>
          </div>
        </div>
      </div>
      <div className='bottom-btn'>
        <button className='btn-cancel' onClick={() => cancel()}>cancel</button>
        <button className='btn-cancel next' onClick={() => onSave()}>Save</button>
      </div>
    </div>
  )
}

const mapStateToProps = ({
  requiredExtensionTypes,
}) => {
  return {
    requiredExtensionTypes,
  }
}

const mapDispatchToProps = {
  fetchRequiredExtensionTypes
}

export default connect(mapStateToProps, mapDispatchToProps)(ReviewAppointmentDetails)
