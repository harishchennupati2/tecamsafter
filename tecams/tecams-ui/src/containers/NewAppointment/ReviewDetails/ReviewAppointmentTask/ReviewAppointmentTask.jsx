import React, { useEffect, useState } from 'react'
import icBlueUpdate from 'assets/icons/ic-blue-update.svg'
import icBlueExpand from 'assets/icons/ic-blue-collapse-expand.svg'
import icRedCancel from 'assets/icons/ic-red-delete.svg'
import _ from 'lodash'
import { fetchAvailableWorkTypes, fetchRequiredExtensionTypesForWorkType } from 'redux/actions/appointment'
import { connect } from 'react-redux'
import Input from '../../../../shared/components/Input/Input.component'
import { INPUT_TYPES, MANDATORY_INDICATORS } from '../../../../config/constants'

const ReviewAppointmentTask = ({
  appointment: data,
  setAppointment: setData,
  isShow,
  isEdit,
  actionDetail,
  workTypes,
  fetchAvailableWorkTypes,
  fetchRequiredExtensionTypesForWorkType,
  workTypeRequiredExtensions,
}) => {
  const [appointment, setAppointment] = useState({ ...data })
  const [submitted, setSubmitted] = useState(false)
  useEffect(
    () => {
      fetchAvailableWorkTypes()
      _.forEach(appointment.appointmentTasks, (task) => {
        if (task.work) {
          fetchRequiredExtensionTypesForWorkType(
            appointment.appointmentType,
            appointment.appointmentSubType,
            task.workCategory,
            task.work.workTypeID
          )
        }
      })
    }, [data]
  )
  /**
   * on change value dropdown
   * @param {string | Object} value value dropdown callback
   * @param {string} label label
   */
  const onChangeValue = (value, label, labelObject, taskIndex, labelObjectNested) => {
    if (labelObject && labelObjectNested === undefined && taskIndex === undefined) {
      setAppointment({
        ...appointment,
        [label]: {
          ...appointment[label],
          [labelObject]: value
        }
      })
    } else if (labelObject && labelObjectNested === undefined && taskIndex !== undefined) {
      const data = [...appointment[label]]
      data[taskIndex] = { ...data[taskIndex], [labelObject]: value }
      setAppointment({
        ...appointment,
        [label]: [
          ...data
        ]
      })
    } else if (labelObject === undefined && labelObjectNested === undefined && taskIndex !== undefined) {
      const data = [...appointment[label]]
      data[taskIndex] = { ...data[taskIndex], ...value }
      setAppointment({
        ...appointment,
        [label]: [
          ...data
        ]
      })
    } else if (labelObject && labelObjectNested !== undefined && taskIndex !== undefined) {
      const data = [...appointment[label]]
      data[taskIndex] = {
        ...data[taskIndex],
        [labelObjectNested]: {
          ...data[taskIndex][labelObjectNested],
          [labelObject]: value
        }
      }
      setAppointment({
        ...appointment,
        [label]: [
          ...data
        ]
      })
    }
  }

  /**
   * add more task
   */
  const addMoreTask = () => {
    setAppointment({
      ...appointment,
      appointmentTasks: [...appointment.appointmentTasks, {}]
    })
  }

  /**
   * delete item order
   * @param {number} index index order
   */
  const removeTask = (index) => {
    setAppointment({
      ...appointment,
      appointmentTasks: appointment.appointmentTasks.filter((item, idx) => index !== idx)
    })
  }

  /**
   * cancel
   */
  const cancel = () => {
    setAppointment({ ...appointment })
    actionDetail({ event: 'edit', isEdit: false })
  }

  /**
   * on save
   */
  const onSave = () => {
    setSubmitted(true)
    let check = false
    _.forEach(appointment.appointmentTasks, (item) => {
      if (!item.work) {
        check = true
        return
      }

      const extensions = workTypeRequiredExtensions[item.work.workTypeID]
      _.forEach(extensions, (key, value) => {
        if (MANDATORY_INDICATORS[value.mandatoryIndicator] && !item.additionalTaskDetails[value.name]) {
          check = true
        }
      })
    })
    if (!check) {
      setData({ ...appointment })
      localStorage.setItem('appointment', JSON.stringify({ ...appointment }))
      actionDetail({ event: 'edit', isEdit: false })
    }
  }

  const getRequiredMins = (task) => {
    const requiredMins = _.get(task, 'work.skillset.requiredMinutes')
    return requiredMins ? `${requiredMins} mins` : 'TBA'
  }

  const groupedWorkTypes = _.groupBy(workTypes, 'productFamily')
  const workCategoryOptions = Object.keys(groupedWorkTypes).map(category => ({ name: category }))

  const WorkTypeExtension = ({ task, taskIndex }) => {
    const extensions = workTypeRequiredExtensions[task.work.workTypeID]
    const groupedExtensions = extensions ? _.groupBy(extensions, 'extensionType') : []

    return Object.keys(groupedExtensions).length > 0 && (
      <div className="box-detail">
        <span className="first-title">Additional Task Details</span>
        {
          Object.keys(groupedExtensions).map(key => (
              <React.Fragment key={`extension-${task.work.workTypeID}-${key}`}>
                <span className='title-b-detail'>{key}</span>
                <ul className='list-title'>
                  {groupedExtensions[key].map(({
                    inputType, extensionName, extensionNameDescription, mandatoryIndicator, valuesList, maxLength
                  }) => {
                    const currentExtensions = _.get(task, `additionalTaskDetails[${key}]`, {})
                    return (
                      <li className='item-title' key={`extension-${key}-${extensionName}`}>
                        <Input
                          type={INPUT_TYPES[inputType]}
                          name={extensionName}
                          description={extensionNameDescription}
                          isMandatory={MANDATORY_INDICATORS[mandatoryIndicator]}
                          options={valuesList && valuesList.map(value => ({ name: value }))}
                          maxLength={maxLength}
                          hasError={!currentExtensions[extensionName] && submitted}
                          value={currentExtensions[extensionName]}
                          selectedValue={currentExtensions[extensionName]}
                          onChange={inputType === INPUT_TYPES.LOV ?
                            (name) => onChangeValue({
                              ...currentExtensions,
                              [extensionName]: name
                            }, 'appointmentTasks', key, taskIndex, 'additionalTaskDetails')
                            : (e) => onChangeValue({
                              ...currentExtensions,
                              [extensionName]: e.target.value
                            }, 'appointmentTasks', key, taskIndex, 'additionalTaskDetails')
                          }
                        />
                      </li>)
                  })}
                </ul>
                <span className="line-bottom"/>

              </React.Fragment>
            )
          )}
      </div>
    )
  }

  return (
    <div className={`cover-content box-appointment-tasks ${!isShow ? 'collapse-ver' : ''} ${isEdit ? 'edit-ver' : ''}`}>
      <div className='top-title'>
        <span className='title-box'>Appointment Tasks</span>
        <div className='right-top'>
          <span className='click-item' onClick={() => actionDetail({ event: 'edit', isEdit: true })}>
            <img src={icBlueUpdate} alt='icon'/>
          </span>
          <span className='click-expand' onClick={() => actionDetail({ event: 'expand', isShow: !isShow })}>
            <img src={icBlueExpand} alt='icon'/>
          </span>
        </div>
      </div>
      <div className='box-content'>
        <div className='content-box'>
          <div className='box-detail'>
            <span className='first-title'>Appointment Tasks</span>
            {
              _.map(appointment.appointmentTasks, (task, taskIndex) => (
                <div className="group-task" key={`task__${taskIndex}`}>
                  <div className="box-detail">
                    <span className="title-b-detail">Task No. {taskIndex + 1}
                      <span className="sub-title">Estimated Duration : {getRequiredMins(task)}</span>
                      {
                        taskIndex >= 1 && (
                          <span className="click-delete" onClick={() => removeTask(taskIndex)}>
                            <img className="one" src={icRedCancel} alt="icon"/>
                          </span>
                        )
                      }
                    </span>
                    <ul className="list-title">
                      <li className="item-title">
                        <Input
                          type={INPUT_TYPES.LOV}
                          name='Work Category'
                          isMandatory
                          options={workCategoryOptions}
                          hasError={!task.workCategory && submitted}
                          selectedValue={task.workCategory}
                          onChange={(value) => onChangeValue(
                            { workCategory: value, typeOfWork: '', work: null },
                            'appointmentTasks',
                            undefined,
                            taskIndex
                          )}
                        />
                      </li>
                      <li className="item-title">
                        <Input
                          name='Type of work'
                          type={INPUT_TYPES.LOV}
                          disabled={!task.workCategory}
                          hasError={!task.typeOfWork && submitted}
                          isMandatory
                          selectedValue={task.typeOfWork}
                          options={
                            _.get(groupedWorkTypes, task.workCategory, [])
                            .map(work => ({ ...work, name: work.workTypeDescription }))
                          }
                          onChange={(value, label, i) => {
                            const work = groupedWorkTypes[task.workCategory][i]
                            fetchRequiredExtensionTypesForWorkType(
                              appointment.appointmentType,
                              appointment.appointmentSubType,
                              task.workCategory,
                              work.workTypeID
                            )
                            onChangeValue({ work, typeOfWork: value }, 'appointmentTasks', undefined, taskIndex)
                          }}
                        />
                      </li>
                    </ul>
                  </div>
                  {task.work &&
                  WorkTypeExtension({ task, taskIndex })
                  }
                </div>
              ))
            }
            <div className="bottom-box">
              <button className="btn-contact" onClick={() => addMoreTask()}>+ More Task(s)</button>
            </div>
          </div>
        </div>
      </div>
      <div className='bottom-btn'>
        <button className='btn-cancel' onClick={() => cancel()}>cancel</button>
        <button className='btn-cancel next' onClick={() => onSave()}>Save</button>
      </div>
    </div>
  )
}
const mapStateToProps = ({ workTypes, workTypeRequiredExtensions }) => {
  return {
    workTypes,
    workTypeRequiredExtensions
  }
}

const mapDispatchToProps = {
  fetchAvailableWorkTypes,
  fetchRequiredExtensionTypesForWorkType
}
export default connect(mapStateToProps, mapDispatchToProps)(ReviewAppointmentTask)
