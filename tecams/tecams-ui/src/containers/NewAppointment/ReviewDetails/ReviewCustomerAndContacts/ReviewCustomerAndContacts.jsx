import React, { useState } from 'react';
import icBlueUpdate from 'assets/icons/ic-blue-update.svg';
import icBlueExpand from 'assets/icons/ic-blue-collapse-expand.svg';
import icYellowWarning from 'assets/icons/ic-warning-yellow.svg';
import icRedCancel from 'assets/icons/ic-red-delete.svg';
import Dropdown from '../../../../shared/components/Dropdown/Dropdown.component';
import * as constants from '../../../../config/constants';
import Autocomplete from '../../Autocomplete/Autocomplete.component';
import _ from 'lodash';
import { CUSTOMER_CONTACT_TYPES } from '../../../../config/constants'
import { TELSTRA_CONTACT_TYPES } from '../../../../config/constants'

const ReviewCustomerAndContacts = ({ appointment:data, setAppointment:setData, isShow, isEdit, actionDetail }) => {
  const [appointment, setAppointment] = useState({ ...data });
  const [customerTypeOptions] = useState(constants.CUSTOMER_TYPES);
  const [stateOptions] = useState(constants.STATES);
  const [submitted, setSubmitted] = useState(false);

  const [customerContactOptions, setCustomerContact] = useState(
    Object.keys(CUSTOMER_CONTACT_TYPES)
    .map(key => ({
      ...CUSTOMER_CONTACT_TYPES[key],
      isShow: key !== CUSTOMER_CONTACT_TYPES['Primary Customer Contact'].name
    })))
  const [tecamContactOptions, setTecamContact] = useState(
    Object.keys(TELSTRA_CONTACT_TYPES)
    .map(key => ({
      ...TELSTRA_CONTACT_TYPES[key],
      isShow: key !== TELSTRA_CONTACT_TYPES['Telstra Primary Contact'].name
    })))
  const [disabledButtonCustomer, setDisabledButtonCustomer] = useState(() => {
    let check = false;
    _.forEach(appointment.customerAndContacts.customerContacts, item => {
      if (!item.firstName || !item.lastName || !item.telephoneNumber || !item.emailAddress) {
        check = true;
      }
    })
    return check;
  })
  const [disabledButtonTecams, setDisabledButtonTecams] = useState(() => {
    let check = false;
    _.forEach(appointment.customerAndContacts.tecamContacts, item => {
      if (!item.firstName || !item.lastName || !item.telephoneNumber || !item.emailAddress || !item.userID) {
        check = true;
      }
    })
    return check;
  })

  const checkCustomerContact = (type) => {
    setDisabledButtonCustomer(false);
    if (type === 'add') {
      setDisabledButtonCustomer(true);
    } else {
      _.forEach(appointment.customerAndContacts.customerContacts, item => {
        if (!item.firstName || !item.lastName || !item.telephoneNumber || !item.emailAddress) {
          setDisabledButtonCustomer(true);
        }
      })
    }
  }

  const checkTecamContact = (type) => {
    setDisabledButtonTecams(false);
    if (type === 'add') {
      setDisabledButtonTecams(true);
    } else {
      _.forEach(appointment.customerAndContacts.tecamContacts, item => {
        if (!item.firstName || !item.lastName || !item.telephoneNumber || !item.emailAddress || !item.userID) {
          setDisabledButtonTecams(true);
        }
      })
    }
  }

  /**
   * on change value dropdown
   * @param {string} value value dropdown callback
   * @param {string} label label
   */
  const onChangeValue = (value, label, labelObject, index) => {
    if (!labelObject && index === undefined) {
      setAppointment({
        ...appointment,
        customerAndContacts: {
          ...appointment.customerAndContacts,
          [label]: value
        }
      })
    } else if (labelObject && index === undefined) {
      setAppointment({
        ...appointment,
        customerAndContacts: {
          ...appointment.customerAndContacts,
          [label]: {
            ...appointment.customerAndContacts[label],
            [labelObject]: value
          }
        }
      })
    } else if (labelObject && index !== undefined) {
      const data = [...appointment.customerAndContacts[label]]
      data[index] = { ...data[index], [labelObject]: value }
      setAppointment({
        ...appointment,
        customerAndContacts: {
          ...appointment.customerAndContacts,
          [label]: [
            ...data
          ]
        }
      })
    } else if (!labelObject && index !== undefined) {
      const data = [...appointment.customerAndContacts[label]]
      data[index] = { ...value }
      setAppointment({
        ...appointment,
        customerAndContacts: {
          ...appointment.customerAndContacts,
          [label]: [
            ...data
          ]
        }
      })
    }
    checkCustomerContact('change');
    checkTecamContact('change');
  }

  /**
   * add other customer contact
   * @param {string} title title customer contact
   */
  const addOtherCustomerContact = (title) => {
    setAppointment({
      ...appointment,
      customerAndContacts: {
        ...appointment.customerAndContacts,
        customerContacts: [
          ...appointment.customerAndContacts.customerContacts,
          {
            title,
            role: CUSTOMER_CONTACT_TYPES[title].key
          }
        ]
      }
    });
    checkCustomerContact('add');
  }

  /**
   * add other customer contact
   * @param {number} idx index customer contact
   * @param {string} title title customer contact options
   */
  const removeCustomerContact = (idx, title) => {
    const data = [...customerContactOptions];
    customerContactOptions.forEach((element, index) => {
      if (element.name === title) {
        data[index] = { ...data[index], isShow: true };
      }
    });
    setCustomerContact([...data]);
    checkCustomerContact('change');
    setAppointment({
      ...appointment,
      customerAndContacts: {
        ...appointment.customerAndContacts,
        customerContacts: appointment.customerAndContacts.customerContacts.filter((item, itemIdx) => idx !== itemIdx)
      }
    });
  }

  /**
   * add other tecams contact
   * @param {string} title title tecams contact
   */
  const addOtherTecamsContact = (title) => {
    setAppointment({
      ...appointment,
      customerAndContacts: {
        ...appointment.customerAndContacts,
        tecamsContacts: [
          ...appointment.customerAndContacts.tecamsContacts,
          {
            title,
            role: TELSTRA_CONTACT_TYPES[title].key
          }
        ]
      }
    });
    checkTecamContact('add');
  }

  /**
   * add other customer contact
   * @param {number} idx index customer contact
   * @param {string} title title customer contact options
   */
  const removeTecamContact = (idx, title) => {
    const data = [...tecamContactOptions];
    tecamContactOptions.forEach((element, index) => {
      if (element.name === title) {
        data[index] = { ...data[index], isShow: true };
      }
    });
    setTecamContact([...data]);
    checkTecamContact('change');
    setAppointment({
      ...appointment,
      customerAndContacts: {
        ...appointment.customerAndContacts,
        tecamsContacts: appointment.customerAndContacts.tecamsContacts.filter((item, itemIdx) => idx !== itemIdx)
      }
    });
  }

  const validateEmail = (email) => {
    var re = /^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/;
    return re.test(email);
  }

  /**
   * cancel
   */
  const cancel = () => {
    setAppointment({ ...appointment });
    actionDetail({ event: 'edit', isEdit: false });
  }

  /**
   * on save
   */
  const onSave = () => {
    setSubmitted(true);
    let check = false;
    if (!appointment.customerAndContacts.customerDetails.customerName || !appointment.customerAndContacts.customerDetails.customerType
      || !appointment.customerAndContacts.customerDetails.customerId || !appointment.customerAndContacts.customerDetails.isCustomerOffshoreConsenting) {
      check = true;
      return;
    }
    _.forEach(appointment.customerAndContacts.customerContacts, item => {
      if (!item.firstName || !item.lastName || !item.telephoneNumber || !item.emailAddress || !validateEmail(item.emailAddress)) {
        check = true;
        return;
      }
    })
    _.forEach(appointment.customerAndContacts.tecamsContacts, item => {
      if (!item.firstName || !item.lastName || !item.telephoneNumber || !item.emailAddress || !validateEmail(item.emailAddress) || !item.userID) {
        check = true;
        return;
      }
    })
    if (!check) {
      setData({...appointment})
      localStorage.setItem('appointment', JSON.stringify({...appointment }));
      actionDetail({ event: 'edit', isEdit: false });
    }
  }

  return (
    <div className={`cover-content box-customer-and-contacts ${!isShow ? 'collapse-ver' : ''} ${isEdit ? 'edit-ver' : ''}`}>
      <div className='top-title'>
        <span className='title-box'>Customer & Contacts</span>
        <div className='right-top'>
          <span className='click-item' onClick={() => actionDetail({ event: 'edit', isEdit: true })}>
            <img src={icBlueUpdate} alt='icon' />
          </span>
          <span className='click-expand' onClick={() => actionDetail({ event: 'expand', isShow: !isShow })}>
            <img src={icBlueExpand} alt='icon' />
          </span>
        </div>
      </div>
      <div className='box-content'>
        <div className='content-box'>
          <div className='box-detail'>
            <span className='title-b-detail'>Customer Details</span>
            <ul className='list-title'>
              <li className='item-title'>
                <span className='name-item'>
                  Customer Name
                    <span className='two-dots'>:</span>
                  <span className='red-symbol'>*</span>
                </span>
                <span className='text-right-input'>{appointment.customerAndContacts.customerDetails.customerName}</span>
                <div className={`cover-input ${!appointment.customerAndContacts.customerDetails.customerName && submitted && 'error-ver'}`}>
                  <input type="text" placeholder="Enter..." maxLength="100"
                    value={appointment.customerAndContacts.customerDetails.customerName ? appointment.customerAndContacts.customerDetails.customerName : ''}
                    onChange={(e) => onChangeValue(e.target.value, 'customerDetails', 'customerName')} />
                </div>
              </li>
              <li className='item-title'>
                <span className='name-item'>
                  Customer Type
                    <span className='two-dots'>:</span>
                  <span className='red-symbol'>*</span>
                </span>
                <span className='text-right-input'>{appointment.customerAndContacts.customerDetails.customerType}</span>
                <div className='cover-input dropdown-ver'>
                  <Dropdown
                    classes={`customer-type ${!appointment.customerAndContacts.customerDetails.customerType && submitted && 'error-ver'}`}
                    label='customerDetails'
                    selectedValue={appointment.customerAndContacts.customerDetails.customerType}
                    options={customerTypeOptions}
                    onChangeDropdown={(value, label) => onChangeValue(value, label, 'customerType')} />
                </div>
              </li>
              <li className='item-title'>
                <span className='name-item'>
                  Customer ID
                    <span className='two-dots'>:</span>
                  <span className='red-symbol'>*</span>
                </span>
                <span className='text-right-input'>{appointment.customerAndContacts.customerDetails.customerId}</span>
                <div className={`cover-input ${!appointment.customerAndContacts.customerDetails.customerId && submitted && 'error-ver'}`}>
                  <input type="text" placeholder="Enter..." maxLength="20"
                    value={appointment.customerAndContacts.customerDetails.customerId ? appointment.customerAndContacts.customerDetails.customerId : ''}
                    onChange={(e) => onChangeValue(e.target.value, 'customerDetails', 'customerId')} />
                </div>
              </li>
              <li className='item-title'>
                <span className='name-item'>
                  Is customer offshore consenting?
                    <span className='two-dots'>:</span>
                  <span className='red-symbol'>*</span>
                </span>
                <span className='text-right-input'>{appointment.customerAndContacts.customerDetails.isCustomerOffshoreConsenting}</span>
                <div className='box-check'>
                  <div className='cover-check'>
                    <ul className='list-check'>
                      <li className="item-check">
                        <input type="radio" id="cb-ctm01" name="ques-01"
                          defaultChecked={appointment.customerAndContacts.customerDetails.isCustomerOffshoreConsenting === 'Yes'} />
                        <label htmlFor="cb-ctm01" className="tick-radio"
                          onClick={() => onChangeValue('Yes', 'customerDetails', 'isCustomerOffshoreConsenting')}>
                          <span>Yes</span>
                        </label>
                      </li>
                      <li className="item-check">
                        <input type="radio" id="cb-ctm02" name="ques-01"
                          defaultChecked={appointment.customerAndContacts.customerDetails.isCustomerOffshoreConsenting === 'No'} />
                        <label htmlFor="cb-ctm02" className="tick-radio"
                          onClick={() => onChangeValue('No', 'customerDetails', 'isCustomerOffshoreConsenting')}>
                          <span>No</span>
                        </label>
                      </li>
                    </ul>
                  </div>
                  <div className='box-warning d-none'>
                    <img className='one' src={icYellowWarning} alt='icon' />
                    <span className='text-war'>TBC copy text warning lorem ipsum dolor sit amet</span>
                  </div>
                </div>
              </li>
              <li className='item-title'>
                <span className='name-item'>
                  Customer Address
                    <span className='two-dots'>:</span>
                  <span className='red-symbol'>*</span>
                </span>
                <span className='text-right-input'>
                  {appointment.customerAndContacts.customerDetails.addressLine1},
                    {appointment.customerAndContacts.customerDetails.addressLine2},
                    {appointment.customerAndContacts.customerDetails.addressLine3}</span>
                <div className="box-input">
                  <div className="cover-input">
                    <input type="text" placeholder="Address Line 1" maxLength="50"
                      value={appointment.customerAndContacts.customerDetails.addressLine1 ? appointment.customerAndContacts.customerDetails.addressLine1 : ''}
                      onChange={(e) => onChangeValue(e.target.value, 'customerDetails', 'addressLine1')} />
                  </div>
                  <div className="cover-input">
                    <input type="text" placeholder="Address Line 2" maxLength="50"
                      value={appointment.customerAndContacts.customerDetails.addressLine2 ? appointment.customerAndContacts.customerDetails.addressLine2 : ''}
                      onChange={(e) => onChangeValue(e.target.value, 'customerDetails', 'addressLine2')} />
                  </div>
                  <div className="cover-input">
                    <input type="text" placeholder="Address Line 3" maxLength="50"
                      value={appointment.customerAndContacts.customerDetails.addressLine3 ? appointment.customerAndContacts.customerDetails.addressLine3 : ''}
                      onChange={(e) => onChangeValue(e.target.value, 'customerDetails', 'addressLine3')} />
                  </div>
                </div>
              </li>
              <li className='item-title'>
                <span className='name-item'>
                  State
                    <span className='two-dots'>:</span>
                  <span className='red-symbol'>*</span>
                </span>
                <span className='text-right-input'>{appointment.customerAndContacts.customerDetails.state}</span>
                <div className='cover-input dropdown-ver'>
                  <Dropdown
                    classes={`customer-type ${!appointment.customerAndContacts.customerDetails.state && submitted && 'error-ver'}`}
                    label='customerDetails'
                    selectedValue={appointment.customerAndContacts.customerDetails.state}
                    options={stateOptions}
                    onChangeDropdown={(value, label) => onChangeValue(value, label, 'state')} />
                </div>
              </li>
            </ul>
          </div>
          <div className='box-detail'>
            <span className='first-title'>Customer Contact(s)</span>
            {
              appointment.customerAndContacts.customerContacts.map((item, index) => (
                <div className="customer-contact-item" key={`customer-contact-item__${index}`}>
                  <span className='title-b-detail'>{item.title}
                    {index >= 1 && (
                      <span className='click-delete' onClick={() => removeCustomerContact(index, item.title)}>
                        <img className='one' src={icRedCancel} alt='icon' />
                      </span>
                    )}
                  </span>
                  <ul className='list-title'>
                    <li className='item-title'>
                      <span className='name-item'>
                        First Name
                          <span className='two-dots'>:</span>
                        <span className='red-symbol'>*</span>
                      </span>
                      <span className='text-right-input'>{item.firstName}</span>
                      <div className={`cover-input ${!item.firstName && submitted && 'error-ver'}`}>
                        <input type="text" placeholder="Enter..." maxLength="50"
                          value={item.firstName ? item.firstName : ''}
                          onChange={(e) => onChangeValue(e.target.value, 'customerContacts', 'firstName', index)} />
                      </div>
                    </li>
                    <li className='item-title'>
                      <span className='name-item'>
                        Last Name
                          <span className='two-dots'>:</span>
                        <span className='red-symbol'>*</span>
                      </span>
                      <span className='text-right-input'>{item.lastName}</span>
                      <div className={`cover-input ${!item.lastName && submitted && 'error-ver'}`}>
                        <input type="text" placeholder="Enter..." maxLength="50"
                          value={item.lastName ? item.lastName : ''}
                          onChange={(e) => onChangeValue(e.target.value, 'customerContacts', 'lastName', index)} />
                      </div>
                    </li>
                    <li className='item-title'>
                      <span className='name-item'>
                        Phone Number
                          <span className='two-dots'>:</span>
                        <span className='red-symbol'>*</span>
                      </span>
                      <span className='text-right-input'>{item.telephoneNumber}</span>
                      <div className={`cover-input ${!item.telephoneNumber && submitted && 'error-ver'}`}>
                        <input type="text" placeholder="Enter..." maxLength="15"
                          value={item.telephoneNumber ? item.telephoneNumber : ''}
                          onChange={(e) => onChangeValue(e.target.value, 'customerContacts', 'telephoneNumber', index)} />
                      </div>
                    </li>
                    <li className='item-title'>
                      <span className='name-item'>
                        Alternative Phone Number
                          <span className='two-dots'>:</span>
                      </span>
                      <span className='text-right-input'>{item.alternativePhoneNumber}</span>
                      <div className="cover-input">
                        <input type="text" placeholder="Enter..." maxLength="15"
                          value={item.mobileNumber ? item.mobileNumber : ''}
                          onChange={(e) => onChangeValue(e.target.value, 'customerContacts', 'mobileNumber', index)} />
                      </div>
                    </li>
                    <li className='item-title'>
                      <span className='name-item'>
                        Email Address
                        <span className='two-dots'>:</span>
                        <span className='red-symbol'>*</span>
                      </span>
                      <span className='text-right-input'>{item.emailAddress}</span>
                      <div className={`cover-input ${(!item.emailAddress || !validateEmail(item.emailAddress)) && submitted && 'error-ver'}`}>
                        <input type="text" placeholder="Enter..." maxLength="100"
                          value={item.emailAddress ? item.emailAddress : ''}
                          onChange={(e) => onChangeValue(e.target.value, 'customerContacts', 'emailAddress', index)} />
                      </div>
                    </li>
                  </ul>
                </div>
              ))
            }
          </div>
          <div className='content-box'>
            <div className='bottom-box'>
              <button className="btn-contact" disabled={(!customerContactOptions[1].isShow && !customerContactOptions[2].isShow) || disabledButtonCustomer}>+ Other Customer Contact(s)
                <ul className="list-btn">
                  {
                    customerContactOptions.map((item, index) => (
                      <li className={`item-btn ${!customerContactOptions[index].isShow ? 'd-none' : ''}`} key={`item__${index}`}>
                        <span className="click-select" onClick={() => addOtherCustomerContact(item.name, index)}>{item.name}</span>
                      </li>
                    ))
                  }
                </ul>
              </button>
            </div>
          </div>
          <div className='box-detail'>
            <span className='first-title'>Tecams Contact(s)</span>
            {
              appointment.customerAndContacts.tecamsContacts.map((item, index) => (
                <div className="tecams-contact-item" key={`tecams-contact-item__${index}`}>
                  <span className='title-b-detail'>{item.title}
                      {index >= 1 && (
                      <span className='click-delete' onClick={() => removeTecamContact(index, item.title)}>
                        <img className='one' src={icRedCancel} alt='icon' />
                      </span>
                    )}
                  </span>
                  <ul className='list-title'>
                    <li className='item-title'>
                      <span className='name-item'>
                        First Name
                          <span className='two-dots'>:</span>
                        <span className='red-symbol'>*</span>
                      </span>
                      <span className='text-right-input'>{item.firstName}</span>
                      <div className={`cover-input ${!item.firstName && submitted && 'error-ver'}`}>
                        <Autocomplete
                          type="firstName"
                          maxLength={50}
                          value={item.firstName}
                          onChangeValue={(value) => onChangeValue(value, 'tecamsContacts', 'firstName', index)}
                          onSelectOption={(value) => onChangeValue(value, 'tecamsContacts', undefined, index)}
                        />
                      </div>
                    </li>
                    <li className='item-title'>
                      <span className='name-item'>
                        Last Name
                          <span className='two-dots'>:</span>
                        <span className='red-symbol'>*</span>
                      </span>
                      <span className='text-right-input'>{item.lastName}</span>
                      <div className={`cover-input ${!item.lastName && submitted && 'error-ver'}`}>
                        <Autocomplete
                          type="lastName"
                          maxLength={50}
                          value={item.lastName}
                          onChangeValue={(value) => onChangeValue(value, 'tecamsContacts', 'lastName', index)}
                          onSelectOption={(value) => onChangeValue(value, 'tecamsContacts', undefined, index)}
                        />
                      </div>
                    </li>
                    <li className='item-title'>
                      <span className='name-item'>
                        Phone Number
                          <span className='two-dots'>:</span>
                        <span className='red-symbol'>*</span>
                      </span>
                      <span className='text-right-input'>{item.telephoneNumber}</span>
                      <div className={`cover-input ${!item.telephoneNumber && submitted && 'error-ver'}`}>
                        <input type="text" placeholder="Enter..." maxLength="15"
                          value={item.telephoneNumber ? item.telephoneNumber : ''}
                          onChange={(e) => onChangeValue(e.target.value, 'tecamsContacts', 'phoneNumber', index)} />
                      </div>
                    </li>
                    <li className='item-title'>
                      <span className='name-item'>
                        Alternative Phone Number
                          <span className='two-dots'>:</span>
                      </span>
                      <span className='text-right-input'>{item.alternativePhoneNumber}</span>
                      <div className="cover-input">
                        <input type="text" placeholder="Enter..." maxLength="15"
                          value={item.mobileNumber ? item.mobileNumber : ''}
                          onChange={(e) => onChangeValue(e.target.value, 'tecamsContacts', 'mobileNumber', index)} />
                      </div>
                    </li>
                    <li className='item-title'>
                      <span className='name-item'>
                        Email Address
                          <span className='two-dots'>:</span>
                      </span>
                      <span className='text-right-input'>{item.emailAddress}</span>
                      <div className={`cover-input ${(!item.emailAddress || !validateEmail(item.emailAddress)) && submitted && 'error-ver'}`}>
                        <input type="text" placeholder="Enter..." maxLength="100"
                          value={item.emailAddress ? item.emailAddress : ''}
                          onChange={(e) => onChangeValue(e.target.value, 'tecamsContacts', 'emailAddress', index)} />
                      </div>
                    </li>
                    <li className='item-title'>
                      <span className='name-item'>
                        User ID
                          <span className='two-dots'>:</span>
                        <span className='red-symbol'>*</span>
                      </span>
                      <span className='text-right-input'>{item.userID}</span>
                      <div className={`cover-input ${!item.userID && submitted && 'error-ver'}`}>
                        <Autocomplete
                          type="userId"
                          maxLength={20}
                          value={item.userID}
                          onChangeValue={(value) => onChangeValue(value, 'tecamsContacts', 'userID', index)}
                          onSelectOption={(value) => onChangeValue(value, 'tecamsContacts', undefined, index)}
                        />
                      </div>
                    </li>
                  </ul>
                </div>
              ))
            }
          </div>
          <div className='bottom-box'>
            <button className="btn-contact" disabled={!tecamContactOptions[1].isShow || disabledButtonTecams}>+ Other Tecams Contact(s)
                <ul className="list-btn">
                {
                  tecamContactOptions.map((item, index) => (
                    <li className={`item-btn ${!tecamContactOptions[index].isShow ? 'd-none' : ''}`} key={`item__${index}`}>
                      <span className="click-select" onClick={() => addOtherTecamsContact(item.name, index)}>{item.name}</span>
                    </li>
                  ))
                }
              </ul>
            </button>
          </div>
        </div>
      </div>
      <div className='bottom-btn'>
        <button className='btn-cancel' onClick={() => cancel()}>cancel</button>
        <button className='btn-cancel next' onClick={() => onSave()}>Save</button>
      </div>
    </div>
  )
}

export default ReviewCustomerAndContacts
