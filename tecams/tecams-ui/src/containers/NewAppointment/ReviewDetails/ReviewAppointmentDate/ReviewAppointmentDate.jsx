import React, { useState } from 'react';
import icBlueUpdate from 'assets/icons/ic-blue-update.svg';
import icBlueExpand from 'assets/icons/ic-blue-collapse-expand.svg';
import icGrayCalen from 'assets/icons/ic-gray-calen.svg';
import icRedWarning from 'assets/icons/ic-red-warning.svg';
import icRedCancel from 'assets/icons/ic-red-delete.svg';
import Dropdown from '../../../../shared/components/Dropdown/Dropdown.component';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import * as constants from '../../../../config/constants';
import moment from 'moment';

const ReviewAppointmentDate = ({ appointment, setAppointment, isShow, isEdit, actionDetail }) => {
  const [appointmentDetails, setAppointmentDetails] = useState({ ...appointment });
  const [periodOptions] = useState(constants.PERIODS);
  const [submitted, setSubmitted] = useState(false);
  const [showErrorDate, setShowErrorDate] = useState(false);

  const renderDayContents = (day, date) => {
    return <span>{new Date(date).getDate()}</span>;
  };

  /**
   * on change value dropdown
   * @param {string} value value dropdown callback
   * @param {string} label label
   */
  const onChangeValue = (value, label, labelObject, index, labelObjectNested) => {
    if (labelObject && labelObjectNested === undefined && index === undefined) {
      setAppointmentDetails({
        ...appointmentDetails,
        [label]: {
          ...appointmentDetails[label],
          [labelObject]: value
        }
      });
    } else if (labelObject && labelObjectNested === undefined && index !== undefined) {
      const data = [...appointmentDetails[label]];
      data[index] = { ...data[index], [labelObject]: value };
      setAppointmentDetails({
        ...appointmentDetails,
        [label]: [
          ...data
        ]
      });
    } else if (labelObject && labelObjectNested !== undefined && index !== undefined) {
      const data = [...appointmentDetails[label]];
      data[index] = {
        ...data[index],
        [labelObjectNested]: {
          ...data[index][labelObjectNested],
          [labelObject]: value
        }
      };
      setAppointmentDetails({
        ...appointmentDetails,
        [label]: [
          ...data
        ]
      });
    }
  }

  const onChangeDate = (value, label, type, labelObject, index) => {
    if (type === undefined && labelObject === undefined && index === undefined) {
      setAppointmentDetails({
        ...appointmentDetails,
        preferredAppointment: {
          ...appointmentDetails.preferredAppointment,
          [label]: moment(value).format('MM/DD/YYYY')
        }
      })
    } else if (type !== undefined && labelObject === undefined && index === undefined) {
      setAppointmentDetails({
        ...appointmentDetails,
        requestedAppointmentDate: {
          ...appointmentDetails.requestedAppointmentDate,
          [label]: {
            ...appointmentDetails.requestedAppointmentDate[label],
            [type]: value
          }
        }
      })
      if ((label === 'startAfterDate' || label === 'startBeforeDate') && type === 'date') {
        const today = new Date();
        const startDate = new Date(formatDate(value, 'time'));
        const endDate = new Date(moment(value).format('MM/DD/YYYY'));
        if (startDate.getTime() - today.getTime() >= 358552701 || endDate.getTime() - today.getTime() >= 358552701) {
          setShowErrorDate(false);
        } else {
          setShowErrorDate(true);
        }
      }
    } else if (type !== undefined && labelObject !== undefined && index !== undefined) {
      const data = [...appointmentDetails.requestedAppointmentDate[labelObject]];
      data[index] = { ...data[index], [label]: { ...data[index][label], [type]: value } };
      setAppointmentDetails({
        ...appointmentDetails,
        requestedAppointmentDate: {
          ...appointmentDetails.requestedAppointmentDate,
          [labelObject]: [...data]
        }
      })
    }
  }

  const addAlternativeAppointmentWindow = () => {
    setAppointmentDetails({
      ...appointmentDetails,
      requestedAppointmentDate: {
        ...appointmentDetails.requestedAppointmentDate,
        alternativeAppointmentWindows: [...appointmentDetails.requestedAppointmentDate.alternativeAppointmentWindows, {
          startAfterDate: {},
          startBeforeDate: {}
        }]
      }
    });
  }

  /**
   * delete item alternative appointment window
   * @param {number} index index alternative appointment window
   */
  const removeAlternativeAppointmentWindow = (index) => {
    setAppointmentDetails({
      ...appointmentDetails,
      requestedAppointmentDate: {
        ...appointmentDetails.requestedAppointmentDate,
        alternativeAppointmentWindows: appointmentDetails.requestedAppointmentDate.alternativeAppointmentWindows.filter((item, idx) => index !== idx)
      }
    });
  }

  const formatDate = (date, type) => {
    if (type === 'date') {
      return moment(date).format('MM/DD/YYYY');
    } else {
      return moment(date).format('MM/DD/YYYY HH:mm');
    }
  }

  const cancel = () => {
    setAppointmentDetails({ ...appointment });
    actionDetail({ event: 'edit', isEdit: false });
  }

  const onSave = () => {
    setSubmitted(true);
    let check = false;
    if (appointment.appointmentType === 'Business Hours' && !appointment.preferredAppointment.startDate) {
      check = true;
      return;
    }
    if (appointment.appointmentType === 'After Hours' && (
      !appointment.requestedAppointmentDate.startAfterDate.date || !appointment.requestedAppointmentDate.startAfterDate.time || !appointment.requestedAppointmentDate.startAfterDate.period ||
      !appointment.requestedAppointmentDate.startBeforeDate.date || !appointment.requestedAppointmentDate.startBeforeDate.time || !appointment.requestedAppointmentDate.startBeforeDate.period
    )) {
      check = true;
      return;
    }
    if (!check) {
      setAppointment({ ...appointment, ...appointmentDetails })
      setAppointmentDetails({ ...appointment, ...appointmentDetails });
      localStorage.setItem('appointment', JSON.stringify({ ...appointment, ...appointmentDetails }));
      actionDetail({ event: 'edit', isEdit: false });
    }
  }

  return (
    <div className={`cover-content box-appointment-date ${!isShow ? 'collapse-ver' : ''} ${isEdit ? 'edit-ver' : ''}`}>
      <div className='top-title'>
        <span className='title-box'>Preferred Appointment Date</span>
        <div className='right-top'>
          <span className='click-item' onClick={() => actionDetail({ event: 'edit', isEdit: true })}>
            <img src={icBlueUpdate} alt='icon' />
          </span>
          <span className='click-expand' onClick={() => actionDetail({ event: 'expand', isShow: !isShow })}>
            <img src={icBlueExpand} alt='icon' />
          </span>
        </div>
      </div>
      <div className='box-content'>
        <div className='content-box'>
          <div className='box-detail'>
            <span className='title-b-detail'>{appointmentDetails.appointmentType === 'After Hours' ? 'Requested Appointment Date' : 'Preferred Appointment Date'}</span>
            {
              appointmentDetails.appointmentType === 'After Hours' ? (
                <div className={`box-day after-hourse ${showErrorDate ? 'error-ver' : ''}`}>
                  <ul className="list-title ">
                    <li className="item-title">
                      <span className="name-item">Start After Date/ Time <span className="red-symbol">*</span></span>
                      <span className='text-right-input'>{`${appointmentDetails.requestedAppointmentDate.startAfterDate.date} ${moment(appointmentDetails.requestedAppointmentDate.startAfterDate.time).format('hh:mm')} ${appointmentDetails.requestedAppointmentDate.startAfterDate.period}`}</span>
                      <div className="box-calen">
                        <div className="cover-input calendar">
                          <DatePicker
                            className={`box-datepicker ${!appointmentDetails.requestedAppointmentDate.startAfterDate.date && submitted ? 'error-ver' : ''}`}
                            renderDayContents={renderDayContents}
                            selected={
                              appointmentDetails.requestedAppointmentDate.startAfterDate.date ?
                                new Date(moment(appointmentDetails.requestedAppointmentDate.startAfterDate.date).format('MM/DD/YYYY')) : null
                            }
                            onChange={date => onChangeDate(formatDate(date, 'date'), 'startAfterDate', 'date')}
                            placeholderText="DD/MM/YYYY"
                            dateFormat="dd/MM/yyyy"
                          />
                          <span className="click-calen">
                            <img src={icGrayCalen} alt="icon" />
                          </span>
                        </div>
                        <div className="cover-input hours">
                          <DatePicker
                            className={`box-datepicker ${!appointmentDetails.requestedAppointmentDate.startAfterDate.time && submitted ? 'error-ver' : ''}`}
                            selected={appointmentDetails.requestedAppointmentDate.startAfterDate.time ? new Date(moment(appointmentDetails.requestedAppointmentDate.startAfterDate.time).format('MM/DD/YYYY HH:mm')) : null}
                            onChange={date => onChangeDate(formatDate(date, 'time'), 'startAfterDate', 'time')}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={15}
                            timeCaption="Time"
                            dateFormat="hh:mm"
                            placeholderText="HH:MM"
                          />
                        </div>
                        <div className="cover-input drop-down">
                          <Dropdown
                            classes={`period ${!appointmentDetails.requestedAppointmentDate.startAfterDate.period && submitted && 'error-ver'}`}
                            label='requestedAppointmentDate'
                            selectedValue={appointmentDetails.requestedAppointmentDate.startAfterDate ? appointmentDetails.requestedAppointmentDate.startAfterDate.period : ''}
                            options={periodOptions}
                            onChangeDropdown={(value, label) => onChangeDate(value, 'startAfterDate', 'period')} />
                        </div>
                      </div>
                    </li>
                    <li className="item-title">
                      <span className="name-item">Start Before Date/ Time <span className="red-symbol">*</span></span>
                      <span className='text-right-input'>{`${appointmentDetails.requestedAppointmentDate.startBeforeDate.date} ${moment(appointmentDetails.requestedAppointmentDate.startBeforeDate.time).format('hh:mm')} ${appointmentDetails.requestedAppointmentDate.startBeforeDate.period}`}</span>
                      <div className="box-calen">
                        <div className="cover-input calendar">
                          <DatePicker
                            className={`box-datepicker ${!appointmentDetails.requestedAppointmentDate.startBeforeDate.date && submitted ? 'error-ver' : ''}`}
                            renderDayContents={renderDayContents}
                            selected={
                              appointmentDetails.requestedAppointmentDate.startBeforeDate.date ?
                                new Date(moment(appointmentDetails.requestedAppointmentDate.startBeforeDate.date).format('MM/DD/YYYY')) : null
                            }
                            onChange={date => onChangeDate(formatDate(date, 'date'), 'startBeforeDate', 'date')}
                            placeholderText="DD/MM/YYYY"
                            dateFormat="dd/MM/yyyy"
                          />
                          <span className="click-calen">
                            <img src={icGrayCalen} alt="icon" />
                          </span>
                        </div>
                        <div className="cover-input hours">
                          <DatePicker
                            className={`box-datepicker ${!appointmentDetails.requestedAppointmentDate.startBeforeDate.time && submitted ? 'error-ver' : ''}`}
                            selected={appointmentDetails.requestedAppointmentDate.startBeforeDate.time ? new Date(moment(appointmentDetails.requestedAppointmentDate.startBeforeDate.time).format('MM/DD/YYYY HH:mm')) : null}
                            onChange={date => onChangeDate(formatDate(date, 'time'), 'startBeforeDate', 'time')}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={15}
                            timeCaption="Time"
                            dateFormat="hh:mm"
                            placeholderText="HH:MM"
                          />
                        </div>
                        <div className="cover-input drop-down">
                          <Dropdown
                            classes={`period ${!appointmentDetails.requestedAppointmentDate.startBeforeDate.period && submitted && 'error-ver'}`}
                            label='requestedAppointmentDate'
                            selectedValue={appointmentDetails.requestedAppointmentDate.startBeforeDate ? appointmentDetails.requestedAppointmentDate.startBeforeDate.period : ''}
                            options={periodOptions}
                            onChangeDropdown={(value, label) => onChangeDate(value, 'startBeforeDate', 'period')} />
                        </div>
                      </div>
                    </li>
                  </ul>
                  <div className="red-war">
                    <img src={icRedWarning} alt="icon" />
                    <p className="text">For standard priority request. Start Date/ Time must be greater than 5 days from current date/time</p>
                  </div>
                </div>
              ) : (
                  <ul className="list-title">
                    <li className="item-title">
                      <span className="name-item">Start Date <span className="red-symbol">*</span></span>
                      <span className='text-right-input'>{appointmentDetails.preferredAppointment.startDate}</span>
                      <div className="cover-input calen">
                        <DatePicker
                          className={`box-datepicker ${!appointmentDetails.preferredAppointment.startDate && submitted ? 'error-ver' : ''}`}
                          renderDayContents={renderDayContents}
                          selected={
                            appointmentDetails.preferredAppointment.startDate ?
                              moment(appointmentDetails.preferredAppointment.startDate).toDate()
                              : moment().startOf('day').add(3, 'days').toDate()
                          }
                          minDate={moment().startOf('day').add(3, 'days').toDate()}
                          onChange={date => onChangeDate(date, 'startDate')}
                          placeholderText="DD/MM/YYYY"
                          dateFormat="dd/MM/yyyy"
                        />
                        <span className="click-calen">
                          <img src={icGrayCalen} alt="icon" />
                        </span>
                      </div>
                    </li>
                  </ul>
                )
            }
          </div>
          {
            appointmentDetails.requestedAppointmentDate && appointmentDetails.requestedAppointmentDate.alternativeAppointmentWindows && appointmentDetails.requestedAppointmentDate.alternativeAppointmentWindows.length > 0 && (
              <div className="box-detail">
                <span className="first-title">Alternative Appointment Windows</span>
                {
                  appointmentDetails.requestedAppointmentDate.alternativeAppointmentWindows.map((item, index) => (
                    <div className="alternative-appointment-window-item" key={`alternative-appointment-window-item__${index}`}>
                      <span className="title-b-detail">Alternative Appointment Window #{index + 1}
                        <span className='click-delete' onClick={() => removeAlternativeAppointmentWindow(index)}>
                          <img className='one' src={icRedCancel} alt='icon' />
                        </span>
                      </span>
                      <div className="box-day after-hourse">
                        <ul className="list-title ">
                          <li className="item-title">
                            <span className="name-item">Start After Date/ Time <span className="red-symbol">*</span></span>
                            <span className='text-right-input'>{item.startAfterDate.date} {moment(item.startAfterDate.time).format('HH:mm')} {item.startAfterDate.period}</span>
                            <div className="box-calen">
                              <div className="cover-input calendar">
                                <DatePicker
                                  className={`box-datepicker ${!item.startAfterDate.date && submitted ? 'error-ver' : ''}`}
                                  renderDayContents={renderDayContents}
                                  selected={
                                    item.startAfterDate.date ?
                                      new Date(moment(item.startAfterDate.date).format('MM/DD/YYYY')) : null
                                  }
                                  onChange={date => onChangeDate(formatDate(date, 'date'), 'startAfterDate', 'date', 'alternativeAppointmentWindows', index)}
                                  placeholderText="DD/MM/YYYY"
                                  dateFormat="dd/MM/yyyy"
                                />
                                <span className="click-calen">
                                  <img src={icGrayCalen} alt="icon" />
                                </span>
                              </div>
                              <div className="cover-input hours">
                                <DatePicker
                                  className={`box-datepicker ${!item.startAfterDate.time && submitted ? 'error-ver' : ''}`}
                                  selected={item.startAfterDate.time ? new Date(moment(item.startAfterDate.time).format('MM/DD/YYYY HH:mm')) : null}
                                  onChange={date => onChangeDate(formatDate(date, 'time'), 'startAfterDate', 'time', 'alternativeAppointmentWindows', index)}
                                  showTimeSelect
                                  showTimeSelectOnly
                                  timeIntervals={15}
                                  timeCaption="Time"
                                  dateFormat="hh:mm"
                                  placeholderText="HH:MM"
                                />
                              </div>
                              <div className="cover-input drop-down">
                                <Dropdown
                                  classes={`period ${!item.startAfterDate.period && submitted && 'error-ver'}`}
                                  label='preferredAppointment'
                                  selectedValue={item.startAfterDate ? item.startAfterDate.period : ''}
                                  options={periodOptions}
                                  onChangeDropdown={(value, label) => onChangeDate(value, 'startAfterDate', 'period', 'alternativeAppointmentWindows', index)} />
                              </div>
                            </div>
                          </li>
                          <li className="item-title">
                            <span className="name-item">Start Before Date/ Time <span className="red-symbol">*</span></span>
                            <span className='text-right-input'>{item.startBeforeDate.date} {moment(item.startBeforeDate.time).format('HH:mm')} {item.startBeforeDate.period}</span>
                            <div className="box-calen">
                              <div className="cover-input calendar">
                                <DatePicker
                                  className={`box-datepicker ${!item.startBeforeDate.date && submitted ? 'error-ver' : ''}`}
                                  renderDayContents={renderDayContents}
                                  selected={
                                    item.startBeforeDate.date ?
                                      new Date(moment(item.startBeforeDate.date).format('MM/DD/YYYY')) : null
                                  }
                                  onChange={date => onChangeDate(formatDate(date, 'date'), 'startBeforeDate', 'date', 'alternativeAppointmentWindows', index)}
                                  placeholderText="DD/MM/YYYY"
                                  dateFormat="dd/MM/yyyy"
                                />
                                <span className="click-calen">
                                  <img src={icGrayCalen} alt="icon" />
                                </span>
                              </div>
                              <div className="cover-input hours">
                                <DatePicker
                                  className={`box-datepicker ${!item.startBeforeDate.time && submitted ? 'error-ver' : ''}`}
                                  selected={item.startBeforeDate.time ? new Date(moment(item.startBeforeDate.time).format('MM/DD/YYYY HH:mm')) : null}
                                  onChange={date => onChangeDate(formatDate(date, 'time'), 'startBeforeDate', 'time', 'alternativeAppointmentWindows', index)}
                                  showTimeSelect
                                  showTimeSelectOnly
                                  timeIntervals={15}
                                  timeCaption="Time"
                                  dateFormat="hh:mm"
                                  placeholderText="HH:MM"
                                />
                              </div>
                              <div className="cover-input drop-down">
                                <Dropdown
                                  classes={`period ${!item.startBeforeDate.period && submitted && 'error-ver'}`}
                                  label='preferredAppointment'
                                  selectedValue={item.startBeforeDate ? item.startBeforeDate.period : ''}
                                  options={periodOptions}
                                  onChangeDropdown={(value, label) => onChangeDate(value, 'startBeforeDate', 'period', 'alternativeAppointmentWindows', index)} />
                              </div>
                            </div>
                          </li>
                        </ul>
                        <div className="red-war">
                          <img src={icRedWarning} alt="icon" />
                          <p className="text">For standard priority request. Start Date/ Time must be greater than 5 days from current date/time</p>
                        </div>
                      </div>
                    </div>
                  ))
                }
              </div>
            )
          }
          {
            appointmentDetails.appointmentType === 'After Hours' && (
              <div className="bottom-box">
                <button className="btn-contact" onClick={() => addAlternativeAppointmentWindow()}>+ Alternate Appointment Window(s)</button>
              </div>
            )
          }
          <div className='box-detail'>
            <span className='title-b-detail'>Notes</span>
            <ul className='list-title'>
              <li className='item-title'>
                {
                  appointmentDetails.appointmentType === 'Business Hours' ? (
                    <span className='text-right-input'>{appointmentDetails.preferredAppointment ? appointmentDetails.preferredAppointment.notes : ''}{' '}</span>
                  ) : (
                    <span className='text-right-input'>{appointmentDetails.requestedAppointmentDate ? appointmentDetails.requestedAppointmentDate.notes : ''}{' '}</span>
                  )
                }
                <div className='cover-area'>
                  {
                    appointmentDetails.appointmentType === 'Business Hours' ? (
                      <textarea placeholder="Add any additional notes relevant for the appointment…."
                        value={appointmentDetails.preferredAppointment ? appointmentDetails.preferredAppointment.notes : ''}
                        onChange={(e) => onChangeValue(e.target.value, 'preferredAppointment', 'notes')}></textarea>
                    ) : (
                        <textarea placeholder="Add any additional notes relevant for the appointment…."
                          value={appointmentDetails.requestedAppointmentDate ? appointmentDetails.requestedAppointmentDate.notes : ''}
                          onChange={(e) => onChangeValue(e.target.value, 'requestedAppointmentDate', 'notes')}></textarea>
                      )
                  }
                </div>
              </li>
            </ul>
            <span className='line-bottom'></span>
          </div>
        </div>
      </div>
      <div className='bottom-btn'>
        <button className='btn-cancel' onClick={() => cancel()}>cancel</button>
        <button className='btn-cancel next' onClick={() => onSave()}>Save</button>
      </div>
    </div>
  )
}

export default ReviewAppointmentDate;
