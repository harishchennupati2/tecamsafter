import './styles.scss';

import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import { Link } from 'react-router-dom';

const ModalCancelAppointment = ({ show, onHide }) => {
  const [showModal, setShowModal] = useState(true);

  const handleClose = () => setShowModal(false);
  // const handleShow = () => setShowModal(true);

  return (
    <>
      <Modal show={showModal} onHide={handleClose} dialogClassName='modal-cancel-apt' centered>
        <div className='modal-container'>
          <span className='big-title'>Do you wish to cancel?</span>
          <span className='warning'>All details will be lost</span>
          <div className='buttons'>
            <div className='style-btn btn-no center-child' onClick={handleClose}>No</div>
            <Link to="/my-appointments" className='style-btn btn-cancel center-child'>Yes, Cancel</Link>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default ModalCancelAppointment;
