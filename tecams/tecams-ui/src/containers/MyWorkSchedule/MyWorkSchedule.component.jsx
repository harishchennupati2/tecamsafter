import React from 'react';
import Pagination from 'react-bootstrap/Pagination';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { connect } from 'react-redux';
import { fetchWorkSchedule } from 'redux/actions';
import ReactLoading from 'react-loading';

import styles from './styles.module.scss';
import searchIcon from 'assets/icons/search.svg';
import filterIcon from 'assets/icons/filter.svg';
import { ReactComponent as PaginationNextIcon } from 'assets/icons/pagination-next.svg';

class MyWorkSchedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sort: {
        sort: '',
        order: ''
      }
    };

    this.getTotalPage = this.getTotalPage.bind(this);
    this.goToNextPage = this.goToNextPage.bind(this);
    this.goToPreviousPage = this.goToPreviousPage.bind(this);
    this.reloadData = this.reloadData.bind(this);
    this.goToPage = this.goToPage.bind(this);
  }

  componentDidMount() {
    this.reloadData();
  }

  reloadData() {
    const { fetchWorkSchedule, workSchedules } = this.props;
    const { sort } = this.state;

    fetchWorkSchedule(
      sort.sort,
      sort.order,
      workSchedules.currentPage,
      workSchedules.limit
    );
  }

  getTotalPage() {
    const {
      workSchedules: { total, limit }
    } = this.props;
    const totalPage = Math.ceil(total / limit);
    return totalPage;
  }

  goToNextPage() {
    const {
      workSchedules: { limit, currentPage },
      fetchWorkSchedule
    } = this.props;
    const { sort } = this.state;
    fetchWorkSchedule(sort.sort, sort.order, currentPage + 1, limit);
  }

  goToPreviousPage() {
    const {
      workSchedules: { limit, currentPage },
      fetchWorkSchedule
    } = this.props;
    const { sort } = this.state;
    fetchWorkSchedule(sort.sort, sort.order, currentPage - 1, limit);
  }

  goToPage(page) {
    const {
      workSchedules: { limit },
      fetchWorkSchedule
    } = this.props;
    const { sort } = this.state;
    fetchWorkSchedule(sort.sort, sort.order, page, limit);
  }

  render() {
    const { workSchedules } = this.props;
    const { datas: actions } = workSchedules;
    const { sort } = this.state;
    const totalPage = this.getTotalPage();
    let items = [];
    let currentPage = workSchedules.currentPage;
    for (let number = 1; number <= totalPage; number++) {
      items.push(
        <Pagination.Item
          onClick={() => {
            this.goToPage(number);
          }}
          key={number}
          active={currentPage === number}
        >
          {number}
        </Pagination.Item>
      );
    }
    if(actions.length !==0 ){
      return (
      <div className='d-flex flex-column'>
        <h1 className={styles.title}>My Work Schedule</h1>
        <div className={styles['container-1']}>
          <div className={`d-flex flex-column ${styles['content-container']}`}>
            <div className='d-flex justify-content-between full-width align-items-center'>
              <span className={`${styles['pending-actions']}`}>
                Pending Actions
              </span>
              <div className='d-flex'>
                <div
                  className={`d-flex align-items-center ${styles['search-container']}`}
                >
                  <img
                    className={`${styles['search-icon']}`}
                    src={searchIcon}
                    alt=''
                  />
                  <input
                    className={`${styles['search-input']} flex-1`}
                    type='text'
                    placeholder='Search appointment or task...'
                  />
                </div>
                <button type='button' className='btn btn-primary'>
                  <img src={filterIcon} alt='' />
                  Filter
                </button>
              </div>
            </div>

            <table className={`table ${styles.table}`}>
              <thead>
                <tr>
                  <th
                    onClick={() => {
                      if (sort.sort !== 'customer') {
                        sort.sort = 'customer';
                        sort.order = 'desc';
                      } else {
                        sort.order = sort.order === 'desc' ? 'asc' : 'desc';
                      }
                      this.setState({ sort }, this.reloadData);

                    }}
                    scope='col'
                  >
                    <div className='d-flex align-items-center'>
                      <span
                        className={cn({
                          [styles.desc]:
                            sort.sort === 'customer' && sort.order === 'desc',
                          [styles.asc]:
                            sort.sort === 'customer' && sort.order === 'asc'
                        })}
                      >
                        Customer
                      </span>
                    </div>
                  </th>
                  <th
                    onClick={() => {
                      if (sort.sort !== 'appointmentId') {
                        sort.sort = 'appointmentId';
                        sort.order = 'desc';
                      } else {
                        sort.order = sort.order === 'desc' ? 'asc' : 'desc';
                      }
                      this.setState({ sort }, this.reloadData);
                    }}
                    scope='col'
                  >
                    <div className='d-flex align-items-center'>
                      <span
                        className={cn({
                          [styles.desc]:
                            sort.sort === 'appointmentId' &&
                            sort.order === 'desc',
                          [styles.asc]:
                            sort.sort === 'appointmentId' &&
                            sort.order === 'asc'
                        })}
                      >
                        Appointment ID
                      </span>
                    </div>
                  </th>
                  <th scope='col'>
                    <div className='d-flex align-items-center'>
                      <span>Task ID</span>
                    </div>
                  </th>
                  <th
                    onClick={() => {
                      if (sort.sort !== 'dateTime') {
                        sort.sort = 'dateTime';
                        sort.order = 'desc';
                      } else {
                        sort.order = sort.order === 'desc' ? 'asc' : 'desc';
                      }
                      this.setState({ sort }, this.reloadData);
                    }}
                    scope='col'
                  >
                    <div className='d-flex align-items-center'>
                      <span
                        className={cn({
                          [styles.desc]:
                            sort.sort === 'dateTime' && sort.order === 'desc',
                          [styles.asc]:
                            sort.sort === 'dateTime' && sort.order === 'asc'
                        })}
                      >
                        Date/ Time
                      </span>
                    </div>
                  </th>
                  <th scope='col'>
                    <div className='d-flex align-items-center'>
                      <span>Action Type</span>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
                {actions &&
                  actions.map((a, index) => (
                    <tr key={index}>
                      <td>{a.pendingActions[0].customerName}</td>
                      <td>{a.pendingActions[0].appointmentID}</td>
                      <td>
                        <div className='d-flex flex-column'>
                          {a.pendingActions &&
                            a.pendingActions.map((task, indexTask) => (
                              <span key={indexTask}>{task.taskID}</span>
                            ))}
                        </div>
                      </td>
                      <td>{a.pendingActions[0].pendingDate}</td>
                      <td>
                        <Link
                          type='button'
                          className='btn btn-link d-flex justify-content-start'
                          to='/action-detail'
                        >
                          {a.pendingActions[0].type}
                        </Link>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>

            <div
              className={`d-flex justify-content-between align-items-center ${styles.bottom}`}
            >
              <button type='button' className='btn btn-link'>
                Show more
              </button>
              <Pagination>
                <Pagination.Item
                  onClick={() => {
                    this.goToPreviousPage();
                  }}
                  className='previous flip-horizontal'
                  disabled={currentPage === 1}
                >
                  <PaginationNextIcon />
                </Pagination.Item>
                {items}
                <Pagination.Item
                  onClick={() => {
                    this.goToNextPage();
                  }}
                  disabled={currentPage === totalPage}
                  className='next'
                >
                  <PaginationNextIcon />
                </Pagination.Item>
              </Pagination>
            </div>
          </div>
        </div>
      </div>
    );
    }
    else{

      return (
        <div
          style={{
            margin: 0,
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: "translateY(-50%)"
          }}
          className="d-flex justify-content-center align-items-center vertical-center"
        >
          <ReactLoading
            type="spin"
            color="rgba(0, 155, 255, 0.93)"
            height={50}
            width={50}
          />
        </div>
      );
    }

  }
}

MyWorkSchedule.propTypes = {
  workSchedules: PropTypes.object.isRequired,
  fetchWorkSchedule: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  console.log(state);
  return {
    workSchedules: state.workSchedules || {}
  };
};
export default connect(mapStateToProps, {
  fetchWorkSchedule
})(MyWorkSchedule);
