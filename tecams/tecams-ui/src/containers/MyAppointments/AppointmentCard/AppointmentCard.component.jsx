import React, {useState, useEffect} from 'react';
import './AppointmentCard.styles.scss';
import * as _ from 'lodash';

const AppointmentCard = props => {
  const [data, setData] = useState ([]);
  const [dataComplete, setDataComplete] = useState ({});
  const [total, setTotal] = useState (0);

  useEffect (() => {
    convertValuePercent ();
  });

  /**
   * convert value percent
   */
  const convertValuePercent = () => {
    let total = 0;
    _.forEach (props.data, element => {
      total += element.count;
    });
    _.forEach (props.data, element => {
      if (element.count !== 0) {
        element.percent = element.count / total * 100;
      } else {
        element.percent = 0;
      }

      if (element.status === 'Complete') {
        setDataComplete (element);
      }
    });

    setTotal (total);
    setData (props.data);
  };

  /**
   * format to currency number
   * @param {number} number
   */
  const toCurrency = number => {
    const formatter = new Intl.NumberFormat ('en-US', {
      style: 'decimal',
      currency: 'SEK',
      maximumFractionDigits: 1,
    });

    return formatter.format (number);
  };

  return (
    <div className="AppointmentCard">
      <span className="count">{total}</span>
      <span className="bold-title">{props.title}</span>
      <div className="data-statistics">
        <span className="status">
          {toCurrency (dataComplete.percent)}
          % Completed
        </span>
        <div className="chart">
          {data &&
            data.length > 0 &&
            data.map ((item, index) => (
              <div
                key={`chart-item` + index}
                className={`chart-item ${item.status === 'Confirmed' ? 'confirmed' : item.status === 'Requested' ? 'requested' : item.status === 'Reserved' ? 'reserved' : item.status === 'In Progress' ? 'in-progress' : item.status === 'Complete' ? 'complete' : item.status === 'Incompleted' ? 'incomplete' : 'canceled'}`}
                style={{width: item.percent + '%'}}
              >
                <span className="hint center-child">
                  {item.count} {item.status}
                </span>
              </div>
            ))}
        </div>
        <div className="legends">
          {data &&
            data.length > 0 &&
            data.map ((item, index) => (
              <div className="legend-item" key={`legend-item` + index}>
                <span
                  className={`dot ${item.status === 'Confirmed' ? 'confirmed' : item.status === 'Requested' ? 'requested' : item.status === 'Reserved' ? 'reserved' : item.status === 'In Progress' ? 'in-progress' : item.status === 'Complete' ? 'complete' : item.status === 'Incompleted' ? 'incomplete' : 'canceled'}`}
                />
                <span className="name">{item.status}</span>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};

export default AppointmentCard;
