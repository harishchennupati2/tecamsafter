import './styles.scss';

import iconWhitePlus from 'assets/icons/ic-white-plus.svg';
import iconFilter from 'assets/icons/ic-filter.svg';
import iconSearch from 'assets/icons/ic-gray-search.svg';

import {PAGE_SIZE, PAGE} from '../../config/constants';

import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {fetchAppointments, fetchUpcommingAppointments} from 'redux/actions';

import Pagination from './Pagination/Pagination.component';
import AppointmentCard from './AppointmentCard/AppointmentCard.component';
import TableAppointment from './TableAppointment/TableAppointment';
import FilterAppointment from './FilterAppointment/FilterAppointment';
import ModalError from './ModalError.component';

import ReactLoading from 'react-loading';
import moment from 'moment';

const MyAppointments = ({
  appointments,
  upcommingAppointment,
  summaryMetrics,
  fetchAppointments,
  fetchUpcommingAppointments,
  error,
  details,
}) => {
  const [showSummaryMetrics, setShowSummaryMetrics] = useState (true);
  const [modalError, setModalError] = useState (false);
  const [role] = useState (localStorage.getItem ('role'));
  const [paramsAppointment, setParamsAppointment] = useState ({
    page: PAGE,
    perPage: PAGE_SIZE,
    order: 'desc',
    orderBy: 'startDate, endDate',
  });
  const [
    paramsUpcommingAppointment,
    setParamsUpcommingAppointment,
  ] = useState ({
    page: 1,
    perPage: 5,
    order: 'desc',
    orderBy: 'startDate, endDate',
  });
  const [querySearch, setQuerySearch] = useState ('');
  const [showFilter, setShowFilter] = useState (false);
  const [filter, setFilter] = useState ({});
  useEffect (
    () => {
      localStorage.removeItem ('appointment');
      fetchUpcommingAppointments (
        `_page=${paramsUpcommingAppointment.page}&_limit=${paramsUpcommingAppointment.perPage}&_sort=${paramsUpcommingAppointment.orderBy}&_order=${paramsUpcommingAppointment.order}&type=Upcomming`
      );
      fetchAppointments (
        `_page=${paramsAppointment.page}&_limit=${paramsAppointment.perPage}&_sort=${paramsAppointment.orderBy}&_order=${paramsAppointment.order}&type=All`
      );
    },
    [
      fetchUpcommingAppointments,
      fetchAppointments,
      paramsAppointment,
      paramsUpcommingAppointment,
    ]
  );

  const changePagination = result => {
    setParamsAppointment ({
      ...paramsAppointment,
      page: 1,
      [result.event]: result[result.event],
    });
  };

  /**
   * sort table appointments
   * @param {string} column column
   * @param {string} order order
   */
  const sortAppointment = (column, order) => {
    setParamsAppointment ({
      ...paramsAppointment,
      order,
      orderBy: column,
    });
    fetchAppointments (
      `_page=${paramsAppointment.page}&_limit=${paramsAppointment.perPage}&_sort=${paramsAppointment.orderBy}&_order=${paramsAppointment.order}&type=All`
    );
  };

  /**
   * sort table upcomming appointments
   * @param {string} column column
   * @param {string} order order
   */
  const sortUpcommingAppointment = (column, order) => {
    setParamsUpcommingAppointment ({
      ...paramsUpcommingAppointment,
      order,
      orderBy: column,
    });
    fetchUpcommingAppointments (
      `_page=${paramsUpcommingAppointment.page}&_limit=${paramsUpcommingAppointment.perPage}&_sort=${paramsUpcommingAppointment.orderBy}&_order=${paramsUpcommingAppointment.order}&type=Upcomming`
    );
  };

  /**
   * on change search table
   * @param {string} value value search input
   */
  const onChangeSearch = value => {
    setQuerySearch (value);
    fetchAppointments (
      `_page=${paramsAppointment.page}&_limit=${paramsAppointment.perPage}&_sort=${paramsAppointment.orderBy}&_order=${paramsAppointment.order}&q=${value}&type=All`
    );
  };

  /**
   * on close filter
   * @param {boolean} result result close filter
   */
  const onCloseFilter = result => {
    setShowFilter (result);
  };

  /**
   * on close filter
   * @param {boolean} result result close filter
   */
  const onClear = () => {
    setFilter ({});
    fetchAppointments (
      `_page=${paramsAppointment.page}&_limit=${paramsAppointment.perPage}&_sort=${paramsAppointment.orderBy}&_order=${paramsAppointment.order}&type=All`
    );
  };

  /**
   * on filter appointment
   * @param {any} result result filter
   */
  const onFilter = result => {
    let str;
    if (result.customer) {
      str += `&customer=${result.customer}`;
    }
    if (result.technician) {
      str += `&technicians?name=${result.technician}`;
    }
    if (result.startDate) {
      str += `&startDate=${moment (result.startDate).format ('MM/DD/YYYY HH:mm')}`;
    }
    if (result.endDate) {
      str += `&endDate=${moment (result.endDate).format ('MM/DD/YYYY HH:mm')}`;
    }
    if (result.appointmentType) {
      str += `&appointmentType=${result.appointmentType}`;
    }
    if (result.appointmentSubType) {
      str += `&appointmentSubType=${result.appointmentSubType}`;
    }
    if (result.status) {
      str += `&status=${result.status}`;
    }
    setFilter (result);
    fetchAppointments (
      `_page=${paramsAppointment.page}&_limit=${paramsAppointment.perPage}&_sort=${paramsAppointment.orderBy}&_order=${paramsAppointment.order}${str}&type=All`
    );
  };
  if (
    (upcommingAppointment.results && upcommingAppointment.results.length > 0) ||
    (appointments.results && appointments.results.length > 0)
  ) {
    return (
      <div className="my-appointments-page">
        <div className="header-page d-flex align-items-center justify-content-between">
          <div className="d-flex justify-content-between align-items-center wrapAll">
            <h1>My Appointments</h1>
            <div className="control d-flex">
              <div className="toggle-switch center-child">
                <span className="name">Summary Metrics</span>
                <input
                  type="checkbox"
                  id="taskIndex-1"
                  checked={showSummaryMetrics}
                  onChange={() => setShowSummaryMetrics (!showSummaryMetrics)}
                />
                <label htmlFor="taskIndex-1">
                  <span className="ball" />
                </label>
              </div>
              {role === 'Appointment Requestor' &&
                <Link to="/new-appointment" className="btn-create center-child">
                  <img src={iconWhitePlus} alt="" />
                  <span className="name">Create Appointment</span>
                </Link>}
            </div>
          </div>
        </div>
        {/* end /header-page */}

        <div className="main-container">
          {showSummaryMetrics &&
            summaryMetrics &&
            summaryMetrics.totalAppointments &&
            <div className="appointment-cards d-flex">
              <div className="appointment-card-item">
                <AppointmentCard
                  title="Total Appointments"
                  data={summaryMetrics.totalAppointments}
                />
              </div>
              <div className="appointment-card-item">
                <AppointmentCard
                  title="Business Hours"
                  data={summaryMetrics.businessHour}
                />
              </div>
              <div className="appointment-card-item">
                <AppointmentCard
                  title="After Hours"
                  data={summaryMetrics.afterHour}
                />
              </div>
            </div>}
          {/* end /appointment-cards */}

          <div className="group-upcoming-appointments">
            <div className="head-content">
              <span className="main-title">Upcoming Appointments</span>
            </div>
            <div className="body-content">
              {upcommingAppointment &&
                <TableAppointment
                  params={paramsUpcommingAppointment}
                  appointments={upcommingAppointment.results}
                  sort={sortUpcommingAppointment}
                />}
            </div>
          </div>
          {/* end /group-upcoming-appointments */}

          <div className="group-upcoming-appointments all-appointments">
            <div className="head-content">
              <span className="main-title">All Appointments</span>
              <div className="group-actions">
                <div className="box-search">
                  <img src={iconSearch} alt="ICON" />
                  <input
                    type="text"
                    placeholder="Search appointment …"
                    value={querySearch}
                    onChange={event => onChangeSearch (event.target.value)}
                  />
                </div>
                <span
                  className="btn-filter center-child"
                  onClick={() => setShowFilter (!showFilter)}
                >
                  <img src={iconFilter} alt="ICON" />
                  <span className="name">Filter</span>
                </span>
                {showFilter &&
                  <FilterAppointment
                    data={filter}
                    onClose={onCloseFilter}
                    onFilter={onFilter}
                    onClear={onClear}
                  />}
              </div>
            </div>
            {appointments &&
              <div className="body-content">
                <TableAppointment
                  params={paramsAppointment}
                  appointments={appointments.results}
                  sort={sortAppointment}
                />
                <Pagination
                  total={appointments.total}
                  page={paramsAppointment.page}
                  perPage={paramsAppointment.perPage}
                  maxSize={5}
                  changePagination={changePagination}
                />
              </div>}
          </div>
          {/* end /group-upcoming-appointments */}
        </div>
        {/* end /main-container */}
      </div>
    );
  } else {
    if (error) {
      return (
        <ModalError
          tryAgain="true"
          show={error}
          onHide={() => setModalError (false)}
        />
      );
    }
    return (
      <div
        style={{
          margin: 0,
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translateY(-50%)',
        }}
        className="d-flex justify-content-center align-items-center vertical-center"
      >
        <ReactLoading
          type="spin"
          color="rgba(0, 155, 255, 0.93)"
          height={50}
          width={50}
        />
      </div>
    );
  }
};

const mapStateToProps = ({
  appointments,
  upcommingAppointment,
  summaryMetrics,
  details,
  error,
}) => {
  return {
    appointments: {...appointments},
    upcommingAppointment: {...upcommingAppointment},
    summaryMetrics: {...summaryMetrics},
    error: error,
    details: details,
  };
};

const mapDispatchToProps = {
  fetchAppointments,
  fetchUpcommingAppointments,
};

export default connect (mapStateToProps, mapDispatchToProps) (MyAppointments);
