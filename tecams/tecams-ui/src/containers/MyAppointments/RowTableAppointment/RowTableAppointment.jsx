import React, { useState } from 'react';
import ClickOutHandler from 'react-clickout-handler';

import iconMenuDots from 'assets/icons/ic-gray-dots.svg';
import iconPen from 'assets/icons/ic-blue-update.svg';
import iconClock from 'assets/icons/ic-blue-reschedule.svg';
import iconBin from 'assets/icons/ic-red-delete.svg';

import moment from 'moment';
import { Link } from 'react-router-dom';

const RowTableAppointment = ({ appointment }) => {
  const [action, setAction] = useState(false);

  return (
    <tr>
      <td className='col01'>
        <Link to={`/appointment-details/${appointment.appointmentId}`} className='value id'>{appointment.appointmentId}</Link>
      </td>
      <td className='col02'>
        <span className='value'>{appointment.customer}</span>
      </td>
      <td className='col03'>
        <div className='group-technician'>
          <span className='value'>
            {
              appointment.technicians && appointment.technicians.length === 1 ?
                `${appointment.technicians[0].name}` :
                `${appointment.technicians[0].name} & ${appointment.technicians.length - 1} others`
            }
          </span>
          <div className='tooltip-technician style-tooltip'>
            <span className='bold-title'>{appointment.technicians.length} Technicians</span>
            <div className='members'>
              {
                appointment.technicians.map((technician, index) => (
                  <span className='member' key={`technician` + index}>{technician.name}</span>
                ))
              }
            </div>
          </div>
        </div>
      </td>
      <td className='col04'>
        <div className='group-datetime'>
          <div className='current-content'>
            <span className='value'>
              <span className='bold'>Start:</span>
              {moment(appointment.startDate).format('DD/MM/YYYY hh:mm A')}
            </span>
            <span className='value'>
              <span className='bold'>End:</span>
              {moment(appointment.endDate).format('DD/MM/YYYY hh:mm A')}
            </span>
          </div>
          <div className='tooltip-datetime style-tooltip'>
            <span className='bold-title'>Appointment Date/ Time</span>
            <span className='bold-subtitle'>(Customer site local time)</span>
            <div className='current-child-content'>
              <span className='value'>
                <span className='bold'>Start:</span>
                {moment(appointment.startDate).format('DD/MM/YYYY hh:mm A')}
              </span>
              <span className='value'>
                <span className='bold'>End:</span>
                {moment(appointment.endDate).format('DD/MM/YYYY hh:mm A')}
              </span>
            </div>
          </div>
        </div>
      </td>
      <td className='col05'>
        <span className='value'>{appointment.appointmentType}</span>
      </td>
      <td className='col06'>
        <span className='value'>{appointment.appointmentSubType}</span>
      </td>
      <td className='col07'>
        <div className='status'>
          <span className={`dot ${appointment.status === 'Confirmed' ? 'confirmed' :
            appointment.status === 'Requested' ? 'requested' :
              appointment.status === 'Reserved' ? 'reserved' :
                appointment.status === 'In Progress' ? 'in-progress' :
                  appointment.status === 'Complete' ? 'complete' :
                    appointment.status === 'Incompleted' ? 'incomplete' : 'canceled'}`}></span>
          <span className='name'>{appointment.status}</span>
        </div>
      </td>
      <td className='col08'>
        <ClickOutHandler onClickOut={() => setAction(false)}>
          <div className='group-action'>
            <span className='btn-menu center-child' onClick={() => setAction(!action)}>
              <img src={iconMenuDots} alt='' />
            </span>
            {
              action && (
                <div className='tooltip-actions style-tooltip'>
                  <span className='item'>
                    <img src={iconPen} alt='' />
                    <span className='name'>Update Appointment</span>
                  </span>
                  <span className='item'>
                    <img src={iconClock} alt='' />
                    <span className='name'>Reschedule Appointment</span>
                  </span>
                  <span className='item last'>
                    <img src={iconBin} alt='' />
                    <span className='name'>Cancel Appointment</span>
                  </span>
                </div>
              )
            }
          </div>
        </ClickOutHandler>
      </td>
    </tr>
  )
}

export default RowTableAppointment
