import React from 'react';
import './styles.scss';
import arrows from '../../../assets/icons/ic-blue-arrow-next.svg';

const Pagination = ({ total, page, perPage, maxSize, changePagination }) => {

  const updatePageLinks = () => {
    return createPageArray(page, perPage, total, maxSize);
  }

  const createPageArray = (currentPage, itemsPerPage, totalItems, paginationRange) => {
    // paginationRange could be a string if passed from attribute, so cast to number.
    paginationRange = +paginationRange;
    const pages = [];
    const totalPages = Math.ceil(totalItems / itemsPerPage);
    const halfWay = Math.ceil(paginationRange / 2);

    const isStart = currentPage <= halfWay;
    const isEnd = totalPages - halfWay < currentPage;
    const isMiddle = !isStart && !isEnd;

    const ellipsesNeeded = paginationRange < totalPages;
    let i = 1;

    while (i <= totalPages && i <= paginationRange) {
      let label;
      const pageNumber = calculatePageNumber(i, currentPage, paginationRange, totalPages);
      const openingEllipsesNeeded = (i === 2 && (isMiddle || isEnd));
      const closingEllipsesNeeded = (i === paginationRange - 1 && (isMiddle || isStart));
      if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
        label = '...';
      } else {
        label = pageNumber;
      }
      pages.push({
        label,
        value: pageNumber
      });
      i++;
    }
    return pages;
  }

  /**
   * Given the position in the sequence of pagination links [i],
   * figure out what page number corresponds to that position.
   */
  const calculatePageNumber = (i, currentPage, paginationRange, totalPages) => {
    const halfWay = Math.ceil(paginationRange / 2);
    if (i === paginationRange) {
      return totalPages;
    } else if (i === 1) {
      return i;
    } else if (paginationRange < totalPages) {
      if (totalPages - halfWay < currentPage) {
        return totalPages - paginationRange + i;
      } else if (halfWay < currentPage) {
        return currentPage - halfWay + i;
      } else {
        return i;
      }
    } else {
      return i;
    }
  }

  return (
    <div className='showing-n-pagination'>
      <div className='group-showing'>
        {
          perPage === 10 ? (
            <button className='display' onClick={() => changePagination({ event: 'perPage', perPage: 20 })}>Show More</button>
          ) : perPage === 20 && (
            <button className='display' onClick={() => changePagination({ event: 'perPage', perPage: 10 })}>Show Less</button>
          )
        }
      </div>
      <div className='group-pagination'>
        <ul className='pagination'>
          <li className={`page-item ${page === 1 ? 'disabled' : ''}`}>
            <button className='page-link btn-prev' onClick={() => changePagination({ event: 'page', page: page - 1 })}>
              <img src={arrows} alt='ICON' />
            </button>
          </li>
          {
            updatePageLinks().map((item, index) => (
              <li className={`page-item ${item.label === page ? 'active' : ''} ${item.label === '...' ? 'disabled' : ''}`} key={index}>
                <button className='page-link' onClick={() => changePagination({ event: 'page', page: item.value })}>
                  <span className='btn-page'>{item.label}</span>
                </button>
              </li>
            ))
          }
          <li className={`page-item ${page === updatePageLinks().length ? 'disabled' : ''}`}>
            <button className='page-link btn-prev btn-next' onClick={() => changePagination({ event: 'page', page: page + 1 })}>
              <img src={arrows} alt='ICON' />
            </button>
          </li>
        </ul>
      </div>
    </div>
  )
};

export default Pagination;
