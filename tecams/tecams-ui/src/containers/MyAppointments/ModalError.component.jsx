import './styles.scss';
import iconClose from 'assets/icons/ic-red-close.svg';

import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Spinner from 'react-bootstrap/Spinner';

const ModalError = ({ tryAgain, show, onHide }) => {
    const [showModal, setShowModal] = useState(true);
    const [showError, setShowError] = useState(false);
    const [showDetails, setShowDetails] = useState(false);

    const handleClose = () => setShowModal(false);

    const showErrorDetails = () => {
        setShowError(!showError);
        setTimeout(() => {
            setShowDetails(true);
        }, 2000);
    }

    return (
        <>
            <Modal show={showModal} onHide={handleClose} dialogClassName='modal-error-apt' centered>
                <div className='modal-container'>
                    <div className='group-title'>
                        <span className='big-title'>Error</span>
    <span className='warning'>Server returned error with code {show.response.data.status}</span>
                    </div>
                    <div className='group-description'>
                        <span className='notice'>Sorry Try again after some time</span>
                        <span className='status' onClick={() => showErrorDetails()}>{showError ? 'Hide' : 'Show'} Error Details</span>
                        {
                            showError && (
                                <div className='error-details-content'>
                                    {
                                        showDetails ? (
                                            <div className="group-content">
                                                <p>Error code {show.response.data.status}
                                                </p>
                                                <p>Error message {show.response.data.message}</p>
                                                <p>
                                                    {JSON.stringify(show.toJSON().message)}

                                                 </p>
                                            </div>
                                        ) : (
                                                <div className="box-spinner">
                                                    <Spinner animation="border" />
                                                </div>
                                            )
                                    }
                                </div>
                            )
                        }
                    </div>
                    <div className='buttons'>
                        <div className='style-btn btn-cancel center-child' onClick={handleClose}>Close</div>
                    </div>
                </div>
            </Modal>
        </>
    );
};

export default ModalError;
