import React, { useState } from 'react';
import './FilterAppointment.scss';
import icGrayCalen from 'assets/icons/ic-gray-calen.svg';
import icGrayClose from 'assets/icons/ic-gray-close.svg';
import * as constants from '../../../config/constants';
import Dropdown from '../../../shared/components/Dropdown/Dropdown.component';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import _ from 'lodash';

const FilterAppointment = ({ data, onClose, onFilter, onClear }) => {
  const [filter, setFilter] = useState(_.cloneDeep(data));
  const [customerOptions] = useState(constants.CUSTOMERS);
  const [technicianOptions] = useState(constants.TECHNICIAN);
  const [appointmentTypeOptions] = useState(constants.APPOINTMENT_TYPES);
  const [appointmentSubTypeOptions] = useState(constants.APPOINTMENT_SUB_TYPES);
  const [statusOptions] = useState(constants.STATUS);

  const renderDayContents = (day, date) => {
    return <span>{new Date(date).getDate()}</span>;
  };

  const onChangeValueDropdown = (value, label) => {
    setFilter({
      ...filter,
      [label]: value
    })
  }

  const onChangeDate = (value, label) => {
    setFilter({
      ...filter,
      [label]: value
    })
  }

  return (
    <div className="box-filter">
      <span className="title-box">Filter</span>
      <span className="click-close" onClick={() => onClose(false)}>
        <img src={icGrayClose} alt="icon" />
      </span>
      <ul className="list-input">
        <li className="item-list">
          <span className="name">Customer</span>
          <div className="cover-input dropdown-ver">
            <Dropdown
              classes='customer'
              label='customer'
              selectedValue={filter.customer ? filter.customer : ''}
              options={customerOptions}
              onChangeDropdown={onChangeValueDropdown} />
          </div>
        </li>
        <li className="item-list">
          <span className="name">Technician</span>
          <div className="cover-input dropdown-ver">
            <Dropdown
              classes='technician'
              label='technician'
              selectedValue={filter.technician ? filter.technician : ''}
              options={technicianOptions}
              onChangeDropdown={onChangeValueDropdown} />
          </div>
        </li>
        <li className="item-list">
          <span className="name">Start Date/ Time</span>
          <div className="cover-input calen">
            <DatePicker
              renderDayContents={renderDayContents}
              selected={filter.startDate ? new Date(filter.startDate) : null}
              onChange={date => onChangeDate(date, 'startDate')}
              dateFormat="MM/dd/yyyy"
            />
            <span className="click-calen">
              <img src={icGrayCalen} alt="icon" />
            </span>
          </div>
        </li>
        <li className="item-list">
          <span className="name">End Date/ Time</span>
          <div className="cover-input calen">
            <DatePicker
              renderDayContents={renderDayContents}
              selected={filter.endDate ? new Date(filter.endDate) : null}
              onChange={date => onChangeDate(date, 'endDate')}
              dateFormat="MM/dd/yyyy"
            />
            <span className="click-calen">
              <img src={icGrayCalen} alt="icon" />
            </span>
          </div>
        </li>
        <li className="item-list">
          <span className="name">Appointment Type</span>
          <div className="cover-input dropdown-ver">
            <Dropdown
              classes='appointment-type'
              label='appointmentType'
              selectedValue={filter.appointmentType ? filter.appointmentType : ''}
              options={appointmentTypeOptions}
              onChangeDropdown={onChangeValueDropdown} />
          </div>
        </li>
        <li className="item-list">
          <span className="name">Appointment Sub Type</span>
          <div className="cover-input dropdown-ver">
            <Dropdown
              classes='appointment-sub-type'
              label='appointmentSubType'
              selectedValue={filter.appointmentSubType ? filter.appointmentSubType : ''}
              options={appointmentSubTypeOptions}
              onChangeDropdown={onChangeValueDropdown} />
          </div>
        </li>
        <li className="item-list">
          <span className="name">Appointment Status</span>
          <div className="cover-input dropdown-ver">
            <Dropdown
              classes='status'
              label='status'
              selectedValue={filter.status ? filter.status : ''}
              options={statusOptions}
              onChangeDropdown={onChangeValueDropdown} />
          </div>
        </li>
      </ul>

      <div className='buttons'>
        <button className="btn-clear" onClick={() => { onClear(); setFilter({}) }}>Clear Filter</button>
        <button className="btn-apply" onClick={() => { onFilter(filter); onClose(false) }}>Apply</button>
      </div>
    </div>
  )
}

export default FilterAppointment;