import React from 'react';
import './TableAppointment.scss';

import RowTableAppointment from '../RowTableAppointment/RowTableAppointment';

const TableAppointment = ({ params, appointments, sort }) => {
  /**
   * sort column table
   * @param {string} column column table
   */
  const sortColumn = (column) => {
    let order;
    if (column !== params.orderBy) {
      order = 'asc';
    } else {
      order = params.order === 'asc' ? 'desc' : 'asc';
    }
    sort(column, order);
  }

  return (
    <div className='appointment-table'>
      <table className='main-table'>
        <thead>
          <tr>
            <th className='col01'>
              <span className={`name-col ${params.orderBy === 'appointmentId' && params.order === 'asc' ? 'active' : ''}`}>
                <span className='text' onClick={() => sortColumn('appointmentId')}>Appointment ID</span>
                {
                  params.orderBy === 'appointmentId' && <span className='symbol'></span>
                }
              </span>
            </th>
            <th className='col02'>
              <span className={`name-col ${params.orderBy === 'customer' && params.order === 'asc' ? 'active' : ''}`}>
                <span className='text' onClick={() => sortColumn('customer')}>Customer</span>
                {
                  params.orderBy === 'customer' && <span className='symbol'></span>
                }
              </span>
            </th>
            <th className='col03'>
              <span className={`name-col ${params.orderBy === 'technician' && params.order === 'asc' ? 'active' : ''}`}>
                <span className='text' onClick={() => sortColumn('technician')}>Technician</span>
                {
                  params.orderBy === 'technician' && <span className='symbol'></span>
                }
              </span>
            </th>
            <th className='col04'>
              <span className={`name-col ${params.orderBy === 'startDate, endDate' && params.order === 'asc' ? 'active' : ''}`}>
                <span className='text' onClick={() => sortColumn('startDate, endDate')}>Date/ Time</span>
                {
                  params.orderBy === 'startDate, endDate' && <span className='symbol'></span>
                }
              </span>
            </th>
            <th className='col05'>
              <span className={`name-col ${params.orderBy === 'appointmentType' && params.order === 'asc' ? 'active' : ''}`}>
                <span className='text' onClick={() => sortColumn('appointmentType')}>Type</span>
                {
                  params.orderBy === 'appointmentType' && <span className='symbol'></span>
                }
              </span>
            </th>
            <th className='col06'>
              <span className={`name-col ${params.orderBy === 'appointmentSubType' && params.order === 'asc' ? 'active' : ''}`}>
                <span className='text' onClick={() => sortColumn('appointmentSubType')}>Sub Type</span>
                {
                  params.orderBy === 'appointmentSubType' && <span className='symbol'></span>
                }
              </span>
            </th>
            <th className='col07'>
              <span className={`name-col ${params.orderBy === 'status' && params.order === 'asc' ? 'active' : ''}`}>
                <span className='text' onClick={() => sortColumn('status')}>Status</span>
                {
                  params.orderBy === 'status' && <span className='symbol'></span>
                }
              </span>
            </th>
            <th className='col08'>
              <span className='name-col'>
                <span className='text'>Action</span>
              </span>
            </th>
          </tr>
        </thead>
        <tbody>
          {appointments &&
            appointments.map((appointment, index) => (
              <RowTableAppointment key={`appointment` + index} appointment={appointment} />
            ))}
        </tbody>
      </table>
    </div>
  );
};

export default TableAppointment;
