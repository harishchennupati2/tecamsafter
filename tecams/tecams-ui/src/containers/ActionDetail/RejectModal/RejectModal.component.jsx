import React from 'react';
import PropTypes from 'prop-types';
import Dropdown from 'react-bootstrap/Dropdown';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';

import styles from './styles.module.scss';

class RejectModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reason: 'Select...',
      note: ''
    };
  }

  render() {
    const reasons = ['Reason 1', 'Reason 2', 'Reason 3'];
    const { reason, note } = this.state;
    const { show, onClose } = this.props;

    return (
      <Modal show={show} onHide={onClose}>
        <Modal.Body>
          <div
            className={`d-flex flex-column ${styles['component-container']}`}
          >
            <span className={`${styles.title}`}>
              Are you sure you want to reject?
            </span>
            <span className={`${styles['sub-title']}`}>
              The entire appointment will be cancelled
            </span>
            <Form>
              <Form.Group controlId='reason'>
                <Form.Label className="required">Reason</Form.Label>
                <Dropdown className='full-width'>
                  <Dropdown.Toggle>{reason}</Dropdown.Toggle>

                  <Dropdown.Menu>
                    {reasons.map(t => (
                      <Dropdown.Item
                        key={t}
                        onSelect={() => {
                          this.setState({ reason: t });
                        }}
                      >
                        {t}
                      </Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
              </Form.Group>

              <Form.Group controlId='formBasicPassword'>
                <Form.Label className="required">Notes</Form.Label>
                <Form.Control
                  onChange={event => {
                    this.setState({ note: event.currentTarget.value });
                  }}
                  placeholder='Enter…'
                  as='textarea'
                  rows='3'
                />
              </Form.Group>
            </Form>

            <div
              className={`d-flex justify-content-between ${styles['bottom-buttons']}`}
            >
              <button onClick={onClose} type='button' className='btn btn-light'>
                Cancel
              </button>
              <button
                onClick={onClose}
                disabled={reason === 'Select...' || !note}
                type='button'
                className='btn btn-danger'
              >
                Confirm Reject
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

RejectModal.defaultProps = {
  show: false
};

RejectModal.propTypes = {
  show: PropTypes.bool
};

export default RejectModal;
