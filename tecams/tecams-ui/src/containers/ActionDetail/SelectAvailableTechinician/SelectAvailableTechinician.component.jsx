import React from 'react';
import PropTypes from 'prop-types'
import Dropdown from 'react-bootstrap/Dropdown';

import styles from './styles.module.scss';

class SelectAvailableTechinician extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTechinician: 'Select...'
    };
  }

  render() {
    const tehchnincians = [
      'Tehchnincian 1',
      'Tehchnincian 2',
      'Tehchnincian 3'
    ];
    const { selectedTechinician } = this.state;
    const { className, onClose } = this.props;

    return (
      <div className={`d-flex flex-column justify-content-between ${styles['component-container']} ${className}`}>
        <div className='d-flex flex-column'>
          <span className={`${styles.title}`}>
            Select Available Techinician
          </span>
          <Dropdown className='full-width'>
            <Dropdown.Toggle>{selectedTechinician}</Dropdown.Toggle>

            <Dropdown.Menu>
              {tehchnincians.map((t) => (
                <Dropdown.Item
                  key={t}
                  onSelect={() => {
                    this.setState({ selectedTechinician: t });
                  }}
                >
                  {t}
                </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
        </div>

        <div className={`d-flex ${styles['bottom-buttons']}`}>
          <button onClick={onClose} type='button' className='btn btn-light'>
            Cancel
          </button>
          <button onClick={onClose} type='button' className='btn btn-primary'>
            Confirm
          </button>
        </div>
      </div>
    );
  }
}

SelectAvailableTechinician.defaultProps = {
  className: '',
  onClose: () => {},
};

SelectAvailableTechinician.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
};

export default SelectAvailableTechinician;
