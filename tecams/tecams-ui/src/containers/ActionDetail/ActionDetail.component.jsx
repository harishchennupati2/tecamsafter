import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import accordionCollapseIcon from 'assets/icons/accordion-collapse.svg';
import { fetchActionDetail } from "redux/actions";

import SelectAvailableTechinician from './SelectAvailableTechinician/SelectAvailableTechinician.component';
import RejectModal from './RejectModal/RejectModal.component';

import styles from './styles.module.scss';

class ActionDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      close: {},
      isShowAllocates: {},
      isShowRejectModal: false
    };
  }

  componentDidMount() {
    this.props.fetchActionDetail();
  }

  render() {
    const { close, isShowRejectModal, isShowAllocates } = this.state;
    const { actionDetail: accordions } = this.props;
    return (
      <div className='d-flex flex-column'>
        <h1 className={styles.title}>Appointment TECAMS1582073392TJTU</h1>
        <div
          className={`d-flex align-items-center ${styles['status-container']}`}
        >
          <span className={styles.dot}></span>
          <span>Requested</span>
        </div>

        <div className={`d-flex center-child ${styles['content-container']}`}>
          <div className={`d-flex flex-column ${styles.accordion}`}>
            {accordions.map((accordion, index) => (
              <Accordion
                key={index}
                activeKey={close[`${index}`] ?  '' : `${index}`}
              >
                <Card>
                  <Accordion.Toggle
                    onClick={() => {
                      if (accordion.sections && accordion.sections.length > 0) {
                        close[`${index}`] = !close[`${index}`];
                        this.setState({ close });
                      }
                    }}
                    as={Card.Header}
                    eventKey={`${index}`}
                  >
                    <div className='d-flex alignn-items-center justify-content-between'>
                      <span className={styles['header-title']}>
                        {accordion.title}
                      </span>
                      <img
                        className={close[`${index}`] ? '' : 'flip-vertical'}
                        src={accordionCollapseIcon}
                        alt=''
                      />
                    </div>
                  </Accordion.Toggle>
                  <Accordion.Collapse eventKey={`${index}`}>
                    <Card.Body className={`d-flex flex-column ${accordion.sections.length === 0 ? 'empty' : ''}`}>
                      {accordion.sections &&
                        accordion.sections.map((section, indexSection) => {
                          let isShowAllocate = isShowAllocates[section.id];
                          return (
                            <div
                              key={indexSection}
                              className='d-flex flex-column'
                            >
                              {!isShowAllocate && section.title && (
                                <span
                                  className={`${styles['body-title']} ${styles['body-title-1']}`}
                                >
                                  {section.title}
                                </span>
                              )}

                              {!isShowAllocate &&
                                section.infos &&
                                section.infos.map((info, indexInfo) => (
                                  <div
                                    key={indexInfo}
                                    className='d-flex flex-column'
                                  >
                                    {info.title && (
                                      <span
                                        className={`${styles['body-title']} ${styles['body-title-2']}`}
                                      >
                                        {info.title}
                                      </span>
                                    )}
                                    {info.subInfos &&
                                      info.subInfos.map(
                                        (subInfo, indexSubInfo) => (
                                          <div
                                            key={indexSubInfo}
                                            className={`${styles['body-info']} d-flex`}
                                          >
                                            <span>{subInfo.title}</span>
                                            <span>{subInfo.value}</span>
                                          </div>
                                        )
                                      )}
                                  </div>
                                ))}

                              {!isShowAllocate &&
                                section.extraInfos &&
                                section.extraInfos.map((info, indexInfo) => (
                                  <div
                                    key={indexInfo}
                                    className={`d-flex flex-column ${styles['group-info']}`}
                                  >
                                    {info.title && (
                                      <span
                                        className={`${styles['body-title']} ${styles['body-title-1']}`}
                                      >
                                        {info.title}
                                      </span>
                                    )}
                                    {info.subInfos &&
                                      info.subInfos.map(
                                        (subInfo, indexSubInfo) => (
                                          <div
                                            key={indexSubInfo}
                                            className={`${styles['body-info']} d-flex`}
                                          >
                                            <span>{subInfo.title}</span>
                                            <span>{subInfo.value}</span>
                                          </div>
                                        )
                                      )}
                                  </div>
                                ))}

                              {section.confirmInfo && (
                                <div className='d-flex flex-column'>
                                  {isShowAllocate ? (
                                    <SelectAvailableTechinician
                                      onClose={() => {
                                        isShowAllocates[section.id] = false;
                                        this.setState({ isShowAllocates });
                                      }}
                                      className={
                                        styles['selectAvailable-techinician']
                                      }
                                    />
                                  ) : (
                                    <div
                                      className={`d-flex ${styles['button-group']}`}
                                    >
                                      <button
                                        onClick={() => {
                                          isShowAllocates[section.id] = true;
                                          this.setState({ isShowAllocates });
                                        }}
                                        type='button'
                                        className='btn btn-info'
                                      >
                                        Allocate
                                      </button>
                                      <button
                                        onClick={() => {
                                          this.setState({
                                            isShowRejectModal: true
                                          });
                                        }}
                                        type='button'
                                        className='btn btn-danger'
                                      >
                                        Reject
                                      </button>
                                    </div>
                                  )}
                                </div>
                              )}

                              {section.note && (
                                <span className={styles['text-area']}>
                                  {section.note}
                                </span>
                              )}
                              {indexSection < accordion.sections.length - 1 && (
                                <span
                                  className={styles['body-separator']}
                                ></span>
                              )}
                            </div>
                          );
                        })}
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
            ))}
          </div>
        </div>

        <RejectModal
          onClose={() => {
            this.setState({
              isShowRejectModal: false
            });
          }}
          show={isShowRejectModal}
        />
      </div>
    );
  }
}

ActionDetail.propTypes = {
  actionDetail: PropTypes.arrayOf(PropTypes.object).isRequired,
  fetchActionDetail: PropTypes.func.isRequired,
};


const mapStateToProps = (state) => {
  return {
    actionDetail: state.actionDetail || [],
  };
};
  ;
export default connect(
  mapStateToProps,
  {
    fetchActionDetail
  }
)(ActionDetail);
