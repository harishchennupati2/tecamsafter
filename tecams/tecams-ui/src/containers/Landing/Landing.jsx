import React from 'react';
import './Landing.scss';
import {useHistory} from 'react-router-dom';
import axios from 'axios';

const Landing = () => {
  let history = useHistory ();
  const login = async role => {
    localStorage.setItem ('role', role);
    const {
      data,
    } = await axios.post ('http://localhost:3002/presentation/v1/auth', {
      username: 'ashish',
      password: 'password',
    });
    localStorage.setItem ('user', JSON.stringify (data));
    if (role !== 'Resource Manager' && role !== 'Technician') {
      history.push ('/my-appointments');
    } else if (role === 'Resource Manager') {
      history.push ('/my-work-schedule');
    }
  };

  return (
    <div className="landing-page">
      <div className="cover-login">
        <h1>Login</h1>
        <ul>
          <li>
            <button
              type="button"
              onClick={() => login ('Appointment Requestor')}
            >
              Login As Appointment Requestor
            </button>
          </li>
          <li>
            <button type="button" onClick={() => login ('System Admin')}>
              Login As System Admin
            </button>
          </li>
          <li>
            <button
              type="button"
              onClick={() => login ('Reference Data Manager')}
            >
              Login As Reference Data Manager
            </button>
          </li>
          <li>
            <button type="button" onClick={() => login ('Reporting Access')}>
              Login As Reporting Access
            </button>
          </li>
          <li>
            <button type="button" onClick={() => login ('Resource Manager')}>
              Login As Resource Manager
            </button>
          </li>
          <li>
            <button type="button">Login As Technician</button>
          </li>
        </ul>
      </div>

    </div>
  );
};

export default Landing;
