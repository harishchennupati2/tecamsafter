import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers';
import apiMiddleware from '../middleware/api';
import { createLogger } from 'redux-logger';

const middlewares = [createLogger()];
middlewares.push(apiMiddleware);
const store = createStore(rootReducer, applyMiddleware(...middlewares));
window.store = store;
export default store;
