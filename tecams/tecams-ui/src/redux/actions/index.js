import {
  API,
  FETCH_ACTION_DETAIL,
  SET_ACTION_DETAIL,
  FETCH_WORK_SCHEDULES,
  SET_WORK_SCHEDULES,
  API_ERROR,
} from './types';

import {
  GET_APPOINTMENTS,
  SET_APPOINTMENTS,
  GET_UPCOMMING_APPOINTMENTS,
  SET_UPCOMMING_APPOINTMENTS,
  GET_USERS,
  SET_USERS,
  SET_APPOINTMENT,
  GET_APPOINTMENT,
} from '../../config/constants';
import _ from 'lodash';

export function fetchActionDetail() {
  return apiAction({
    url: `${process.env.REACT_APP_BASE_URL}/actionDetail`,
    onSuccess: setArticleDetails,
    label: FETCH_ACTION_DETAIL,
  });
}

function setArticleDetails(data, headers) {
  return {
    type: SET_ACTION_DETAIL,
    payload: data,
  };
}

export function fetchWorkSchedule(sort, order, page, limit) {
  const userID = 'd876765';
  const fromDate = '2019-02-01';
  return apiAction({
    url: `${process.env.REACT_APP_BASE_URL}/appointplusportal/tecams/user-work-schedules?userID=${userID}&fromDate=${fromDate}_sort=${sort}&_order=${order}&perPage=${limit}&page=${page}`,
    data: {
      correlationId: 'e8314645-c0fe-42a7-beac-2e64f8b3ebcf',
    },
    onSuccess: (data, headers) => {
      return setWorkSchedule(data, headers, {
        page,
      });
    },
    label: FETCH_WORK_SCHEDULES,
  });
}

function setWorkSchedule(data, headers, requestData) {
  return {
    type: SET_WORK_SCHEDULES,
    payload: data,
    headers,
    requestData,
  };
}

export function fetchAppointments(payload = '') {
  return apiAction({
    url: `${
      process.env.REACT_APP_BASE_URL
    }/appointments/appointplusportal/tecams/existing-appointments${
      _.isEmpty(payload) ? '' : `?${payload}`
    }`,
    onSuccess: (data, headers) => {
      const metrics = setSummaryMetrics(data);
      return {
        type: SET_APPOINTMENTS,
        payload: {metrics, data},
        headers,
      };
    },
    onFailure: error => {
      return {
        type: API_ERROR,
        payload: error,
      };
    },
    label: GET_APPOINTMENTS,
  });
}

export function fetchUpcommingAppointments(payload = '') {
  return apiAction({
    url: `${
      process.env.REACT_APP_BASE_URL
    }/appointments/appointplusportal/tecams/existing-appointments${
      _.isEmpty(payload) ? '' : `?${payload}`
    }`,
    onSuccess: (data, headers) => {
      return setUpcommingAppointments(data, headers);
    },
    onFailure: error => {
      console.log('error', error);
      return {
        type: API_ERROR,
        payload: error,
      };
    },
    label: GET_UPCOMMING_APPOINTMENTS,
  });
}

function setUpcommingAppointments(data, headers) {
  return {
    type: SET_UPCOMMING_APPOINTMENTS,
    payload: data,
    headers,
  };
}

export function fetchAppointmentById(id) {
  return apiAction({
    url: `${process.env.REACT_APP_BASE_URL}/appointments/appointplusportal/tecams/existing-appointments/${id}`,
    onSuccess: setAppointmentById,
    label: GET_APPOINTMENT,
  });
}

function setAppointmentById(data) {
  return {
    type: SET_APPOINTMENT,
    payload: data,
  };
}

function setSummaryMetrics(data) {
  let metrics = {};
  data.forEach(item => {
    if (metrics[item.status]) {
      metrics[item.status].push(item);
    } else {
      metrics[item.status] = [item];
    }
  });
  let newMetrics = {totalAppointments: [], businessHour: [], afterHour: []};
  for (var key in metrics) {
    if (metrics.hasOwnProperty(key)) {
      newMetrics.totalAppointments.push({
        status: key,
        count: metrics[key].length,
      });
      let businessHourCount = 0;
      let appointmentHourCount = 0;
      metrics[key].forEach(item => {
        if (item.appointmentType === 'Business Hours') {
          businessHourCount += 1;
        } else {
          appointmentHourCount += 1;
        }
      });
      newMetrics.businessHour.push({
        status: key,
        count: businessHourCount,
      });
      newMetrics.afterHour.push({
        status: key,
        count: appointmentHourCount,
      });
    }
  }
  return newMetrics;
}

export function fetchUsers(query) {
  return apiAction({
    url: `${process.env.REACT_APP_BASE_URL}/appointplusportal/tecams/user-summary`,
    onSuccess: setUsers,
    label: GET_USERS,
    data: {
      userID:query,
      partialName: query
    }
  });
}

function setUsers(data) {
  return {
    type: SET_USERS,
    payload: data.userProfiles,
  };
}

export function apiAction({
  url = '',
  method = 'GET',
  data = null,
  onSuccess = () => {},
  onFailure,
  label = '',
  headersOverride = null,
}) {
  return {
    type: API,
    payload: {
      url,
      method,
      data,
      onSuccess,
      onFailure,
      label,
      headersOverride,
    },
  };
}
