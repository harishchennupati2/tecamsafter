import moment from 'moment'
import _ from 'lodash'
import {
  GET_REQUIRED_EXTENSION_TYPES, GET_WORK_TYPES,
  SET_REQUIRED_EXTENSION_TYPES, SET_WORK_TYPES,
  GET_REQUIRED_EXTENSION_TYPES_FOR_WORKTYPE,
  SET_REQUIRED_EXTENSION_TYPES_FOR_WORKTYPE,
  GET_SLOTS, SET_SLOTS, POST_APPOINTMENT, POST_APPOINTMENT_SUCCESS, POST_APPOINTMENT_FAILURE
} from './types'
import { DATE_FORMAT, LEVEL, STATES } from '../../config/constants'
import { apiAction } from './index'

export function fetchRequiredExtensionTypes (level, appointmentType, appointmentSubType, productFamily, workTypeID) {
  return apiAction({
    url: `${process.env.REACT_APP_BASE_URL}/appointplusportal/tecams/required-extension-types`,
    onSuccess: setRequiredExtensionTypes,
    label: GET_REQUIRED_EXTENSION_TYPES,
    data: {
      level,
      appointmentType,
      appointmentSubType,
      productFamily,
      workTypeID
    }
  })
}

function setRequiredExtensionTypes (data) {
  return {
    type: SET_REQUIRED_EXTENSION_TYPES,
    payload: data,
  }
}

export function fetchAvailableWorkTypes () {
  return apiAction({
    url: `${process.env.REACT_APP_BASE_URL}/appointplusportal/tecams/work-types`,
    onSuccess: setWorkTypes,
    label: GET_WORK_TYPES
  })
}

function setWorkTypes (data) {
  return {
    type: SET_WORK_TYPES,
    payload: data.workTypes
  }
}

export function fetchRequiredExtensionTypesForWorkType (appointmentType, appointmentSubType, productFamily, workTypeID) {
  return apiAction({
    url: `${process.env.REACT_APP_BASE_URL}/appointplusportal/tecams/required-extension-types`,
    onSuccess: setRequiredExtensionTypesForWorkType(workTypeID),
    label: GET_REQUIRED_EXTENSION_TYPES_FOR_WORKTYPE,
    data: {
      level: LEVEL.TASK_LEVEL,
      appointmentType,
      appointmentSubType,
      productFamily,
      workTypeID
    }
  })
}

function setRequiredExtensionTypesForWorkType (workTypeID) {
  return (data) => {
    return {
      type: SET_REQUIRED_EXTENSION_TYPES_FOR_WORKTYPE,
      payload: { data: data.extensions, workTypeID }
    }
  }
}

export function getSlots (appointment) {
  const {
    appointmentType,
    appointmentSubType,
    appointmentPriority,
    additionalAppointment,
    orderDetails,
    customerAndContacts,
    appointmentTasks,
    preferredAppointment,
    relatedAppointments
  } = appointment
  const appointmentExtensions = []
  _.forEach(_.keys(additionalAppointment), (extensionType) => {
    _.forEach(_.keys(additionalAppointment[extensionType]), (extensionName) => {
      appointmentExtensions.push({
        extensionType,
        extensionName,
        extensionValue: additionalAppointment[extensionType][extensionName]
      })
    })
  })

  const payload = {
    orderReferences: orderDetails.map((item, index) => ({
      identifier: item.primaryOrderNumber,
      system: item.orderSystem,
      type: index === 0 ? 'PRIMARY' : 'SECONDARY'
    })),
    appointmentType: appointmentType.toUpperCase(),
    appointmentSubType: appointmentSubType.toUpperCase(),
    priority: appointmentPriority.toUpperCase(),
    ipNetworkFnn: orderDetails[0].ipNetworkFnn,
    appointmentNotes: preferredAppointment.notes,
    addressDetails: {
      line1: customerAndContacts.customerDetails.addressLine1,
      line2: customerAndContacts.customerDetails.addressLine2,
      line3: customerAndContacts.customerDetails.addressLine3,
      state: _.find(STATES, { name: customerAndContacts.customerDetails.state }).code,
    },
    contacts: [
      ...customerAndContacts.customerContacts.map(contact => ({
        firstName: contact.firstName,
        lastName: contact.lastName,
        primaryPhone: contact.telephoneNumber,
        secondaryPhone: contact.mobileNumber,
        role: contact.role,
        emailAddress: contact.emailAddress
      })),
      ...customerAndContacts.tecamsContacts.map(contact => ({
        firstName: contact.firstName,
        lastName: contact.lastName,
        primaryPhone: contact.telephoneNumber,
        secondaryPhone: contact.mobileNumber,
        role: contact.role,
        userID: contact.userID,
        emailAddress: contact.emailAddress
      }))
    ],
    customerDetails: {
      name: customerAndContacts.customerDetails.customerName,
      customerId: customerAndContacts.customerDetails.customerId,
      customerType: customerAndContacts.customerDetails.customerType,
      offshoreConsenting: customerAndContacts.customerDetails.isCustomerOffshoreConsenting === 'Yes'
    },
    appointmentExtensions,
    taskItems: appointmentTasks.map((task, index) => {
      const taskExtensions = []
      _.forEach(_.keys(task.additionalTaskDetails), extensionType => {
        _.forEach(_.keys(task.additionalTaskDetails[extensionType]), extensionName => {
          taskExtensions.push({
            extensionType,
            extensionName,
            extensionValue: task.additionalTaskDetails[extensionType][extensionName]
          })
        })
      })
      return ({
        arrayItemNumber: index + 1,
        productFamily: task.productFamily,
        workTypeID: task.work.workTypeID,
        taskExtensions
      })
    }),
    relatedAppointments: relatedAppointments.map(item => ({
      appointmentCategory: item.relatedAppointmentCategory,
      appointmentID: item.relatedAppointmentId,
      location: item.relatedAppointmentLocation
    })),
    slotCriteria: {
      startDate: moment(preferredAppointment.startDate)
      .startOf('day').format(DATE_FORMAT)
    }
  }
  return apiAction({
    url: `${process.env.REACT_APP_BASE_URL}/appointplusportal/tecams/slot-enquiry`,
    method: 'POST',
    onSuccess: setSlots,
    label: GET_SLOTS,
    data: {
      data: payload
    }
  })
}

function setSlots (data) {
  return {
    type: SET_SLOTS,
    payload: data,
  }
}

export function postAppointment (appointment) {
  const {
    appointmentType,
    appointmentSubType,
    appointmentPriority,
    additionalAppointment,
    orderDetails,
    customerAndContacts,
    appointmentTasks,
    preferredAppointment,
    relatedAppointments,
    slotID
  } = appointment
  const appointmentExtensions = []
  _.forEach(_.keys(additionalAppointment), (extensionType) => {
    _.forEach(_.keys(additionalAppointment[extensionType]), (extensionName) => {
      appointmentExtensions.push({
        extensionType,
        extensionName,
        extensionValue: additionalAppointment[extensionType][extensionName]
      })
    })
  })

  const payload = {
    orderReferences: orderDetails.map((item, index) => ({
      identifier: item.primaryOrderNumber,
      system: item.orderSystem,
      type: index === 0 ? 'PRIMARY' : 'SECONDARY'
    })),
    appointmentType: appointmentType.toUpperCase(),
    appointmentSubType: appointmentSubType.toUpperCase(),
    priority: appointmentPriority.toUpperCase(),
    ipNetworkFnn: orderDetails[0].ipNetworkFnn,
    appointmentNotes: preferredAppointment.notes,
    addressDetails: {
      line1: customerAndContacts.customerDetails.addressLine1,
      line2: customerAndContacts.customerDetails.addressLine2,
      line3: customerAndContacts.customerDetails.addressLine3,
      state: _.find(STATES, { name: customerAndContacts.customerDetails.state }).code,
    },
    contacts: [
      ...customerAndContacts.customerContacts.map(contact => ({
        firstName: contact.firstName,
        lastName: contact.lastName,
        primaryPhone: contact.telephoneNumber,
        secondaryPhone: contact.mobileNumber,
        role: contact.role,
        emailAddress: contact.emailAddress
      })),
      ...customerAndContacts.tecamsContacts.map(contact => ({
        firstName: contact.firstName,
        lastName: contact.lastName,
        primaryPhone: contact.telephoneNumber,
        secondaryPhone: contact.mobileNumber,
        role: contact.role,
        userID: contact.userID,
        emailAddress: contact.emailAddress
      }))
    ],
    customerDetails: {
      name: customerAndContacts.customerDetails.customerName,
      customerId: customerAndContacts.customerDetails.customerId,
      customerType: customerAndContacts.customerDetails.customerType,
      offshoreConsenting: customerAndContacts.customerDetails.isCustomerOffshoreConsenting === 'Yes'
    },
    appointmentExtensions,
    taskItems: appointmentTasks.map((task, index) => {
      const taskExtensions = []
      _.forEach(_.keys(task.additionalTaskDetails), extensionType => {
        _.forEach(_.keys(task.additionalTaskDetails[extensionType]), extensionName => {
          taskExtensions.push({
            extensionType,
            extensionName,
            extensionValue: task.additionalTaskDetails[extensionType][extensionName]
          })
        })
      })
      return ({
        arrayItemNumber: index + 1,
        productFamily: task.productFamily,
        workTypeID: task.work.workTypeID,
        taskExtensions
      })
    }),
    relatedAppointments: relatedAppointments.map(item => ({
      appointmentCategory: item.relatedAppointmentCategory,
      appointmentID: item.relatedAppointmentId,
      location: item.relatedAppointmentLocation
    }))
  }
  return apiAction({
    url: `${process.env.REACT_APP_BASE_URL}/appointplusportal/tecams/reservation/${slotID}`,
    method: 'POST',
    onSuccess: postAppointmentSuccess,
    onFailure: postAppointmentFailure,
    label: POST_APPOINTMENT,
    data: {
      data: payload
    }
  })
}

function postAppointmentSuccess (data) {
  return {
    type: POST_APPOINTMENT_SUCCESS,
    payload: data,
  }
}

function postAppointmentFailure (error) {
  return {
    type: POST_APPOINTMENT_FAILURE,
    payload: error
  }
}
