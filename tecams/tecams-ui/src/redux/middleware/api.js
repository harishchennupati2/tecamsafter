// inspired by https://leanpub.com/redux-book
import axios from 'axios'
import { API } from '../actions/types'
import { accessDenied, apiError, apiStart, apiEnd } from '../actions/api'

const apiMiddleware = ({ dispatch }) => next => action => {
  console.log('action', action)
  if (!action) {
    return
  }
  next(action)

  if (action.type !== API) return

  const {
    url,
    method,
    data,
    onSuccess,
    onFailure,
    label,
    headers,
  } = action.payload
  const dataOrParams = ['GET', 'DELETE'].includes(method) ? 'params' : 'data'

  // axios default configs
  axios.defaults.baseURL = process.env.REACT_APP_BASE_URL || ''
  axios.defaults.headers.common['Content-Type'] = 'application/json'
  axios.defaults.headers.common[
    'Authorization'
    ] = `Bearer ${process.env.REACT_APP_AUTH_TOKEN}`
  axios.defaults.headers.common['callingSystemID'] = 'TECAMS_UI'
  axios.defaults.headers.common['requestUserID'] = 'd888999'

  if (label) {
    dispatch({type: label})
    dispatch(apiStart(label))
  }

  axios
  .request({
    url,
    method,
    headers,
    [dataOrParams]: data,
  })
  .then(({ data, headers }) => {
    console.log('data', data)
    dispatch(onSuccess(data, headers))
  })
  .catch(error => {
    dispatch(apiError(error))
    if (onFailure) {
      dispatch(onFailure(error))
    }
    if (error.response && error.response.status === 403) {
      dispatch(accessDenied(window.location.pathname))
    }
  })
  .finally(() => {
    if (label) {
      dispatch(apiEnd(label))
    }
  })
}

export default apiMiddleware
