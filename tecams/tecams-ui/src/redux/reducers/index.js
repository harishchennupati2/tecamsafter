import {
  API_START,
  API_END,
  SET_ACTION_DETAIL,
  SET_WORK_SCHEDULES,
  API_ERROR,
  SET_REQUIRED_EXTENSION_TYPES,
  SET_WORK_TYPES,
  SET_REQUIRED_EXTENSION_TYPES_FOR_WORKTYPE,
  SET_SLOTS, POST_APPOINTMENT, POST_APPOINTMENT_SUCCESS, POST_APPOINTMENT_FAILURE
} from '../actions/types'

import {
  SET_APPOINTMENTS,
  SET_UPCOMMING_APPOINTMENTS,
  SET_APPOINTMENT,
  SET_DETAILS,
  SET_USERS
} from '../../config/constants'

import _ from 'lodash'

export const initialState = {
  actionDetail: [],
  workSchedules: {
    total: 0,
    currentPage: 1,
    datas: [],
    limit: 10,
  },
  appointments: {
    total: 0,
    results: [],
  },
  upcommingAppointment: {
    total: 0,
    results: [],
  },
  autocompleteUsers: [],
  workTypes: [],
  workTypeRequiredExtensions: {},
  requiredExtensionTypes: null,
  appointment: null,
  summaryMetrics: {},
  error: null,
  details: {},
  availableSlots: {
    fetched: false,
    slots: []
  },
  reservedAppointment: {
    reserving: false,
    error: null
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_ACTION_DETAIL:
      return {
        ...state,
        actionDetail: action.payload,
      }
    case SET_WORK_SCHEDULES: {
      const { workSchedules } = state
      workSchedules.datas = action.payload
      workSchedules.total = parseInt(action.headers['x-total-count'])
      workSchedules.currentPage = action.requestData.page
      return {
        ...state,
        workSchedules: _.cloneDeep(workSchedules),
      }
    }
    case SET_DETAILS: {
      console.log('cannot set details')
      return {
        ...state,
        details: action.payload,
      }
    }
    case SET_APPOINTMENTS: {
      console.log()
      let res = {
        results: action.payload.data,
        total: parseInt(action.headers['x-total-count']),
      }

      return {
        ...state,
        appointments: _.cloneDeep(res),
        summaryMetrics: action.payload.metrics,
      }
    }
    case SET_UPCOMMING_APPOINTMENTS: {
      let res = {
        results: action.payload,
        total: parseInt(action.headers['x-total-count']),
      }

      return {
        ...state,
        upcommingAppointment: _.cloneDeep(res),
      }
    }
    case SET_APPOINTMENT: {
      return {
        ...state,
        appointment: action.payload,
      }
    }
    case SET_REQUIRED_EXTENSION_TYPES: {
      return {
        ...state,
        requiredExtensionTypes: action.payload,
      }
    }
    case SET_USERS: {
      return {
        ...state,
        autocompleteUsers: action.payload
      }
    }
    case SET_WORK_TYPES: {
      return {
        ...state,
        workTypes: action.payload
      }
    }
    case SET_REQUIRED_EXTENSION_TYPES_FOR_WORKTYPE: {
      return {
        ...state,
        workTypeRequiredExtensions: {
          ...state.workTypeRequiredExtensions,
          [action.payload.workTypeID]: action.payload.data
        }
      }
    }
    case SET_SLOTS: {
      return {
        ...state,
        availableSlots: {
          fetched: true,
          slots: [...action.payload.slots],
          estimatedDuration: action.payload.estimatedDuration
        }
      }
    }
    case POST_APPOINTMENT: {
      return {
        ...state,
        reservedAppointment: {
          reserving: true,
          error: null
        }
      }
    }

    case POST_APPOINTMENT_SUCCESS: {
      return {
        ...state,
        reservedAppointment: {
          reserving: false,
          error:null,
          appointmentID: action.payload.appointmentID
        }
      }
    }

    case POST_APPOINTMENT_FAILURE: {
      return {
        ...state,
        reservedAppointment: {
          reserving: false,
          error: action.payload
        }
      }
    }

    case API_ERROR: {
      return {
        ...state,
        error: action.error,
      }
    }
    case API_START:
      return state
    case API_END:
      return state
    default:
      return state
  }
}
