import './styles.scss';
import iconClose from 'assets/icons/ic-gray-close.svg';

import React from 'react';
import Modal from 'react-bootstrap/Modal';

const ModalContact = ({ show, onHide, emailCustomer, emailTech, phoneNumber }) => (
  <div className='ModalContact'>
    <Modal centered show={show} dialogClassName='modal-contact' onHide={onHide}>
      <div className='modal-container'>
        <span className='btn-close center-child' onClick={onHide}>
          <img src={iconClose} alt='' />
        </span>
        <span className='big-title'>Contact</span>
        <div className='modal-container__body'>
          <div className='info'>
            <span className='title'>Customer Support & General Inquiry</span>
            <ul className='rows'>
              <li className='row-item'>
                <span className='row-item-name'>Email</span>
                <a className='row-item-value email' href={`mailto:${emailCustomer}`}>
                  {emailCustomer}
                </a>
              </li>
              <li className='row-item'>
                <span className='row-item-name'>Phone</span>
                <a className='row-item-value' href={`tel:+${phoneNumber}`}>
                  {phoneNumber}
                </a>
              </li>
            </ul>
          </div>
          <div className='info clone'>
            <span className='title'>TeCAMS Technical Support</span>
            <ul className='rows'>
              <li className='row-item'>
                <span className='row-item-name'>Email</span>
                <a className='row-item-value email' href={`mailto:${emailTech}`}>
                  {emailTech}
                </a>
              </li>
              <li className='row-item'>
                <span className='row-item-name'>Phone</span>
                <a className='row-item-value' href={`tel:+${phoneNumber}`}>
                  {phoneNumber}
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </Modal>
  </div>
);

ModalContact.defaultProps = {
  emailCustomer: 'contact@tecams.com',
  emailTech: 'techsupport@tecams.com',
  phoneNumber: '1231231234',
};

export default ModalContact;
