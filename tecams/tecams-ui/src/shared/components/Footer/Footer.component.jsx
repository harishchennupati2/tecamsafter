import style from './styles.module.scss';
import appoint from 'assets/icons/appoint.svg';

import React, { useState } from 'react';
import ModalContact from './ModalContact/ModalContact.component';
import ModalTermOfUse from './ModalTermOfUse/ModalTermOfUse.component';
import { withRouter } from 'react-router-dom';

const Footer = ({ location }) => {
  const [modalTermShow, setModalTermShow] = useState(false);
  const [modalContactShow, setModalContactShow] = useState(false);

  const handleModalTerm = () => setModalTermShow(true);
  const handleModalContact = () => setModalContactShow(true);

  if (location.pathname !== '/') {
    return (
      <div className={`d-flex justify-content-between ${style['component-container']}`}>
        <div className={`d-flex justify-content-between align-items-center ${style['wrapAll']}`}>
          <img src={appoint} alt='' />
          <div className={`d-flex flex-wrap align-items-center ${style['right-link']}`}>
            <span onClick={handleModalTerm}>Terms of Use</span>
            <span onClick={handleModalContact}>Contact</span>
            {modalContactShow && <ModalContact show={modalContactShow} onHide={() => setModalContactShow(false)} />}
            {modalTermShow && <ModalTermOfUse show={modalTermShow} onHide={() => setModalTermShow(false)} />}
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div></div>
    );
  }
};
export default withRouter(Footer);
