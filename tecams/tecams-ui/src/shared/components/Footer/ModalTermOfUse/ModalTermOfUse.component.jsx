import './styles.scss';
import iconClose from 'assets/icons/ic-gray-close.svg';

import React from 'react';
import Modal from 'react-bootstrap/Modal';

const ModalTermOfUse = ({ show, onHide }) => (
  <div className='ModalTermOfUse'>
    <Modal centered show={show} dialogClassName='modal-contact' onHide={onHide}>
      <div className='modal-container'>
        <span className='btn-close center-child' onClick={onHide}>
          <img src={iconClose} alt='' />
        </span>
        <span className='big-title'>Term of Use</span>
        <div className='modal-container__body'>
          <div className='section-one'>
            <span className='section-title'>Section title</span>
            <div className='wrap'>
              <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et
                ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
                dolor sit amet, consetetur sadipscing elitr, sed diam.
              </p>
              <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et
                ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
                dolor sit amet, consetetur sadipscing elitr, sed diam.
              </p>
            </div>
          </div>
          <div className='section-one'>
            <span className='section-title'>Section title</span>
            <div className='wrap'>
              <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et
                ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
                dolor sit amet, consetetur sadipscing elitr, sed diam.
              </p>
              <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et
                ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
                dolor sit amet, consetetur sadipscing elitr, sed diam.
              </p>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  </div>
);

export default ModalTermOfUse;
