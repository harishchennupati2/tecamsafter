import React from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import './styles.scss';

const CalendarMultipleActive = ({ startDate, timeSlots, setStartDate }) => {

  const renderDayContents = (day, date) => {
    return <span>{new Date(date).getDate()}</span>;
  };

  return (
    <DatePicker
      formatWeekDay={nameOfDay => nameOfDay.substr(0, 1)}
      renderDayContents={renderDayContents}
      selected={startDate}
      onChange={date => setStartDate(date)}
      highlightDates={timeSlots}
      inline
    />
  )
}

export default CalendarMultipleActive;
