import React from 'react'
import { INPUT_TYPES } from '../../../config/constants'
import icGrayHelp from 'assets/icons/help.svg'
import Dropdown from '../Dropdown/Dropdown.component'

const Input = ({
  type, name, value, onChange, selectedValue, description, isMandatory, options, maxLength, hasError, disabled
}) => {
  switch (type) {
    case INPUT_TYPES.LOV:
      return (
        <>
          <span className='name-item'>
            {name}
            <span className="two-dots">:</span>
            {isMandatory && <span className='red-symbol'>*</span>}
            <span className='click-noti'>
              <img className='one' src={icGrayHelp} alt='icon'/>
              <div className='noti-content'>
                <p className='text-noti'>
                  {description}
                </p>
              </div>
            </span>
          </span>
          <span className="text-right-input">{selectedValue || ''}</span>
          <div className='cover-input dropdown-ver'>
            <Dropdown
              classes={`${hasError && 'error-ver'} ${disabled && 'disabled'}`}
              label='additionalAppointment'
              selectedValue={selectedValue}
              options={options}
              onChangeDropdown={onChange}
              disabled={disabled}
            />
          </div>
        </>
      )
    case INPUT_TYPES.STRING:
      return (
        <>
          <span className='name-item'>
            {name}
            <span className="two-dots">:</span>
            {isMandatory && <span className='red-symbol'>*</span>}
            <span className='click-noti'>
              <img className='one' src={icGrayHelp} alt='icon'/>
              <div className='noti-content'>
                <p className='text-noti'>
                  {description}
                </p>
              </div>
            </span>
          </span>
          <span className="text-right-input">{value || ''}</span>
          <div
            className={`cover-input ${hasError && 'error-ver'} ${disabled && 'disabled'}`}>
            <input
              type='text'
              placeholder='Enter…'
              maxLength={`${maxLength}`}
              value={value || ''}
              onChange={onChange}
              disabled={disabled}
            />
          </div>
        </>
      )
    default:
      return <></>
  }
}

export default Input
