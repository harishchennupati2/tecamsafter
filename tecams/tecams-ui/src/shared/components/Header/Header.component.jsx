import style from './styles.module.scss';
import logo from 'assets/images/logo.svg';
import logout from 'assets/icons/ic-blue-logout.svg';
import help from 'assets/icons/help.svg';
import helpHover from 'assets/icons/help-hover.svg';
import notification from 'assets/icons/notification.svg';
import notificationHover from 'assets/icons/notification-hover.svg';

import React, { useState } from 'react';
import { Link, NavLink, withRouter } from 'react-router-dom';

const Header = ({ location }) => {
  const [role] = useState(localStorage.getItem('role'));
  if (location.pathname !== '/') {
    return (
      <div className={`d-flex justify-content-between align-items-center ${style['component-container']}`}>
        <div className={`d-flex justify-content-between align-items-center ${style['wrapAll']}`}>
          <Link to='/my-appointments'>
            <img src={logo} alt='' />
          </Link>
          <div className={`d-flex flex-wrap align-items-center ${style['middle-link']}`}>
            {role === 'Resource Manager' && (
              <NavLink activeClassName={style['is-active']} to='/my-work-schedule'>
                My Work Schedule
                  </NavLink>
            )}
            {role === 'Resource Manager' && (
              <NavLink activeClassName={style['is-active']} to='/my-team-work-schedule'>
                My Team Work Schedule
                  </NavLink>
            )}
            {role !== 'Resource Manager' && role !== 'Technician' && (
              <NavLink activeClassName={style['is-active']} to='/my-appointments'>
                My Appointments
                  </NavLink>
            )}
            <NavLink activeClassName={style['is-active']} to='/view-appointments'>
              View Appointments
                </NavLink>
            <NavLink activeClassName={style['is-active']} to='/manage-appointments'>
              Manage Appointments
                </NavLink>
            {role === 'Resource Manager' && (
              <NavLink activeClassName={style['is-active']} to='/manage-tasks'>
                Manage Tasks
                  </NavLink>
            )}
          </div>
          <div className='d-flex align-items-center'>
            <div className={`d-flex align-items-center ${style['user-info']}`}>
              <div className='d-flex flex-column align-items-end'>
                <span className={style['username']}>Jack Smith</span>
                <span className={style['job-title']}>Job Title</span>
              </div>
              <span className={`${style['avatar']} center-child`}>JS</span>
              <div className={style['tooltip-user']}>
                <div className={style.overview}>
                  <span className={style.idd}>ID 123456</span>
                  <span className={style.name}>Acme Team</span>
                  <span className={style.location1}>IT Service Department</span>
                  <span className={style.location2}>New York, USA</span>
                </div>
                <div className={style.logout}>
                  <Link to='/' className={`d-flex align-items-center ${style['btn-logout']}`}>
                    <img src={logout} alt='' />
                    Log Out
                      </Link>
                </div>
              </div>
            </div>
            <span className={style.separator}></span>
            <div className={style['group-noti']}>
              <img className={style.notification} src={notification} alt='' />
              <img className={style.notification} src={notificationHover} alt='' />
              <div className={`${style['tooltip-noti']} center-child`}>Notifications</div>
            </div>
            <div className={style['group-help']}>
              <img className={style.notification} src={help} alt='' />
              <img className={style.notification} src={helpHover} alt='' />
              <div className={`${style['tooltip-help']} center-child`}>Help</div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div></div>
    );
  }
};

export default withRouter(Header);
