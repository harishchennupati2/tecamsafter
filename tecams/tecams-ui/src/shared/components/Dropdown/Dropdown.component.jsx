import React, { useState } from 'react';
import ClickOutHandler from 'react-clickout-handler';
import arrows from '../../../assets/icons/ic-blue-arrow-down.svg';
import './styles.scss';

const Dropdown = ({ classes, label, selectedValue, options, onChangeDropdown }) => {
  const [isShowDropdown, setIsShowDropdown] = useState(false);

  /**
   * show dropdown
   */
  function showDropdown() {
    setIsShowDropdown(!isShowDropdown);
  }

  /**
   * hide dropdown
   */
  function hideDropdown() {
    setIsShowDropdown(false);
  }

  return (
    <React.Fragment>
      <ClickOutHandler onClickOut={() => setIsShowDropdown(false)}>
        {!!options &&
          (
            <div className={`drop-down ${isShowDropdown ? 'open' : ''} ${classes !== undefined ? classes : ''}`}>
              <button className='dropdown-btn' onClick={() => showDropdown()}>
                <span className={selectedValue ? 'value' : 'value placeholder'}>
                  {selectedValue ? selectedValue : 'Select...'}
                </span>
                <img src={arrows} alt='icon' className='icons icon-arrow-down'></img>
              </button>
              <ul className='drop-menu'>
                {
                  options.map((item, index) =>
                    (
                      <li key={'li_' + index + 1} >
                        <button className={selectedValue === item.name ? 'active' : ''}
                          onClick={() => { onChangeDropdown(item.name, label, index); hideDropdown() }}>
                          {item.name}
                        </button>
                      </li>
                    )
                  )
                }
              </ul>
            </div>
          )
        }
      </ClickOutHandler>
    </React.Fragment>
  );
}

export default Dropdown;
