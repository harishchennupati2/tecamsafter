export const SET_APPOINTMENTS = 'SET_APPOINTMENTS'
export const GET_APPOINTMENTS = 'GET_APPOINTMENTS'
export const SET_APPOINTMENT = 'SET_APPOINTMENT'
export const GET_APPOINTMENT = 'GET_APPOINTMENT'
export const SET_UPCOMMING_APPOINTMENTS = 'SET_UPCOMMING_APPOINTMENTS'
export const GET_UPCOMMING_APPOINTMENTS = 'GET_UPCOMMING_APPOINTMENTS'
export const SET_SUMMARY_METRICS = 'SET_SUMMARY_METRICS'
export const GET_SUMMARY_METRICS = 'GET_SUMMARY_METRICS'
export const SET_USERS = 'SET_USERS'
export const GET_USERS = 'GET_USERS'
export const GET_DETAILS = 'GET_DETAILS'
export const SET_DETAILS = 'SET_DETAILS'

export const PAGE_SIZE = 10
export const PAGE = 1

export const API_URL = 'http://localhost:1234'

export const LEVEL = {
  APPT_LEVEL: 'APPT_LEVEL',
  TASK_LEVEL: 'TASK_LEVEL',
}

export const INPUT_TYPES = {
  STRING: 'STRING',
  LOV: 'LOV',
  INTEGER: 'INTEGER'
}

export const MANDATORY_INDICATORS = {
  Y: true,
  N: false
}

export const APPOINTMENT_TYPES = [
  { name: 'Business Hours', value: 'BUSINESS_HOURS' },
  { name: 'After Hours', value: 'AFTER_HOURS' },
]

export const APPOINTMENT_SUB_TYPES = [
  { name: 'Cutover', value: 'CUTOVER' },
  { name: 'Non cutover', value: 'NONCUTOVER' },
]

export const APPOINTMENT_PRIORITIES = [{ name: 'Standard' }, { name: 'Urgent' }]

export const PRODUCTS = [{ name: 'Product 001' }, { name: 'Product 002' }]

export const ORDERING_SYSTEM = [{ name: 'RASS' }, { name: 'AMDOCS' }]

export const CUSTOMER_TYPES = [{ name: 'GOVERNMENT' }, { name: 'ENTERPRISE' }, { name: 'WHOLESALE' }]

export const RELATED_CATEGORIES = [{ name: 'TELSTRA' }]

export const RELATED_LOCATIONS = [{ name: 'SITE' }, { name: 'FIELD' }]

export const FUNDING_CODE_TYPES = [{ name: 'RASS order' }]

export const STATES = [
  { name: 'NSW', code: 'NSW' },
  { name: 'QLD', code: 'QLD' },
  { name: 'ACT', code: 'ACT' },
  { name: 'NT', code: 'NT' },
  { name: 'WA', code: 'WA' },
  { name: 'SA', code: 'SA' },
  { name: 'VIC', code: 'VIC' },
  { name: 'TAS', code: 'TAS' }
]

export const WORK_CATEGORIES = [
  { name: 'NBN Transition' },
  { name: 'Acme Work Category' },
]

export const TYPE_OF_WORK = [
  { name: 'NBN Transition: Transition BIP NBN + IOM' },
  { name: 'Acme Type of Work' },
]

export const MDN_ROUTER_ACTION = [{ name: 'Replaced' }]

export const EXITING_MDN_FNN = [{ name: 'Replaced' }]

export const PERIODS = [{ name: 'AM' }, { name: 'PM' }]

export const STATUS = [
  { name: 'Requested' },
  { name: 'Reserved' },
  { name: 'Confirmed' },
  { name: 'In Progress' },
  { name: 'Complete' },
  { name: 'Incompleted' },
  { name: 'Cancelled' },
]

export const CUSTOMERS = [
  { name: 'Government' },
  { name: 'Enterprise' },
  { name: 'Wholesale' },
]

export const TECHNICIAN = [
  {
    name: 'David Allington',
  },
  {
    name: 'David Brown',
  },
  {
    name: 'David Dun',
  },
  {
    name: 'David Falcon',
  },
  {
    name: 'Davidson Last Name',
  },
  {
    name: 'John Snow',
  },
  {
    name: 'Handy Manny',
  },
  {
    name: 'Marry Kim',
  },
  {
    name: 'Trevor Scott',
  },
  {
    name: 'Joshua Chan',
  },
  {
    name: 'Jack Longnamous',
  },
  {
    name: 'Alex Beck',
  },
]

export const DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ss'

export const CUSTOMER_CONTACT_TYPES = {
  'Primary Customer Contact': {
    name: 'Primary Customer Contact',
    key: 'CUSTOMER PRIMARY'
  },
  'Secondary Customer Contact': {
    name: 'Secondary Customer Contact',
    key: 'CUSTOMER PRIMARY'
  },
  'Customer Technical Contact': {
    name: 'Customer Technical Contact',
    key: 'CUSTOMER PRIMARY'
  }
}

export const TELSTRA_CONTACT_TYPES = {
  'Telstra Primary Contact': {
    name: 'Telstra Primary Contact',
    key: 'TELSTRA PRIMARY'
  },
  'Telstra Secondary Contact': {
    name: 'Telstra Secondary Contact',
    key: 'TELSTRA SECONDARY'
  }
}
