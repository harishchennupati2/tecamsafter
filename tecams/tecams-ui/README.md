# tecams-dev

## Prerequisite

- NodeJS v10

## Installing dependencies

Exuecute the following to install dependencies

```
npm install
```

## Running the frotend app

Execute the following command to run the frontend app

```
npm run start
```

After this go to the browser & browser this url http://localhost:3000/

## Runnight the mock backend API:

Download the mock API code from here: https://gitlab.com/tcdev/telstra/tecams-api
After this install the dependencies using `npm install` & then run the backend by executing `npm run start` to start the backend API.

![](https://i.imgur.com/OGlGdIy.png)
