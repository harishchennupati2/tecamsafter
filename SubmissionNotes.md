# Submission Notes

I have implemented all fixes but `When Clicking "Reserve Slot '' when it shows success then a new also should be added to db.json & on the dashboard page table `http://localhost:3000/my-appointments` the new record should be listed.
` this one. The current implementation of the appointments endpoint does not comply with the API document, the variable names and the overall data structure is different. Either both frontend and mock-api code should be modified for appointments section so that It is in sync with the API documents or I can implement a temporary mapping to comply with the data structure currently used in the mock-api.

For testing the error case on reservation, please select the 04:30PM - 06:30PM option. All other slots for each date will result in success.

